## So, I've heard you want to encrypt your environment files?

Well, you must be crazy then, but all right. Here's how you can do it.
See the [Encryption Data Flow](env-file-encr.md) for a detailed explanation on what algorithms, keys and methods are
used.

### Setup

What do we need?

- A private key (`private_key.pem`) and a public key (`public_key.pem`) (RSA) file in a safe location.
  These files will never ever leave your computer, keep them safe.
  No one may have access to both of these files, even the public key must be kept safe.
- A directory `crypt_config` with these files:
    - `aes_key.rsa`: An AES key (randomly generated), encrypted with the private RSA key.
    - `public_key_encrypted.pem`: The encrypted public RSA key, encrypted using a hash value calculated from several
      system information values.
    - `env.aes`: The .env file encrypted with the randomly generated AES key.

In any case, in the [../pom.xml](../pom.xml) file, switch the comments so that the `ConfigEncryption` class is the one
that is not commented out and build the jar.

```xml
<mainClass>fsi.config.encr.ConfigEncryption</mainClass>
<!--<mainClass>fsi.startup.Bot</mainClass>-->
```

Whenever you run commands for this tutorial on your own machine,
make sure to run all the commands listed below with these points in mind:

- run them inside a separate directory where no other file is located.
- have the `.env` file you want to encrypt in that directory as well.
  It must be called `.env`!

#### First time to run this process

_Obtain the system fingerprint_

I will assume in this process that you do not want to build the encrypted file on the target machine.
We will therefore first fetch some values from that target machine, create the encrypted files on your personal computer
and then move them over to the target machine.

One option for moving the jar file is to use scp:

```bash
scp target\FS-discordbot-1.1.2-SNAPSHOT.jar fsi@192.168.188.185:~/fsi-discord-bot
FS-discordbot-1.1.2-SNAPSHOT.jar        100%   22MB  71.2MB/s   00:00
```

Run the jar that you just built on the target machine.
The class that you used as the `mainClass` contains a utility to perform all the operations.

When running it, you will be prompted for what you want to do.
First, we want to find the system fingerprint that will be used to encrypt the public key.
For this, enter `s` and you will get a system fingerprint.
Copy this fingerprint and save it somewhere safe, you will need it later and every time from now on when you want to
update the config.

```bash
fsi@debian:~/fsi-discord-bot java -jar FS-discordbot-1.1.2-SNAPSHOT.jar
FS-I Bot configuration file encryption tool
Enter what you want to do:
 - [e]ncrypt a new configuration file
 - [d]ecrypt an existing configuration file
 - [s]how the system fingerprint
s
System fingerprint: 6efb823ha4d7b3f15474194d79k343574a69df58d7a0c32d6b3h6ae0f46ecff9
System fingerprint (as key): [45, ..., 51, 53, 55]
MAC Address: 041979F9CCA8
CPU: Intel Core i9-4330 CPU @ 1000GHz
OS: Linux
User: my-user
```

... to be honest, I like adding something else to the fingerprint, so that there is one more layer of obscureness.
Please talk to me about this in person, I would not like to note anything more about this here.

_Obtain the keys, encrypt them and the .env file_

Now, on your personal computer, you can run the jar again.
This time, you will enter `e` to encrypt a new configuration file.
Use the log below as a reference for what you need to do.

```bash
C:\Users\...\FS-discordbot\prepare-env>java -jar C:\Users\...\FS-discordbot\target\FS-discordbot-1.1.2-SNAPSHOT.jar
FS-I Bot configuration file encryption tool
Enter what you want to do:
 - [e]ncrypt a new configuration file
 - [d]ecrypt an existing configuration file
 - [s]how the system fingerprint
e
Do you want to [g]enerate new keys or [l]oad existing keys?
g
Do you want to use the [s]ystem fingerprint or enter a [c]ustom key for AES encryption?
c
Enter the custom key for AES encryption:
6efb823ha4d7b3f15474194d79k343574a69df58d7a0c32d6b3h6ae0f46ecff9

Encryption completed successfully.
```

You will now have the `crypt_config` directory with the files you need.
You can now move these files to the target machine.
Make sure the target directory is called `crypt_config` and is located in the same directory as the jar file.

You can do so with scp.
Make sure to move the whole directory with the `-r` flag.

```bash
scp -r C:\Users\...\FS-discordbot\prepare-env\crypt_config fsi@192.168.188.185:~/fsi-discord-bot
```

You can test whether the configuration is working by running the jar still with the changed main class on the target
machine.
This time, select `d` for decryption and see if the `.env` file is decrypted correctly.
Depending on whether you did it correctly or not, the process will either fail or succeed.

```bash
fsi@debian:~/fsi-discord-bot$ java -jar FS-discordbot-1.1.2-SNAPSHOT.jar
[INFO] [2024-06-24 16:33:31 f.c.e.ConfigEncryption    ]
[INFO] [2024-06-24 16:33:31 f.c.e.ConfigEncryption    ] FS-I Bot configuration file encryption tool
[INFO] [2024-06-24 16:33:31 f.c.e.ConfigEncryption    ] Enter what you want to do:
[INFO] [2024-06-24 16:33:31 f.c.e.ConfigEncryption    ]  - [e]ncrypt a new configuration file
[INFO] [2024-06-24 16:33:31 f.c.e.ConfigEncryption    ]  - [d]ecrypt an existing configuration file
[INFO] [2024-06-24 16:33:31 f.c.e.ConfigEncryption    ]  - [s]how the system fingerprint
d
[INFO] [2024-06-24 16:33:32 f.c.e.ConfigDecryption    ]
[INFO] [2024-06-24 16:33:32 f.c.e.ConfigDecryption    ] FS-I Bot configuration file decryption tool
[INFO] [2024-06-24 16:33:32 f.c.e.ConfigDecryption    ] Enter the directory where the encrypted files are located (public_key_encrypted.pem, aes_key.rsa, env.aes):
crypt_config
[INFO] [2024-06-24 16:33:39 f.c.e.ConfigDecryption    ] Do you want to use the [s]ystem fingerprint or enter a [c]ustom key for AES encryption?
s
[ERROR] [2024-06-24 16:33:41 f.c.e.ConfigDecryption    ] Error during decryption: Tag mismatch!

fsi@debian:~/fsi-discord-bot$ java -jar FS-discordbot-1.1.2-SNAPSHOT.jar
[INFO] [2024-06-24 16:33:56 f.c.e.ConfigEncryption    ]
[INFO] [2024-06-24 16:33:56 f.c.e.ConfigEncryption    ] FS-I Bot configuration file encryption tool
[INFO] [2024-06-24 16:33:56 f.c.e.ConfigEncryption    ] Enter what you want to do:
[INFO] [2024-06-24 16:33:56 f.c.e.ConfigEncryption    ]  - [e]ncrypt a new configuration file
[INFO] [2024-06-24 16:33:56 f.c.e.ConfigEncryption    ]  - [d]ecrypt an existing configuration file
[INFO] [2024-06-24 16:33:56 f.c.e.ConfigEncryption    ]  - [s]how the system fingerprint
d
[INFO] [2024-06-24 16:33:57 f.c.e.ConfigDecryption    ]
[INFO] [2024-06-24 16:33:57 f.c.e.ConfigDecryption    ] FS-I Bot configuration file decryption tool
[INFO] [2024-06-24 16:33:57 f.c.e.ConfigDecryption    ] Enter the directory where the encrypted files are located (public_key_encrypted.pem, aes_key.rsa, env.aes):
crypt_config
[INFO] [2024-06-24 16:34:02 f.c.e.ConfigDecryption    ] Do you want to use the [s]ystem fingerprint or enter a [c]ustom key for AES encryption?
s
[INFO] [2024-06-24 16:34:03 f.c.e.ConfigDecryption    ] Decrypted .env file:
DISCORD_BOT_TOKEN=...
PRIMARY_BOT_GUILD_ID=...
```

#### Updating the configuration

If you want to update the configuration, you can do so by running the jar on your personal computer again.
Use `e` to select the encryption process.
This time, you will enter `l` to load the existing keys and then as usual `c` to use the system fingerprint.
Make sure to take a good look at the prompts, they are not in the order you would expect, but I am too lazy now.

```bash
C:\Users\...\FS-discordbot\prepare-env>java -jar C:\Users\...\FS-discordbot\target\FS-discordbot-1.1.2-SNAPSHOT.jar
FS-I Bot configuration file encryption tool
Enter what you want to do:
 - [e]ncrypt a new configuration file
 - [d]ecrypt an existing configuration file
 - [s]how the system fingerprint
e
Do you want to [g]enerate new keys or [l]oad existing keys?
l
Do you want to use the [s]ystem fingerprint or enter a [c]ustom key for AES encryption?
c
Enter the path to the public key file (leave empty for public_key.pem):

Enter the path to the private key file (leave empty for private_key.pem):

Enter the custom key for AES encryption:
6efb823ha4d7b3f15474194d79k343574a69df58d7a0c32d6b3h6ae0f46ecff9

Encryption completed successfully.
```

And once again move them to the target machine.
That's it!
