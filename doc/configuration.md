## Bot-Konfigurationsparameter

Diese Konfigurationsparameter werden für die Einrichtung und den Betrieb des FS-I Discord Bots benötigt.
Unten sind einige Beschreibungen und Beispiele für die Werte aufgelistet.

Diese Attribute können entweder in einer .env (dotenv) Datei oder über Umgebungsvariablen gesetzt werden.
Eine Template-Datei [../template.env](../template.env) ist verfügbar, um die Konfiguration zu erleichtern.

Alle Attribute werden in [FsBotConfig.java](../src/main/java/fsi/config/FsBotConfig.java) definiert.

### Discord Bot

- Siehe [Bot.java](../src/main/java/fsi/startup/Bot.java)

- `DISCORD_BOT_TOKEN`
    - Beschreibung: Der Token, der durch den Besuch des
      [Discord Developer Portals](https://discord.com/developers/applications) und das Erstellen eines Bots dort
      erhalten wird.
- `PRIMARY_BOT_GUILD_ID`
    - Beschreibung: Die Guild ID des Servers, auf dem der Bot betrieben wird.
    - Beispiel: `691590564315004959`

### Google Mail Konfiguration

- Siehe [MailSendingService.java](../src/main/java/fsi/service/MailSendingService.java)

- `MAIL_CLIENT_ID`
    - Beschreibung: Siehe [google_mail_secret.md](google_mail_secret.md) für Informationen, wie dieser Wert erhalten
      wird.
- `MAIL_CLIENT_SECRET`
    - Beschreibung: Siehe [google_mail_secret.md](google_mail_secret.md) für Informationen, wie dieser Wert erhalten
      wird.
- `MAIL_REFRESH_TOKEN`
    - Beschreibung: Siehe [google_mail_secret.md](google_mail_secret.md) für Informationen, wie dieser Wert erhalten
      wird.
- `MAIL_SENDER_EMAIL_ADDRESS`
    - Beschreibung: Die E-Mail-Adresse, die zum Versenden der Mails verwendet wird.
      Siehe [google_mail_secret.md](google_mail_secret.md) mehr Details darüber.
    - Beispiel: `fachschaftverify@gmail.com`
- `MAIL_SENDER_USER`
    - Beschreibung: Der Teil vor dem `@gmail.com` in der E-Mail-Adresse, die zum Versenden der Mail verwendet wird.
      Dieses Attribut wird für die Erzeugung des Authentication Tokens in Kombination mit den anderen Attributen oben.
    - Beispiel: `fachschaftverify`

### Google Mail SMTP Einstellungen

- Siehe [MailSendingService.java](../src/main/java/fsi/service/MailSendingService.java)
- Hier einfach die Standardwerte verwenden.

- `MAIL_SMTP_HOST`
    - Beschreibung: Üblicherweise: `smtp.gmail.com`
    - Beispiel: `smtp.gmail.com`
- `MAIL_SMTP_PORT`
    - Beschreibung: Üblicherweise: `587`
    - Beispiel: `587`
- `MAIL_SMTP_STARTTLS_ENABLE`
    - Beschreibung: Üblicherweise: `true`
    - Beispiel: `true`
- `MAIL_SMTP_STARTTLS_REQUIRED`
    - Beschreibung: Üblicherweise: `true`
    - Beispiel: `true`
- `MAIL_SMTP_AUTH_MECHANISMS`
    - Beschreibung: Üblicherweise: `XOAUTH2`
    - Beispiel: `XOAUTH2`

### Verifizierung

- Siehe [VerifyUserSlash.java](../src/main/java/fsi/listeners/verification/VerifyUserSlash.java)
- Siehe
  [VerifyUserPrivateChatListener.java](../src/main/java/fsi/listeners/verification/VerifyUserPrivateChatListener.java)

- `VERIFY_CHANNEL_ID`
    - Beschreibung: Die Channel-ID des Kanals, auf den der `verify` Befehl beschränkt ist.
    - Beispiel: `952260026494500955`
- `VERIFY_COOLDOWN_DURATION_MS`
    - Beschreibung: Die Zeit in Millisekunden zwischen den Versuchen des Benutzers, sich mit dem `verify` Befehl zu
      verifizieren.
    - Beispiel: `60000`
- `VERIFY_ADD_USER_ROLES_ON_VERIFICATION`
    - Beschreibung: Eine CSV-Liste von Rollen-IDs, die einem Benutzer nach erfolgreicher Verifizierung hinzugefügt
      werden.
    - Beispiel: `756495178180853811, 951825918127661066`
- `VERIFY_REMOVE_USER_ROLES_ON_VERIFICATION`
    - Beschreibung: Eine CSV-Liste von Rollen-IDs, die einem Benutzer nach erfolgreicher Verifizierung entfernt werden.
    - Beispiel: `756494816749027411, 756494701389152337, 756494665691299978, 756494610720751726, 763449922351136798`
- `VERIFY_REDIRECT_USER_ON_VERIFICATION`
    - Beschreibung: Eine Channel-ID zu einem Kanal, zu dem der Nutzer einen Link erhalten soll, wenn er sich
      erfolgreich verifiziert hat.
    - Beispiel: `1087412251750838274`

### Sprachkanäle

- Siehe [VcListener.java](../src/main/java/fsi/listeners/voice/VcListener.java)

- `VOICE_CHANNEL_ID_CREATE_CHANNEL`
    - Beschreibung: Die Voice Channel ID des Audiokanals, der beim Beitritt eines Benutzers einen neuen Kanal mit dem
      Namen des Benutzers erstellt und diesen dorthin verschiebt.
    - Beispiel: `765599701533327396`
- `VOICE_CHANNEL_IDS_IGNORE`
    - Beschreibung: Alle Audiokanäle mit dem Begriff `Lerngruppe` in ihrem Namen werden überwacht und entfernt, wenn
      ihre Mitgliederanzahl 0 erreicht. Dieses Attribut ist eine CSV-Liste von Voice Channel IDs, die in diesem Prozess
      unabhängig von ihrem Namen ignoriert werden.
    - Beispiel: `755373837436321824, 755374121646555136, 824405905214144523`

### Altklausuren Nextcloud Konfiguration

- `ALTKL_NEXTCLOUD_BASE_URL`
    - Beschreibung: Die Basis-URL der Nextcloud-Instanz.
    - Beispiel: `https://owncloud.informatik.hs-mannheim.de`

- `ALTKL_NEXTCLOUD_WEB_DAV`
    - Beschreibung: Die WebDAV-URL der Nextcloud-Instanz.
    - Beispiel: `https://owncloud.informatik.hs-mannheim.de/remote.php/dav/files`

- `ALTKL_NEXTCLOUD_KLAUSUREN_PATH`
    - Beschreibung: Der Pfad auf der Nextcloud-Instanz, wo die Altklausur-Dateien verfügbar sind.
    - Beispiel: `/Fachschaft/Klausuren/Klausuren`

- `ALTKL_NEXTCLOUD_QUERY_LOG_PATH`
    - Beschreibung: Der Pfad auf der Nextcloud-Instanz, wo eine Textdatei liegt, zu der Benutzerabfragen hinzugefügt
      werden sollen.
    - Beispiel: `/Fachschaft/Klausuren/discord-klausur-exports-log.log`

- `ALTKL_NEXTCLOUD_UPLOAD_SHARE_ZIPS_PATH`
    - Beschreibung: Der Dateipfad auf der Nextcloud-Instanz, wohin die hochgeladenen Dateien zur Freigabe gespeichert
      werden.
    - Beispiel: `/Fachschaft/Klausuren/Klausuren/_temp`

- `ALTKL_NEXTCLOUD_CACHE_VALIDITY_DURATION`
    - Beschreibung: Die Gültigkeitsdauer des Caches der Altklausur-Dateien in Millisekunden, nach der der Cache ungültig
      wird und neu abgerufen wird.
    - Beispiel: `10800000` (entspricht 3 Stunden)

- `ALTKL_NEXTCLOUD_USER_UPLOAD_SHARE_ALTKLAUSUREN_PATH`
    - Beschreibung: Der Dateipfad auf der Nextcloud-Instanz, wo neue Verzeichnisse angelegt und freigegeben werden
      sollen, wenn ein Nutzer den Befehl `/altklausuren upload` ausführt.
    - Beispiel: `/Fachschaft/Klausuren/User-Upload`

- `ALTKL_NEXTCLOUD_CHANNEL`
    - Beschreibung: Die Channel-ID, an den Nutzer weitergeleitet werden sollen, wenn sie an den Altklausuren-Channel
      weitergeleitet werden sollen.
    - Beispiel: `1253310367891980359`

- `ALTKL_NEXTCLOUD_USER`
    - Beschreibung: Der Benutzername zur Authentifizierung bei der Nextcloud-Instanz.
    - Beispiel: `fsi_bot`

- `ALTKL_NEXTCLOUD_PASSWORD`
    - Beschreibung: Das Passwort zur Authentifizierung bei der Nextcloud-Instanz.
    - Beispiel: `mypassword123`

### Admin and Bot Management

- `ADMIN_USER_ID`
    - Beschreibung: Eine einzige Discord User-Id des Nutzers, der als Administrator des Bots fungiert.
      Diesem Nutzer wird die Möglichkeit gegeben, den Bot über einen Privatchat mit dem Bot fernzusteuern.  
      Mehr infos dazu in [Admin User](admin_user.md).
    - Beispiel: `508601845363507200`
