## Just a few commands for your convenience

```shell
sudo systemctl enable tailscaled
```

```shell
scp -r C:\Users\...\FS-discordbot\prepare-env\crypt_config fsi@192.168.188.185:~/fsi-discord-bot
scp FS-discordbot-1.5.1.jar fsi@192.168.188.185:~/fsi-discord-bot
ssh fsi@192.168.188.185
```

```shell
scp -r C:\Users\...\FS-discordbot\prepare-env\crypt_config fsi@100.84.217.93:~/fsi-discord-bot
scp FS-discordbot-1.5.2.jar fsi@100.84.217.93:~/fsi-discord-bot
ssh fsi@100.84.217.93
```
