### Encryption Process

There are two AES-256 keys in play.
- `aesR`: One is `R`andomly generated and will be encrypted using the private RSA key. It encrypts the `.env` file.
- `aesS` :The other is generated from the `S`ystem fingerprint and will encrypt the public RSA key.

1. **Generate RSA Key Pair:**
    - Use `RSAEncryption.generateKeyPair()` to generate an RSA key pair.
    - Save the public key to a file (`public_key.pem`).
    - Save the private key to a file (`private_key.pem`).

2. **Generate AES Key `aesS` from System Fingerprint:**
    - Generate a system fingerprint using `SystemInfo.createSystemFingerprint()`.
    - Generate a 256-bit AES key (`aesS`) using the system fingerprint.

3. **Encrypt Public Key with AES using System Fingerprint (`aesS`):**
    - Read the `public_key.pem` file.
    - Encrypt the contents of `public_key.pem` using AES encryption with the `aesS` key derived from the system fingerprint.
    - Store the encrypted public key on disk (`public_key_encrypted.pem`).

4. **Encrypt AES Key `aesR` with RSA Private Key:**
    - Generate a random 256-bit AES key (`aesR`) for encrypting the `.env` file.
    - Encrypt the `aesR` key using RSA encryption with the private RSA key (`private_key.pem`).
    - Store the encrypted AES key `aesR` on disk (`aes_key.rsa`).

5. **Encrypt Environment File (.env) with AES (`aesR`):**
    - Read the `.env` file.
    - Encrypt the contents of `.env` file using AES encryption with the `aesR` key.
    - Store the encrypted `.env` file on disk (`env.aes`).

**Outputs of Encryption Process:**
- `public_key.pem`: Public RSA key saved to file.
- `public_key_encrypted.pem`: Encrypted RSA public key encrypted with AES key `aesS` saved to file.
- `private_key.pem`: Private RSA key saved to file.
- `aes_key.rsa`: Encrypted AES key `aesR` encrypted with RSA private key saved to file.
- `env.aes`: Encrypted `.env` file encrypted with AES key `aesR` saved to file.

### Decryption Process

1. **Generate AES Key `aesS` from System Fingerprint:**
    - Generate a system fingerprint using `SystemInfo.createSystemFingerprint()`.
    - Generate a 256-bit AES key (`aesS`) using the system fingerprint.

2. **Decrypt Public Key with AES using System Fingerprint (`aesS`):**
    - Read the `public_key_encrypted.pem` file.
    - Decrypt the `public_key_encrypted.pem` file using AES decryption with the `aesS` key derived from the system fingerprint.
    - Obtain the original `public_key.pem` as a byte array or string (`decryptedPublicKeyBytes`). Do not write it to disk.

3. **Decrypt AES Key `aesR` with RSA Public Key:**
    - Retrieve the encrypted AES key `aesR` from file (`aes_key.rsa`).
    - Decrypt the `aes_key.rsa` using RSA decryption with the in memory decrypted public RSA key.
    - Obtain the original AES key `aesR` as a byte array (`decryptedAesKeyBytes`). Do not write it to disk.

4. **Decrypt Environment File `.env` with AES (`aesR`):**
    - Read the `env.aes` file.
    - Decrypt the `env.aes` file using AES decryption with the decrypted `aesR` key.
    - Obtain the original `.env` file as a byte array or string (`decryptedEnvBytes`). Do not write it to disk.

**Outputs of Decryption Process:**
- `decryptedPublicKeyBytes`: Decrypted public key (`public_key.pem`) stored as byte array or string.
- `decryptedAesKeyBytes`: Decrypted AES key `aesR` stored as byte array.
- `decryptedEnvBytes`: Decrypted `.env` file stored as byte array or string.

### Important Points
- **No Data Written to Disk During Decryption:** Ensure all decryption operations use byte arrays or strings in memory.
- **AES Key Regeneration:** The AES key `aesS` used for decrypting the public key is dynamically generated from the system fingerprint and is not persisted.
- **Encryption Methods:** AES is used to decrypt the public key and the `.env` file, while RSA decrypts the AES key.

### Process Flow Diagram

```mermaid
graph v

   subgraph Files
      direction LR

      102[public_key_encrypted.pem]
      103[private_key.pem]
      104[aes_key.rsa]
      105[env.aes]
   end

   subgraph Encryption_Process
      direction LR

      1[RSA Key Pair]
      2[RSA Public Key]
      3[RSA Private Key]
      4[Random AES Key aesR]
      201[System Fingerprint]
      202[???]
      5[AES Key aesS]
      9[.env File]
      6[Encrypt Environment File .env with AES aesR]
      7[Encrypt RSA Public Key with AES using aesS]
      8[Encrypt AES Key aesR with RSA Private Key]

      201 --> 5
      202 --> 5
      1 --> 3
      1 --> 2
      2 --> 7
      5 --> 7
      3 --> 8
      4 --> 8
      9 --> 6
      4 --> 6
   end

   subgraph Decryption_Process
      direction LR

      203[System Fingerprint]
      204[???]
      10[AES Key aesS]
      11[Decrypt Public Key with AES aesS]
      12[Decrypt AES Key aesR with RSA Public Key]
      13[Decrypt Environment File .env with AES aesR]
      14[RSA Public Key]
      15[AES Key aesR]
      16[.env File]

      6 --> 105
      7 --> 102
      8 --> 104
      3 --> 103
      203 --> 10
      204 --> 10
      10 --> 11
      102 --> 11
      11 --> 14
      104 --> 12
      14 --> 12
      12 --> 15
      105 --> 13
      15 --> 13
      13 --> 16
   end
```
