# Admin User

Der Admin-User im System ist ein Nutzer, der erweitere Rechte hat, um den Bot zu konfigurieren und zu steuern.
Primär interagiert dieser mit dem Bot über einen Privatchat.

Mehr infos über die Konfiguration des Bots kann in [doc/configuration.md](configuration.md) gefunden werden.

## Startup-Notification

Der Admin-User wird beim Start des Bots benachrichtigt, dass der Bot gestartet wurde.

```md
:white_check_mark: Bot started successfully in version `1.4.0`
```

## Error-Notification

Der Admin-User wird bei einem Fehler benachrichtigt, der während der Ausführung des Bots aufgetreten ist.

```md
:warning: Command-Error in: `Skyball's private server/verify-user/1239967176882913301`
User-Message: Fehler beim Verarbeiten des Befehls.
Reload command is not implemented yet.
Use `log -start 2024101335e96c63` to get more information
```

Dies führt uns auch direkt zum nächsten Thema: Befehle.

## Befehle

Befehle werden hier einfach ohne `/` eingegeben, Argumente mit einer `-argname arg value`-Syntax.
Dies ist der Output des `help`-Befehls:

Available commands:
- `help` - Show this help message
- `info` - Show bot information
- `log` - `[lines=<count>] [start=<substring|line-number>]` - Retrieve a certain number of lines from the latest log file with optional start conditions
- `download-update` - `<remote-url>` `<remote-sha256>` `<local-name>` - Download an update from the given URL, but do not apply it
- `update` - `<file-name>` - Apply the update from the given file and terminate the program
- `exit` - Exit the program, type `exit confirm` to confirm

Die meisten Befehle erklären sich selbst, auf einige wird im Folgenden eingegangen.

### Updated des Bots

Der Bot kann sich selbst aktualisieren, indem ein Update heruntergeladen und angewendet wird.
Dazu wird der `download-update`-Befehl verwendet, um das Update herunterzuladen, und der `update`-Befehl, um das Update anzuwenden.
In Folgenden ein Beispiel:

Erstellen des Patches:

```shell
mvn clean package
sha256sum target/FS-discordbot-1.4.0.jar
```

Nun muss dieses Paket auf einem Server bereitgestellt werden, von dem der Bot es herunterladen kann.

In Discord kann dieses Update dann heruntergeladen und angewendet werden:

```shell
download-update -remote-url https://example.com/FS-discordbot-1.4.0.jar -remote-sha256 1234567890abcdef -local-name FS-discordbot-1.4.0.jar
update -file-name FS-discordbot-1.4.0.jar
```

```md
download-update -remote-url ... -remote-sha256 ... -local-name FS-discordbot-1.4.0.jar

:white_check_mark: File downloaded and verified successfully:
`/home/yan/workspace/projects/FS-discordbot/index.html`

update -file-name FS-discordbot-1.4.0.jar

Applying update from file: `FS-discordbot-1.4.0.jar`
:white_check_mark: New process started successfully.
```

Wenn man die PID vom neuen Prozess haben will, einfach den `info`-Befehl ausführen.
