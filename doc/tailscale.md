### Configure SSH on the Server Machine

Ensure that the OpenSSH server or any other SSH server is installed and running.

```
sudo apt update
sudo apt install openssh-server
sudo systemctl enable ssh
sudo systemctl start ssh
```

_Optional, I did not:_

To enhance security and ensure compatibility with Tailscale, modify the SSH configuration file located
at `/etc/ssh/sshd_config`.
Add the following lines:

```
Match Address 100.x.x.x/32
  PasswordAuthentication no
  PermitRootLogin no
```

Reload the SSH configuration with `sudo systemctl reload sshd`.

### Install Tailscale on Both Machines

Begin by installing Tailscale on both the server and client machines.
Visit [tailscale.com/download](https://tailscale.com/download) and follow the installation instructions for your
operating systems.

### Authenticate Tailscale

After installation, start Tailscale on both machines.
Authenticate with the same Tailscale account on both devices by running the `tailscale up --ssh` command.
This will list your machine on https://login.tailscale.com/admin/machines.

On the server machine, ensure that Tailscale SSH is enabled.
Open the Tailscale configuration file at `/etc/tailscale/tailscaled.conf` and confirm or add the following lines:

```
[ssh]
enable = true
```

```
sudo systemctl enable tailscaled
sudo systemctl restart tailscaled
```

### Retrieve the Tailscale IP of the Server

On the server machine, run `tailscale ip` to get its Tailscale-assigned IP address.
The output will include an address such as `100.x.x.x`.

### Connect to the Server from Your Machine

On your client machine, use the Tailscale IP to connect to the server via SSH. 
eplace `<username>` with the username of the user account on the server:

```
ssh <username>@100.x.x.x  
```  

This establishes a secure SSH connection between the client and the server over the Tailscale network.

### Other

```shell
sudo systemctl stop tailscaled
sudo systemctl disable tailscaled
tailscale logout
```
