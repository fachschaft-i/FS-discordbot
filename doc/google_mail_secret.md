Partially ChatGPT-generated content, partially manually written.
This worked for me.

### Step-by-Step Guide to Obtain Google Mail Secrets

#### Set Up a Google Cloud Project

To start, create a new project on the Google Cloud Console. Visit the
[Google Cloud Console](https://console.cloud.google.com/), click on the project dropdown, and select "New Project".
Enter a project name and click "Create".

Next, enable the Gmail API. Navigate to `APIs & Services > Library`, search for "Gmail API", and click on it. Click
"Enable" to activate the API for your project.

#### Create OAuth 2.0 Credentials

Go to `APIs & Services > Credentials` and click "Create Credentials", selecting "OAuth client ID". If you haven't
already configured the OAuth consent screen, follow the prompts to set it up, filling in the required fields. Choose
"Web application" as the application type and set the authorized redirect URIs to: `http://localhost`.
You will either be able to download a JSON file with your credentials here or click "Create" to generate your
credentials.

You will receive a `client_id` and a `client_secret`. Here are the example values:

- **client_id**: `357211532501-qpsbk1a9hsgu838car0jsk229g8egant.apps.googleusercontent.com`
- **client_secret**: `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`

#### Configure Required Scopes

Ensure you have the appropriate scopes for accessing Gmail. The scopes you need are:

- `https://mail.google.com/`: This allows reading, writing, sending, and deleting Gmail emails.
- `.../auth/gmail.send`: This allows sending emails on your behalf.

#### Obtain the Refresh Token

First, generate the authorization URL with `access_type=offline` by opening this URL in your browser:

```
https://accounts.google.com/o/oauth2/auth?client_id=357211532501-qpsbk1a9hsgu838car0jsk229g8egant.apps.googleusercontent.com&redirect_uri=http://localhost&response_type=code&scope=https://mail.google.com/&access_type=offline
```

Grant permission when prompted. After authorization, you will be redirected to `http://localhost` with a code parameter
in the URL. For example, you might see something like:

```
http://localhost/?code=4/0AdLIrYc9NVYArQmkCK2otrZDWuf-SSVsOpKFOl_-xR4va7SSj7tZUajJmvTH3kPJedZKiw&scope=https://mail.google.com/
```

Copy the code from the URL and use it in the following cURL command to exchange it for an access token and refresh
token:

```bash
curl --request POST \
  --data "code=4/0AdLIrYc9NVYArQmkCK2otrZDWuf-SSVsOpKFOl_-xR4va7SSj7tZUajJmvTH3kPJedZKiw&client_id=357211532501-qpsbk1a9hsgu838car0jsk229g8egant.apps.googleusercontent.com&client_secret=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&redirect_uri=http://localhost&grant_type=authorization_code" \
  https://oauth2.googleapis.com/token
```

The response will include an `access_token` and a `refresh_token`. For example:

```json
{
  "access_token": "ya29.a0AXooCgvf0IHG11xPswwacLUGkZQhbuH6rwZF07lTHj-ri6rW3QCear6mKOAUBRF0_aaqr_JRbE7cNFK6V4kb-ggmAqBsty9YljUXa5NyTWAsECtcxgAJJOXIIDJV3voGWd5LUjFiUe5c1nKH2_eguvd0g8GDZojhb-a0aCgYKAXoSARASFQHGX2Miv9suhIF-a4yjv6rTYFHL9Q0171",
  "expires_in": 3599,
  "refresh_token": "1//03ZVEaEJJPj3MCgYIARAAGAMSNwF-L9IrX-n-jbYbTF1K7iM2mLIk1iYnSMcY2ckoOGPzY2XP7namz07PwL-QZ3sYZD5imxSkWRE",
  "scope": "https://mail.google.com/",
  "token_type": "Bearer"
}
```

#### Sender Email Address

Ensure you have an email address associated with your Google account that you will use to send emails. This email
address should be used in your configuration.

### Configuration Mapping

Use the obtained credentials to set up your configuration. Here are the values to use in your `.env` file:

```properties
MAIL_CLIENT_ID=357211532501-qpsbk1a9hsgu838car0jsk229g8egant.apps.googleusercontent.com
MAIL_CLIENT_SECRET=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
MAIL_REFRESH_TOKEN=1//03ZVEaEJJPj3MCgYIARAAGAMSNwF-L9IrX-n-jbYbTF1K7iM2mLIk1iYnSMcY2ckoOGPzY2XP7namz07PwL-QZ3sYZD5imxSkWRE
MAIL_SENDER_EMAIL_ADDRESS=your_email_address_here
```

Replace `your_email_address_here` with the actual email address you will use for sending emails.

By following these steps, you will have all the necessary details to configure your application for sending emails using
the Gmail API with OAuth 2.0.
