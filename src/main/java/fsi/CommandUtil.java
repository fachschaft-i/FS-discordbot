package fsi;

import fsi.listeners.verification.AdminPrivateChatListener;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.entities.channel.middleman.AudioChannel;
import net.dv8tion.jda.api.entities.channel.unions.MessageChannelUnion;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.StringSelectInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;

@Log4j2
public class CommandUtil {

    private static AdminPrivateChatListener adminPrivateChat;

    public static void setAdminPrivateChat(AdminPrivateChatListener adminPrivateChat) {
        CommandUtil.adminPrivateChat = adminPrivateChat;
    }

    public static String formatChannelName(SlashCommandInteractionEvent event) {
        return (event.getGuild() == null ? "null" : event.getGuild().getName()) + "/" + event.getChannel().getName() + "/" + event.getChannel().getId();
    }

    public static String formatChannelName(AudioChannel channel) {
        return channel.getGuild().getName() + "/" + channel.getName() + "/" + channel.getId();
    }

    public static String formatChannelName(MessageChannelUnion channel) {
        return channel.getName() + "/" + channel.getId();
    }

    public static String formatChannelName(StringSelectInteractionEvent event) {
        return event.getGuild().getName() + "/" + event.getChannel().getName() + "/" + event.getChannel().getId();
    }

    public static String formatChannelName(ButtonInteractionEvent event) {
        return event.getGuild().getName() + "/" + event.getChannel().getName() + "/" + event.getChannel().getId();
    }

    public static void logCommandError(SlashCommandInteractionEvent event, Exception e, String userMessage, boolean sendAsDirectResponse) {
        final String errorIdString = generateErrorId();
        if (sendAsDirectResponse) {
            event.getHook().sendMessage(
                            userMessage + "\n" +
                                    "Bitte melde dich bei einem Entwickler mit diesem Fehler-Code: `" + errorIdString + "`")
                    .setEphemeral(true).queue();
        } else {
            event.getChannel().sendMessage(
                            userMessage + "\n" +
                                    "Bitte melde dich bei einem Entwickler mit diesem Fehler-Code: `" + errorIdString + "`")
                    .queue();
        }
        log.error("[{}] Error handling command [{}] from user [{}] in [{}]",
                errorIdString, event.getName(), event.getUser().getName(), formatChannelName(event), e);
        CommandUtil.notifyAdminUserOfError("Command-Error in: `" + formatChannelName(event) + "`\nUser-Message: " + userMessage.replace("\n", "\\n"), errorIdString, e);
    }

    public static void logCommandError(MessageReceivedEvent event, Exception e, String userMessage) {
        final String errorIdString = generateErrorId();
        event.getChannel().sendMessage(
                        userMessage + "\n" +
                                "Bitte melde dich bei einem Entwickler mit diesem Fehler-Code: `" + errorIdString + "`")
                .queue();
        log.error("[{}] Error handling private chat message from user [{}]",
                errorIdString, event.getAuthor().getName(), e);
        CommandUtil.notifyAdminUserOfError("Command-Error in private chat with user: `" + event.getAuthor().getName() + "`\nUser-Message: " + userMessage.replace("\n", "\\n"), errorIdString, e);
    }

    public static void logCommandError(MessageChannelUnion channel, Exception e, String s) {
        final String errorIdString = generateErrorId();
        channel.sendMessage(
                        s + "\n" +
                                "Bitte melde dich bei einem Entwickler mit diesem Fehler-Code: `" + errorIdString + "`")
                .queue();
        log.error("[{}] Error handling command in [{}]", errorIdString, formatChannelName(channel), e);
        CommandUtil.notifyAdminUserOfError("Command-Error in: `" + formatChannelName(channel) + "`\n" + s, errorIdString, e);
    }

    public static void notifyAdminUserOfError(String message, String errorId, Exception e) {
        adminPrivateChat.sendAdminMessageWarning(message + "`\n" + e.getMessage() + "`\nUse `log -start " + errorId + "` to get more information");
    }

    private static String generateErrorId() {
        final UUID uuid = UUID.randomUUID();
        final String uuidPart = uuid.toString().substring(0, 8);

        final ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        final String datePart = now.getYear() + ""
                + (now.getMonthValue() <= 9 ? "0" + now.getMonthValue() : now.getMonthValue())
                + (now.getDayOfMonth() <= 9 ? "0" + now.getDayOfMonth() : now.getDayOfMonth());

        return datePart + uuidPart;
    }
}
