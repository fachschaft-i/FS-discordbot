package fsi.listeners.utility;

import fsi.CommandUtil;
import fsi.startup.ApplicationStartupLogger;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;

@Log4j2
public class HelpSlash extends ListenerAdapter implements ISlash {

    private final SlashManager manager;

    public HelpSlash(SlashManager manager) {
        this.manager = manager;
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName() + " as " + this.getName());
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        if (!event.getName().equals(this.getName())) {
            return;
        }

        try {
            final String commandName = event.getOption("command").getAsString();
            final ISlash command = manager.getCommand(commandName);

            if (command == null) {
                event.reply("Command nicht gefunden!").setEphemeral(true).queue();
                return;
            }

            final EmbedBuilder builder = new EmbedBuilder();
            builder.setTitle(command.getName());
            builder.setDescription(command.getHelp());

            event.replyEmbeds(builder.build()).queue();
        } catch (Exception e) {
            CommandUtil.logCommandError(event, e, "Fehler beim Anzeigen der Hilfe. Moment mal... wie hast du das denn geschafft?", false);
        }
    }

    @Override
    public SlashCommandData constructCommand() {
        final SlashCommandData help = Commands.slash(this.getName(), "Zeigt die einen Hilfe-Menu zu etwas an.");

        final OptionData commandHelpOption = new OptionData(OptionType.STRING, "command", "Wobei brauchst du denn Hilfe?", true);
        manager.getCommands()
                .forEach(command -> commandHelpOption.addChoice(command.getName(), command.getName()));
        help.addOptions(commandHelpOption);

        return help;
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getHelp() {
        return "... so weit, so offensichtlich?\nDer `help`-Befehl kann dir Anweisungen zu den anderen Befehlen geben.";
    }
}
