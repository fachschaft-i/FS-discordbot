package fsi.listeners.utility;

import fsi.CommandUtil;
import fsi.startup.ApplicationStartupLogger;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.StringSelectInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

@Log4j2
public class CommandLoggerListener extends ListenerAdapter {
    public CommandLoggerListener() {
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName());
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        final String user = event.getUser().getName();
        final String subcommandName = event.getSubcommandName();
        log.info("Received [{}] command from user [{}] in [{}] with subcommand [{}] and options {}",
                event.getName(), user,
                CommandUtil.formatChannelName(event),
                subcommandName, event.getOptions());
    }

    @Override
    public void onStringSelectInteraction(StringSelectInteractionEvent event) {
        final String user = event.getUser().getName();
        log.info("Received [{}] selection from user [{}] in [{}] with values {}",
                event.getComponentId(), user,
                CommandUtil.formatChannelName(event),
                event.getValues());
    }


    @Override
    public void onButtonInteraction(ButtonInteractionEvent event) {
        final String user = event.getUser().getName();
        log.info("Received [{}] button interaction from user [{}] in [{}] with values {}",
                event.getComponentId(), user,
                CommandUtil.formatChannelName(event),
                event.getComponentId());
    }
}
