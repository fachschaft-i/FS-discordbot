package fsi.listeners.utility;

import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;

import java.util.Arrays;
import java.util.List;

public interface ISlash {

    String getName();

    String getHelp();

    SlashCommandData constructCommand();

    default List<String> getAliases() {
        return Arrays.asList();
    }
}
