package fsi.listeners.utility;

import lombok.Getter;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

@Getter
public class SlashManager {

    private final List<ISlash> commands = new ArrayList<>();

    public List<CommandData> constructCommands() {
        List<CommandData> commandData = new ArrayList<>();
        for (ISlash command : commands) {
            commandData.add(command.constructCommand());
        }
        return commandData;
    }

    public void addCommand(ISlash cmd) {
        boolean nameFound = this.commands.stream().anyMatch((it) -> it.getName().equalsIgnoreCase(cmd.getName()));

        if (nameFound) {
            throw new IllegalArgumentException("Command existiert schon");
        }

        commands.add(cmd);
    }

    public void addCommands(ISlash... commands) {
        for (ISlash command : commands) {
            addCommand(command);
        }
    }

    @Nullable
    public ISlash getCommand(String search) {
        final String normalizedSearch = search.toLowerCase();

        return this.commands.stream()
                .filter(candidate -> candidate.getName().equals(normalizedSearch) || candidate.getAliases().contains(normalizedSearch))
                .findFirst()
                .orElse(null);

    }

    public JDABuilder registerCommands(JDABuilder jdaBuilder) {
        for (ISlash command : commands) {
            jdaBuilder.addEventListeners(command);
        }
        return jdaBuilder;
    }
}
