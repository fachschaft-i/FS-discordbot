package fsi.listeners.voice;

import fsi.CommandUtil;
import fsi.config.FsBotConfig;
import fsi.startup.ApplicationStartupLogger;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.Category;
import net.dv8tion.jda.api.entities.channel.middleman.AudioChannel;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceUpdateEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@Log4j2
public class VcListener extends ListenerAdapter {

    private static final List<String> IGNORE_VOICE_CHANNELS = new ArrayList<>();

    static {
        IGNORE_VOICE_CHANNELS.add(FsBotConfig.get(FsBotConfig.Attribute.VOICE_CHANNEL_ID_CREATE_CHANNEL));
        IGNORE_VOICE_CHANNELS.addAll(FsBotConfig.getCsvStringList(FsBotConfig.Attribute.VOICE_CHANNEL_IDS_IGNORE));
    }

    public VcListener() {
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName());
    }

    @Override
    public void onGuildVoiceUpdate(GuildVoiceUpdateEvent event) {
        final Member member = event.getMember();
        if (member.getUser().isBot()) {
            return;
        }

        try {
            handleVoiceChannelJoin(event);
            handleVoiceChannelLeave(event);
        } catch (Exception e) {
            log.error("Error while handling voice channel event", e);
        }
    }

    private void handleVoiceChannelJoin(GuildVoiceUpdateEvent event) {
        final AudioChannel channelJoined = event.getNewValue();
        if (shouldCreateNewVoiceChannel(channelJoined)) {
            log.info("[VC_LERNRAUM] User [{}] joined the Lernraum channel [{}]", event.getMember().getEffectiveName(), channelJoined.getId());
            createAndMoveToVoiceChannel(event.getGuild(), event.getMember(), channelJoined);
        }
    }

    private static boolean shouldCreateNewVoiceChannel(AudioChannel channelJoined) {
        return channelJoined != null && channelJoined.getId().equals(FsBotConfig.get(FsBotConfig.Attribute.VOICE_CHANNEL_ID_CREATE_CHANNEL));
    }

    private void createAndMoveToVoiceChannel(Guild guild, Member member, AudioChannel channelJoined) {
        final Category parent = channelJoined.getParentCategory();
        if (parent != null) {
            final AudioChannel createdAudioChannel = parent.createVoiceChannel(member.getEffectiveName() + "'s Lernraum")
                    .addMemberPermissionOverride(member.getIdLong(), EnumSet.of(Permission.MANAGE_CHANNEL), null)
                    .complete();
            guild.moveVoiceMember(member, createdAudioChannel).queue();
        }
    }

    private void handleVoiceChannelLeave(GuildVoiceUpdateEvent event) {
        final AudioChannel channelLeft = event.getOldValue();
        if (shouldDeleteChannel(channelLeft)) {
            deleteChannelIfEmpty(channelLeft);
        }
    }

    private boolean shouldDeleteChannel(AudioChannel channel) {
        if (channel == null) {
            return false;
        }
        return channel.getName().contains("Lernraum") && !IGNORE_VOICE_CHANNELS.contains(channel.getId());
    }

    private void deleteChannelIfEmpty(AudioChannel channel) {
        if (channel.getMembers().isEmpty()) {
            log.info("[VC_LERNRAUM] Lernraum Channel [{}] is empty, removing it", CommandUtil.formatChannelName(channel));
            channel.delete().queue();
        } else {
            log.info("[VC_LERNRAUM] Lernraum Channel [{}] with [{}] members is not yet empty, not removing it yet",
                    CommandUtil.formatChannelName(channel), channel.getMembers().size());
        }
    }
}

