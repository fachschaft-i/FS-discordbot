package fsi.listeners.timetable;

import fsi.CommandUtil;
import fsi.listeners.utility.ISlash;
import fsi.service.timetable.ProfessorScraper;
import fsi.service.timetable.TimetableScraper;
import fsi.startup.ApplicationStartupLogger;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;

import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Log4j2
public class TimetableInfoSlash extends ListenerAdapter implements ISlash {

    private final ProfessorScraper professorScraper;
    private final TimetableScraper timetableScraper;

    private JDA jda;

    public TimetableInfoSlash(TimetableScraper timetableScraper, ProfessorScraper professorScraper) {
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName() + " as " + this.getName());
        this.timetableScraper = timetableScraper;
        this.professorScraper = professorScraper;
    }

    public void setup(JDA jda) {
        this.jda = jda;
    }

    public synchronized void ensureCache() {
        this.professorScraper.ensureCache();
        this.timetableScraper.ensureCache();
    }

    public synchronized void clearCache() {
        this.professorScraper.clearCache();
        this.timetableScraper.clearCache();
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        if (!event.getName().equals(this.getName())) {
            return;
        }

        event.deferReply().queue();

        final String subcommand = event.getSubcommandName();
        if (subcommand == null) {
            log.warn("No subcommand received for [{}] command.", this.getName());
            event.reply("Du musst einen der Subcommands auswählen!").setEphemeral(true).queue();
            return;
        }

        try {
            switch (subcommand) {
                case "prof":
                    handleProfCommand(event);
                    break;
                case "listprofs":
                    handleListProfsCommand(event);
                    break;
                case "freeroom":
                    handleFreeRoomCommand(event);
                    break;
                case "reload":
                    handleReloadCommand(event);
                    break;
                case "quelle":
                    event.getHook().sendMessage("Die Daten für die Stundenpläne und Professoren werden von der offiziellen Website der Hochschule Mannheim abgerufen.\n" +
                            "- [Campusplan](https://www.hs-mannheim.de/fileadmin/user_upload/hauptseite/Bilder/campusplan/campusplan_de.pdf)\n" +
                            "- [Raumliste](https://services.informatik.hs-mannheim.de/stundenplan/?cookie=raum)\n" +
                            "- [Professoren](https://www.hs-mannheim.de/professorenliste/professoren.php?suchen=&alle=l%C3%B6schen)\n" +
                            "- [Professoren (I)](https://www.informatik.hs-mannheim.de/professoren.html)\n" +
                            "- [Lehrveranstaltungspläne](https://services.informatik.hs-mannheim.de/stundenplan/)\n"
                    ).queue();
                    break;
                default:
                    log.warn("Unknown subcommand '{}' received for [{}] command.", subcommand, this.getName());
                    event.reply("Unbekannter Subcommand erhalten.").setEphemeral(true).queue();
                    break;
            }
        } catch (Exception e) {
            CommandUtil.logCommandError(event, e, "Fehler beim Verarbeiten des Befehls.", true);
        }
    }

    private void handleProfCommand(SlashCommandInteractionEvent event) {
        final String profName = event.getOption("name").getAsString();

        final List<TimetableScraper.TimetableEntry> timetables;
        try {
            timetables = timetableScraper.findEntriesForProfessor(profName);
        } catch (IOException e) {
            CommandUtil.logCommandError(event, e, "Fehler beim Abrufen der Stundenpläne für Professor " + profName, true);
            return;
        }

        final List<ProfessorScraper.Professor> profInfos;
        final ProfessorScraper.Professor profInfo;
        try {
            profInfos = professorScraper.findEntriesForProfessor(profName);
            profInfo = professorScraper.findEntryForProfessor(profName);
        } catch (IOException e) {
            CommandUtil.logCommandError(event, e, "Fehler beim Abrufen der Informationen für Professor " + profName, true);
            return;
        }

        if (timetables.isEmpty() && profInfo == null) {
            event.getHook().sendMessage("Keine Informationen gefunden für **" + profName + "**.").queue();
            return;
        }

        final EmbedBuilder embed = new EmbedBuilder();
        embed.setColor(Color.BLUE);

        final String multipleProfVariantsWarning;
        if (profInfos.size() >= 2) {
            final StringBuilder warning = new StringBuilder();
            warning.append("Es wurden mehrere Professoren gefunden:\n");
            for (ProfessorScraper.Professor prof : profInfos) {
                warning.append("- ")
                        .append(makeOptionalLink(prof.getFullName(), prof.getPersonalWebsite()))
                        .append(" (").append(prof.getShortName()).append(")")
                        .append(prof.getFaculty().isEmpty() ? "" : " - Fakultät " + String.join("/", prof.getFaculty()))
                        .append("\n");
            }
            warning.append("Der erste Professor der liste wurde für die Anzeige oben ausgewählt.\n" +
                    "Um einen anderen Professor anzuzeigen, gib den vollständigen Namen oder das Kürzel an.");
            multipleProfVariantsWarning = warning.toString();
        } else {
            multipleProfVariantsWarning = null;
        }

        if (profInfo != null) {
            final String title = profInfo.getFullName() + " (" + profInfo.getShortName() + ")";
            embed.setTitle(title, profInfo.getPersonalWebsite());

            if (!profInfo.getFaculty().isEmpty()) {
                embed.setDescription("Fakultät " + String.join("/", profInfo.getFaculty()));
            }

            if (profInfo.getRoom() != null) {
                embed.addField("Raum", profInfo.getRoom(), true);
            }
            if (profInfo.getSpecialFunction() != null) {
                embed.addField("Besondere Funktion", profInfo.getSpecialFunction(), true);
            }

            final StringBuilder contactInfo = new StringBuilder();
            if (profInfo.getContact() != null) {
                contactInfo.append(profInfo.getContact());
            }
            if (profInfo.getEmail() != null) {
                if (contactInfo.length() > 0) contactInfo.append("\n");
                contactInfo.append(profInfo.getEmail());
            }
            if (contactInfo.length() > 0) {
                embed.addField("Kontakt", contactInfo.toString(), false);
            }

            if (profInfo.getConsultationHours() != null) {
                embed.addField("Sprechstunden", profInfo.getConsultationHours(), false);
            }

            if (PROF_SHORT_NAME_TO_IMAGE_URL.containsKey(profInfo.getShortName())) {
                embed.setThumbnail(PROF_SHORT_NAME_TO_IMAGE_URL.get(profInfo.getShortName()));
            }
        } else {
            embed.setTitle("Informationen für " + profName);
        }

        if (!timetables.isEmpty()) {
            final Map<String, List<TimetableScraper.TimetableEntry>> mergedEntries =
                    TimetableScraper.TimetableEntry.mergeSimilarEntries(
                            TimetableScraper.TimetableEntry.sortEntriesByTime(timetables)
                    );

            for (List<TimetableScraper.TimetableEntry> entries : mergedEntries.values()) {
                final TimetableScraper.TimetableEntry firstEntry = entries.get(0);

                final Map<String, Set<String>> timeInfo = new LinkedHashMap<>();

                for (TimetableScraper.TimetableEntry entry : entries) {
                    timeInfo.computeIfAbsent("**" + weekday(entry.getWeekday()).substring(0, 2) + "**, " + entry.getStart() + " - " + entry.getEnd() + " Uhr", k -> new HashSet<>())
                            .add(entry.getSemesterShortName());
                }

                embed.addField(
                        firstEntry.getLectureShortName() + ", " + firstEntry.getBuilding() + firstEntry.getRoom().replaceAll("[A-Za-z]+", "") + " (" + firstEntry.getLectureLongName() + ")",
                        timeInfo.entrySet().stream()
                                .map(e -> e.getKey() + " (" + String.join(", ", e.getValue()) + ")")
                                .collect(Collectors.joining("\n")) + "\n",
                        false
                );
            }
        }

        event.getHook().sendMessageEmbeds(embed.build()).queue();
        if (multipleProfVariantsWarning != null) {
            event.getHook().sendMessage(multipleProfVariantsWarning).queue();
        }
    }

    public void handleListProfsCommand(SlashCommandInteractionEvent event) {
        final List<ProfessorScraper.Professor> profs;
        try {
            profs = professorScraper.findEntries(e -> true);
        } catch (IOException e) {
            CommandUtil.logCommandError(event, e, "Fehler beim Abrufen der Liste aller Professoren.", true);
            return;
        }

        if (profs.isEmpty()) {
            event.getHook().sendMessage("Keine Professoren gefunden.").queue();
            return;
        }

        profs.sort(Comparator
                .comparing((ProfessorScraper.Professor p) -> p.getFaculty().isEmpty())
                .thenComparing((ProfessorScraper.Professor p) -> p.getFaculty().isEmpty() ? "" : String.join("/", p.getFaculty()))
                .thenComparing(ProfessorScraper.Professor::getShortName)
                .thenComparing(ProfessorScraper.Professor::getFullName));

        final List<String> response = new ArrayList<>();
        response.add("Liste aller Professoren:\n");
        for (ProfessorScraper.Professor prof : profs) {
            response.add("- " +
                    prof.getFullName() +
                    " (" + prof.getShortName() + ")" +
                    (prof.getFaculty().isEmpty() ? "" : " - Fakultät " + String.join("/", prof.getFaculty())) +
                    (prof.getPersonalWebsite() == null ? "" : " <" + prof.getPersonalWebsite() + ">") +
                    "\n");
        }

        // max length of a message is 2000 characters
        final List<String> messages = new ArrayList<>();
        final StringBuilder currentMessage = new StringBuilder();
        for (String line : response) {
            if (currentMessage.length() + line.length() > 2000) {
                messages.add(currentMessage.toString());
                currentMessage.setLength(0);
            }
            currentMessage.append(line);
        }
        if (currentMessage.length() > 0) {
            messages.add(currentMessage.toString());
        }

        for (String message : messages) {
            event.getHook().sendMessage(message).queue();
        }
    }

    private void handleFreeRoomCommand(SlashCommandInteractionEvent event) {
        final String building = event.getOption("building").getAsString();
        String time = event.getOption("time").getAsString();
        final int weekday = Integer.parseInt(event.getOption("weekday").getAsString());

        // input correction
        if (time.matches("\\d:\\d{2}")) {
            time = "0" + time;
        } else if (time.matches("\\d")) {
            time = "0" + time + ":00";
        } else if (time.matches("\\d{2}")) {
            time = time + ":00";
        }

        if (time.length() != 5 || !time.matches("\\d{2}:\\d{2}")) {
            event.getHook().sendMessage("Ungültiges Zeitformat. Bitte gib die Zeit im Format HH:mm an.").queue();
            return;
        }

        final Set<String> knownRooms = KNOWN_ROOMS_PER_BUILDING.getOrDefault(building, Collections.emptyList()).stream().map(r -> building + r).collect(Collectors.toSet());
        try {
            final List<TimetableScraper.TimetableEntry> entries = timetableScraper.findEntries(e -> e.getBuilding().equals(building));
            for (TimetableScraper.TimetableEntry entry : entries) {
                knownRooms.add(entry.getBuilding() + entry.getRawRoomNumber());
            }
        } catch (IOException e) {
            CommandUtil.logCommandError(event, e, "Fehler beim Abrufen der Stundenpläne für den freien Raum (Raumidentifikation).", true);
            return;
        }

        final List<String> freeRoomsAtGivenTime = new ArrayList<>(knownRooms);
        try {
            final List<TimetableScraper.TimetableEntry> entries = timetableScraper.findEntries(e -> e.getWeekday() == weekday && e.getBuilding().equals(building));
            for (TimetableScraper.TimetableEntry entry : entries) {
                if (entry.isTimeSlotOccupied(time)) {
                    freeRoomsAtGivenTime.remove(entry.getBuilding() + entry.getRawRoomNumber());
                }
            }
        } catch (IOException e) {
            CommandUtil.logCommandError(event, e, "Fehler beim Abrufen der Stundenpläne für den freien Raum (Überprüfung der Belegung).", true);
            return;
        }

        if (freeRoomsAtGivenTime.isEmpty()) {
            event.getHook().sendMessage("Keine freien Räume gefunden für " + weekday(weekday) + " um " + time + " Uhr in Gebäude " + building + ".").queue();
            return;
        }

        freeRoomsAtGivenTime.sort(String::compareTo);

        final Map<String, String> nextPossessionTime = new HashMap<>();
        try {
            final List<TimetableScraper.TimetableEntry> entries = timetableScraper.findEntries(e -> e.getWeekday() == weekday && e.getBuilding().equals(building));
            for (String freeRoom : freeRoomsAtGivenTime) {
                for (TimetableScraper.TimetableEntry entry : entries) {
                    if ((entry.getBuilding() + entry.getRawRoomNumber()).equals(freeRoom)) {
                        final String nextStartTime = entry.getStart().trim();

                        // check if time is after the check time
                        if (nextStartTime.compareTo(time) <= 0) {
                            continue;
                        }

                        // if no next possession time is set yet, simply set it
                        if (!nextPossessionTime.containsKey(freeRoom)) {
                            nextPossessionTime.put(freeRoom, nextStartTime);
                        } else {
                            // if the next possession time is later than the current one, update it
                            if (nextStartTime.compareTo(nextPossessionTime.get(freeRoom)) < 0) {
                                nextPossessionTime.put(freeRoom, nextStartTime);
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            CommandUtil.logCommandError(event, e, "Fehler beim Abrufen der Stundenpläne für den freien Raum (Überprüfung der Belegung).", true);
            return;
        }

        final StringJoiner fullyFreeRooms = new StringJoiner(", ");
        final StringJoiner partiallyFreeRooms = new StringJoiner("\n");
        for (String room : freeRoomsAtGivenTime) {
            final String nextPossession = nextPossessionTime.get(room);
            if (nextPossession != null) {
                partiallyFreeRooms.add("- " + room + " (frei bis " + nextPossession + " Uhr)");
            } else {
                fullyFreeRooms.add(room);
            }
        }

        final StringBuilder response = new StringBuilder();
        response.append("Freie Räume in Gebäude ").append(building).append(" am ").append(weekday(weekday)).append(" um ").append(time).append(" Uhr:\n");
        if (fullyFreeRooms.length() > 0) {
            response.append("- ").append(fullyFreeRooms).append("\n");
        }
        if (partiallyFreeRooms.length() > 0) {
            response.append(partiallyFreeRooms);
        }

        event.getHook().sendMessage(response.toString()).queue();
    }

    private String makeOptionalLink(String linkText, String url) {
        if (url == null || url.isEmpty()) {
            return linkText;
        }
        return "[" + linkText + "](" + url + ")";
    }

    private String weekday(int index) {
        switch (index) {
            case 0:
                return "Montag";
            case 1:
                return "Dienstag";
            case 2:
                return "Mittwoch";
            case 3:
                return "Donnerstag";
            case 4:
                return "Freitag";
            case 5:
                return "Samstag";
            case 6:
                return "Sonntag";
            default:
                return "Unbekannt";
        }
    }

    private void handleReloadCommand(SlashCommandInteractionEvent event) {
        if (!event.getMember().hasPermission(Permission.BAN_MEMBERS) && !event.getMember().hasPermission(Permission.KICK_MEMBERS)) {
            log.warn("User [{} / {}] tried to reload the timetable cache without proper permissions.", event.getMember().getId(), event.getMember().getEffectiveName());
            event.getHook().sendMessage("**Du bist nicht berechtigt**, um einen Reload des Stundenplan-Caches durchzuführen.").setEphemeral(true).queue();
            return;
        }

        event.getHook().sendMessage("Lösche und lade den Stundenplan-Cache neu. **Das kann bis zu 15 Minuten dauern!**").queue();

        new Thread(() -> {
            try {
                clearCache();
            } catch (Exception e) {
                CommandUtil.logCommandError(event, e, "Fehler beim Löschen des Caches für Stundenpläne und Professoren.", true);
                return;
            }
            try {
                ensureCache();
            } catch (Exception e) {
                CommandUtil.logCommandError(event, e, "Fehler beim Laden des Caches für Stundenpläne und Professoren.", true);
                return;
            }

            event.getHook().sendMessage("Stundenplan-Cache wurde erfolgreich neu geladen!").queue();
        }).start();
    }

    @Override
    public SlashCommandData constructCommand() {
        return Commands.slash(this.getName(), "Zeigt Informationen zu den Stundenplänen von Professoren und Modulen.")
                .addSubcommands(
                        new SubcommandData("reload", "Löscht und lädt den Cache für Stundenpläne und Professoren neu.")
                )
                .addSubcommands(
                        new SubcommandData("prof", "Zeigt Informationen zu einem Professor.")
                                .addOptions(
                                        new OptionData(OptionType.STRING, "name", "Der volle Name oder Kürzel des Professors.").setRequired(true)
                                )
                )
                .addSubcommands(
                        new SubcommandData("listprofs", "Listet alle Professoren auf.")
                )
                .addSubcommands(
                        new SubcommandData("freeroom", "Findet für einen Timeslot einen freien Raum in einem Gebäude.")
                                .addOptions(
                                        new OptionData(OptionType.STRING, "time", "HH:mm - Die Uhrzeit, zu der der freie Raum gesucht werden soll.").setRequired(true),
                                        new OptionData(OptionType.STRING, "weekday", "Der Wochentag, an dem der freie Raum gesucht werden soll.").setRequired(true)
                                                .addChoices(
                                                        new Command.Choice("Montag", "0"),
                                                        new Command.Choice("Dienstag", "1"),
                                                        new Command.Choice("Mittwoch", "2"),
                                                        new Command.Choice("Donnerstag", "3"),
                                                        new Command.Choice("Freitag", "4"),
                                                        new Command.Choice("Samstag", "5"),
                                                        new Command.Choice("Sonntag", "6")
                                                ),
                                        // building is an enum of [A-Z]
                                        new OptionData(OptionType.STRING, "building", "Das Gebäude, in dem der freie Raum gesucht werden soll.").setRequired(true)
                                                .addChoices(IntStream.rangeClosed('A', 'Z')
                                                        .filter(e -> e != 'I')
                                                        .mapToObj(i -> {
                                                            final String str = String.valueOf((char) i);
                                                            final String extended = EXTENDED_BUILDING_DESCRIPTIONS.get(str);
                                                            return new Command.Choice(str + (extended == null ? "" : " - " + extended), str);
                                                        })
                                                        .collect(Collectors.toList()))

                                )
                )
                .addSubcommands(
                        new SubcommandData("quelle", "Zeigt die Quelle der Daten an.")
                );
    }

    @Override
    public String getName() {
        return "find";
    }

    @Override
    public String getHelp() {
        return "Zeigt Informationen zu den Stundenplänen von Professoren und Modulen.";
    }

    final static Map<String, String> PROF_SHORT_NAME_TO_IMAGE_URL = new HashMap<>();

    static {
        PROF_SHORT_NAME_TO_IMAGE_URL.put("DOP", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/a/6/csm_dopatka-frank-300px_e7984c5d16.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("FIM", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/1/0/csm_fimmel-elena-300px_6b980b152c.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("FIJ", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/9/d/csm_fischer_joern-300px_09f4d7a3c5.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("FOL", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/e/8/csm_foeller-nord-miriam-300px_fb3818f286.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("GMI", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/8/8/csm_groeschel-michael-300px_c6c013b0df.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("GUM", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/d/0/csm_gumbel_markus-300px_ef2084f6f1.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("HUM", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/9/6/csm_hummel_oliver-300px_402b719a7e.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("IME", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/b/8/csm_ihme-thomas-300px_2b1db24445.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("KAE", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/6/8/csm_peter-Kaiser-300px_4de422bc12.jpg");
        // PROF_SHORT_NAME_TO_IMAGE_URL.put("KLS", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/e/5/csm_avatar-klaus-sven-300px_6639fe6301.jpg"); // generic
        PROF_SHORT_NAME_TO_IMAGE_URL.put("KNU", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/b/8/csm_peter-knauber-300px_2f7993e981.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("KOK", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/d/1/csm_kohler_kirstin-300px_2a57d264c5.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("KST", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/3/7/csm_kraus_stefan-300px_6f7c4862f8.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("LET", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/4/c/csm_leuchter_sandro-300px_296752a8b9.jpg");
        // PROF_SHORT_NAME_TO_IMAGE_URL.put("MRM", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/9/1/csm_avatar-maennlich-300px_bcc90e51a5.jpg"); // generic
        PROF_SHORT_NAME_TO_IMAGE_URL.put("NAT", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/9/f/csm_nagel_till-300px_9c5ea62c08.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("PLS", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/f/5/csm_sachar_paulus-300px_764830418e.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("ROD", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/d/8/csm_roth-dietrich_gabriele-300px_3f7f9becf7.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("SCA", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/3/e/csm_schramm_wolfgang-300px_d1bb332e04.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("SGB", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/3/2/csm_steger-sebastian-300px_6f1d56bda3.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("SMI", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/e/4/csm_smits_thomas-300px_f684521029.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("SPE", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/e/2/csm_specht_thomas-300px_459980cd7d.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("STL", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/8/9/csm_struengmann-lutz-300px_e85c8284df.jpg");
        PROF_SHORT_NAME_TO_IMAGE_URL.put("WOI", "https://www.informatik.hs-mannheim.de/fileadmin/_processed_/3/1/csm_wolf_ivo-300px_c9a70b0ae5.jpg");
    }

    final static Map<String, List<String>> KNOWN_ROOMS_PER_BUILDING = new HashMap<>();

    static {
        // https://services.informatik.hs-mannheim.de/stundenplan/?cookie=raum
        KNOWN_ROOMS_PER_BUILDING.put("A", Arrays.asList("117", "008", "010", "011", "012", "107", "108", "111", "206", "208", "210", "211", "212", "305", "307", "309", "311", "001"));
        KNOWN_ROOMS_PER_BUILDING.put("E", Arrays.asList("001", "203"));
        KNOWN_ROOMS_PER_BUILDING.put("F", Arrays.asList("001"));
        KNOWN_ROOMS_PER_BUILDING.put("G", Arrays.asList("005", "013", "021", "026", "032", "035", "041", "045", "054", "103", "111", "118", "127", "130", "133", "137", "211", "213", "217"));
        KNOWN_ROOMS_PER_BUILDING.put("H", Arrays.asList("0307", "0406A", "0407", "0408", "0506", "0507", "0706", "0806", "0807", "0808", "0809", "1006", "1007", "1008", "1009", "1010", "1107", "1114"));
        KNOWN_ROOMS_PER_BUILDING.put("K", Arrays.asList("019", "106"));
        KNOWN_ROOMS_PER_BUILDING.put("L", Arrays.asList("011", "012", "013", "014", "015", "110", "111", "112", "113", "114", "206", "207", "208", "209", "210", "215", "303", "304", "305", "306"));
        KNOWN_ROOMS_PER_BUILDING.put("MED", Arrays.asList("U"));
        KNOWN_ROOMS_PER_BUILDING.put("O", Arrays.asList("100", "101", "102", "103"));
        KNOWN_ROOMS_PER_BUILDING.put("R", Arrays.asList("010", "108", "300"));
        KNOWN_ROOMS_PER_BUILDING.put("S", Arrays.asList("012", "016_017", "017", "113", "117", "211", "212", "213", "218", "220", "221", "322"));
        KNOWN_ROOMS_PER_BUILDING.put("U", Arrays.asList("020", "106a", "106b", "204", "210", "216"));
    }

    final static Map<String, String> EXTENDED_BUILDING_DESCRIPTIONS = new HashMap<>();

    static {
        // https://www.hs-mannheim.de/fileadmin/user_upload/hauptseite/Bilder/campusplan/campusplan_de.pdf
        EXTENDED_BUILDING_DESCRIPTIONS.put("A", "Informatik, Maschinenbau, Rektorat");
        EXTENDED_BUILDING_DESCRIPTIONS.put("B", "Elektrotechnik, Maschinenbau");
        EXTENDED_BUILDING_DESCRIPTIONS.put("C", "Gestaltung, Sozialwesen, Aula");
        EXTENDED_BUILDING_DESCRIPTIONS.put("D", "Biotechnologie");
        EXTENDED_BUILDING_DESCRIPTIONS.put("E", "Verfahrens- und Chemietechnik");
        EXTENDED_BUILDING_DESCRIPTIONS.put("F", "Elektrotechnik");
        EXTENDED_BUILDING_DESCRIPTIONS.put("G", "Biotechnologie, Maschinenbau, Verfahrens- und Chemietechnik");
        EXTENDED_BUILDING_DESCRIPTIONS.put("H", "Biotechnologie, Maschinenbau, Verfahrens- und Chemietechnik, StudentenService, Rechenzentrum");
        EXTENDED_BUILDING_DESCRIPTIONS.put("I", "International Office");
        EXTENDED_BUILDING_DESCRIPTIONS.put("J", "International Office, Mensa");
        EXTENDED_BUILDING_DESCRIPTIONS.put("K", "Biotechnologie, Maschinenbau, Wirtschaftsingenieurwesen");
        EXTENDED_BUILDING_DESCRIPTIONS.put("L", "Gestaltung, Wirtschaftsingenieurwesen, Bibliothek");
        EXTENDED_BUILDING_DESCRIPTIONS.put("M", "Mensa");
        EXTENDED_BUILDING_DESCRIPTIONS.put("O", null);
        EXTENDED_BUILDING_DESCRIPTIONS.put("P", null);
        EXTENDED_BUILDING_DESCRIPTIONS.put("Q", "Maschinenbau, Turbo Academy");
        EXTENDED_BUILDING_DESCRIPTIONS.put("R", "Maschinenbau, Informationstechnik");
        EXTENDED_BUILDING_DESCRIPTIONS.put("S", "Informationstechnik, Verfahrens- und Chemietechnik");
        EXTENDED_BUILDING_DESCRIPTIONS.put("T", "Maschinenbau, Kompetenzzentrum Tribologie");
        EXTENDED_BUILDING_DESCRIPTIONS.put("U", "Verfahrens- und Chemietechnik");
        EXTENDED_BUILDING_DESCRIPTIONS.put("V", null);
        EXTENDED_BUILDING_DESCRIPTIONS.put("W", null);
        EXTENDED_BUILDING_DESCRIPTIONS.put("X", null);
        EXTENDED_BUILDING_DESCRIPTIONS.put("Y", null);
        EXTENDED_BUILDING_DESCRIPTIONS.put("Z", "Gestaltung");

    }
}
