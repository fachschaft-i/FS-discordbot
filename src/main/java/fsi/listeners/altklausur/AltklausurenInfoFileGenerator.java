package fsi.listeners.altklausur;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class AltklausurenInfoFileGenerator {

    public void generateInfoFile(File directory, String nowDate, String userName, List<String> requestIds, List<String> modules) throws IOException {
        final StringBuilder infoFileContent = new StringBuilder();

        final String shortenedUserName;
        if (userName.length() > 22) {
            shortenedUserName = userName.substring(0, 22 - 3) + "...";
        } else {
            shortenedUserName = userName;
        }

        infoFileContent
                .append("\n=^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=\n\n")
                .append("Hallo ").append(userName).append("!\n")
                .append("Du hast am ").append(nowDate).append(" die folgenden Altklausuren angefordert:\n\n- ")
                .append(String.join("\n- ", modules)).append("\n\n")
                .append("Wenn du nach der Prüfungsphase deine Prüfungen abgeschlossen hast, würden wir uns freuen,\n" +
                        "wenn du deine eigenen Altklausuren und Gedankenprotokolle mit \"/altklausuren upload\" hochlädst.\n" +
                        "Das wäre eine große Hilfe für die Studenten der kommenden Semester.\n\n");

        if (modules.isEmpty()) {
            infoFileContent
                    .append("Warte... Du hast gar keine Module heruntergeladen. Das sollte gar nicht gehen.\n")
                    .append("Das erklärt auch, warum die Liste da oben so kaputt aussieht. Hmm.\n")
                    .append("Melde dich einfach mal bei der Fachschaft-I, damit wir das Problem beheben können.\n\n");
        }

        final AsciiArt asciiArt = new AsciiArt();
        final double[][] asciiArtBuffer = AsciiArt.generateBuffer(asciiArt);
        final String asciiArtString = asciiArt.renderAsciiArt(asciiArtBuffer);
        final String sammelkartenId = UUID.nameUUIDFromBytes(asciiArtString.getBytes()).toString().substring(0, 8);

        infoFileContent
                .append("Als Dank, dass du den Altklausuren-Service der Fachschaft-I verwendest,\n")
                .append("bekommst du auch eine persönlich für dich generierte ASCII-Art-Sammelkarte!\n\n");

        final String[] asciiArtLines = asciiArtString.split("\n");
        infoFileContent.append("   ╔══════════════════════════════════════════════════════════════╗\n");
        for (int i = 0; i < asciiArtLines.length; i++) {
            infoFileContent.append("   ║ ");
            infoFileContent.append(asciiArtLines[i]);
            infoFileContent.append(" ║\n");
        }
        infoFileContent.append("   ╠════════════════════════╦════════════════════════╦════════════╣\n");
        final int remainingUserChars = 22 - shortenedUserName.length();
        final int addonRemainingUserChars = remainingUserChars % 2;
        final String sammelkartenIdContent = "ASCII-Art-ID: " + sammelkartenId + " ║ " + repeat(" ", remainingUserChars / 2) + shortenedUserName + repeat(" ", remainingUserChars / 2 + addonRemainingUserChars) + " ║ " + nowDate;
        infoFileContent.append("   ║ ").append(sammelkartenIdContent).append(" ║\n");
        infoFileContent.append("   ╚════════════════════════╩════════════════════════╩════════════╝");
        infoFileContent.append("\n\n=^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=  =^..^=\n");

        final File infoFile = new File(directory, "download-info.txt");
        final OutputStream fileOutputStream = new BufferedOutputStream(Files.newOutputStream(infoFile.toPath()));
        fileOutputStream.write(infoFileContent.toString().getBytes());
        fileOutputStream.close();
    }

    private String repeat(String s, int n) {
        return String.valueOf(new char[n]).replace("\0", s);
    }

    public static void main(String[] args) throws IOException {
        final AltklausurenInfoFileGenerator generator = new AltklausurenInfoFileGenerator();
        final File directory = new File("target");
        final String nowDate = "2021-08-01";
        final String userName = "skyball";
        final List<String> requestIds = Arrays.asList("1", "2", "3");
        final List<String> modules = Arrays.asList("PR3 (Unbekannter Professor)", "MA2 (Todorov)");
        generator.generateInfoFile(directory, nowDate, userName, requestIds, modules);
    }
}
