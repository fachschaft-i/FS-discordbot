package fsi.listeners.altklausur;

import lombok.Data;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Setter
public class AsciiArt {
    private final String ASCII_LOOKUP = " .'^,:;Il!i><~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao#MW&8%B@$";
    private final int ASCII_LEN = ASCII_LOOKUP.length();
    private final int WIDTH = 60;
    private final int HEIGHT = 20;
    private Random random;

    private double[][] buffer;
    private double[][] attractionBuffer;
    private double totalAttraction = 0;

    private double attractionDistanceScale = 1;
    private int attractionStrengthTransferFunction = 1;

    private int amountOfAttractorsPoint = 0;
    private int amountOfAttractorsLine = 0;
    private int amountOfAttractorsCircle = 2;

    private List<StoredBuffer> storedBuffers = new ArrayList<>();

    @Data
    public static class StoredBuffer {
        private final double[][] buffer;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            AsciiArt asciiArt = new AsciiArt();
            double[][] outputBuffer = generateBuffer(asciiArt);
            asciiArt.printBuffer(outputBuffer, "output-buffer");
        }
    }

    public static double[][] generateBuffer(AsciiArt asciiArt) {
        final Random random = new Random();

        final int shapeTypes = random.nextInt(6);
        if (shapeTypes == 0) {
            asciiArt.setAmountOfAttractorsLine(2);
            asciiArt.setAmountOfAttractorsCircle(0);
        } else if (shapeTypes == 1) {
            asciiArt.setAmountOfAttractorsPoint(random.nextInt(6) + 2);
            asciiArt.setAmountOfAttractorsCircle(0);
            asciiArt.setAmountOfAttractorsLine(0);
        } else if (shapeTypes == 2) {
            asciiArt.setAmountOfAttractorsCircle(4);
        }

        if (random.nextInt(7) == 0) {
            asciiArt.setAttractionStrengthTransferFunction(4);
            asciiArt.setAmountOfAttractorsLine(1);
        }

        asciiArt.setAttractionDistanceScale(random.nextDouble() * 0.5 + 0.5);

        final double[][] outputBuffer = asciiArt.run(random);

        if (random.nextInt(3) != 0 || asciiArt.storedBuffers.isEmpty()) {
            return outputBuffer;
        } else {
            final StoredBuffer storedBuffer = asciiArt.storedBuffers.get(random.nextInt(asciiArt.storedBuffers.size()));
            return storedBuffer.getBuffer();
        }
    }

    public double[][] run(Random random) {
        this.random = random;

        buffer = initializeBuffer();
        int attemptsForAttractionBuffer = 0;
        do {
            attractionBuffer = initializeAttractionBuffer();
        } while (this.totalAttraction < 40 && attemptsForAttractionBuffer++ < 10);

        // printBuffer(buffer);
        // printBuffer(attractionBuffer);

        evolvePattern(buffer);

        // printBuffer(attractionBuffer);
        // printBuffer(buffer);

        return buffer;
    }

    private double[][] initializeBuffer() {
        double seed = random.nextDouble() * 100;
        double[][] buffer = new double[WIDTH][HEIGHT];
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                buffer[x][y] = generateInitialPattern(x, y, seed);
            }
        }
        return buffer;
    }

    private double generateInitialPattern(int x, int y, double seed) {
        double[] uv = {(double) x / WIDTH, (double) y / HEIGHT};

        double patternA = Math.sin(uv[0] * 20 + seed) * Math.cos(uv[1] * 20 + seed);
        double patternB = Math.sin(uv[0] * 10 + seed) * Math.cos(uv[1] * 10 + seed);
        double combinedPattern = patternA * 0.5 + patternB * 0.5;
        double normalizedPattern = (combinedPattern + 1) / 2;

        double contrastPattern;
        if (normalizedPattern < 0.5) {
            contrastPattern = Math.pow(normalizedPattern, 2);
        } else {
            contrastPattern = 1 - Math.pow(1 - normalizedPattern, 2);
        }

        return contrastPattern;
    }

    private double[][] initializeAttractionBuffer() {
        double[][] attractionBuffer = new double[WIDTH][HEIGHT];
        addRandomAttractors(attractionBuffer, amountOfAttractorsPoint, amountOfAttractorsLine, amountOfAttractorsCircle);

        this.totalAttraction = 0;
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                this.totalAttraction += attractionBuffer[i][j];
            }
        }

        return attractionBuffer;
    }

    private void addRandomAttractors(double[][] attractionBuffer, int numPoints, int numLines, int numCircles) {
        // Add random points
        for (int i = 0; i < numPoints; i++) {
            int x = random.nextInt(WIDTH);
            int y = random.nextInt(HEIGHT);
            attractionBuffer[x][y] += random.nextDouble() * 0.5 + 0.5; // Random strength between 0.5 and 1.0
        }

        // Add random lines
        for (int i = 0; i < numLines; i++) {
            int x1 = random.nextInt(WIDTH);
            int y1 = random.nextInt(HEIGHT);
            int x2 = random.nextInt(WIDTH);
            int y2 = random.nextInt(HEIGHT);
            double strength = random.nextDouble() * 0.3 + 0.3; // Random strength between 0.3 and 0.6
            addLineToAttractionBuffer(attractionBuffer, x1, y1, x2, y2, strength);
        }

        // Add random circles
        for (int i = 0; i < numCircles; i++) {
            int x = random.nextInt(WIDTH);
            int y = random.nextInt(HEIGHT);
            int radius = random.nextInt(Math.min(WIDTH, HEIGHT) / 2);
            double strength = random.nextDouble() * 0.5 + 0.5; // Random strength between 0.5 and 1.0
            addCircleToAttractionBuffer(attractionBuffer, x, y, radius, strength);
        }
    }

    private void addLineToAttractionBuffer(double[][] attractionBuffer, int x1, int y1, int x2, int y2, double strength) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        double distance = Math.sqrt(dx * dx + dy * dy);
        for (int x = 0; x < WIDTH; x++) {
            for (int y = 0; y < HEIGHT; y++) {
                double distToLine = Math.abs((dy * (x - x1)) - (dx * (y - y1))) / distance;
                if (distToLine < 1.0) {
                    attractionBuffer[x][y] += (1.0 - distToLine) * strength;
                }
            }
        }
    }

    private void addCircleToAttractionBuffer(double[][] attractionBuffer, int cx, int cy, int radius, double strength) {
        for (int x = Math.max(0, cx - radius); x < Math.min(WIDTH, cx + radius); x++) {
            for (int y = Math.max(0, cy - radius); y < Math.min(HEIGHT, cy + radius); y++) {
                double distance = Math.sqrt((x - cx) * (x - cx) + (y - cy) * (y - cy));
                if (distance < radius) {
                    attractionBuffer[x][y] += (1.0 - (distance / radius)) * strength;
                }
            }
        }
    }

    private void evolvePattern(double[][] buffer) {
        int iterations = random.nextInt(10) + 10;
        // int iterations = 10;

        for (int i = 0; i < iterations; i++) {
            double[][] newBuffer = new double[WIDTH][HEIGHT];
            copyBuffer(newBuffer, buffer);

            double[][] attractionStrengthBuffer = new double[WIDTH][HEIGHT];
            double[][] moveToBuffer = new double[WIDTH][HEIGHT];
            double[][] moveFromBuffer = new double[WIDTH][HEIGHT];

            for (int y = 0; y < HEIGHT; y++) {
                for (int x = 0; x < WIDTH; x++) {
                    double[] attractionVector = calculateAttractionVector(x, y);

                    double attractionVectorLength = Math.sqrt(attractionVector[0] * attractionVector[0] + attractionVector[1] * attractionVector[1]);
                    double attractionStrength = 1 - attractionVectorLength;

                    int xModif = (int) (attractionVector[0] * WIDTH * attractionDistanceScale);
                    int yModif = (int) (attractionVector[1] * HEIGHT * attractionDistanceScale);
                    int newX = x + xModif;
                    int newY = y + yModif;
                    attractionStrengthBuffer[x][y] = attractionStrength;

                    if (newX >= 0 && newX < WIDTH && newY >= 0 && newY < HEIGHT) {
                        newBuffer[newX][newY] += buffer[x][y] * attractionStrength;
                        moveToBuffer[newX][newY] += buffer[x][y] * attractionStrength;
                    }
                    newBuffer[x][y] -= buffer[x][y] * attractionStrength;
                    moveFromBuffer[x][y] = buffer[x][y] * attractionStrength;
                }
            }

            copyBuffer(buffer, newBuffer);

            // printBuffer(attractionStrengthBuffer, "attraction-strength");
            // printBuffer(moveToBuffer, "move-to");
            // printBuffer(moveFromBuffer, "move-from");

            // printBuffer(buffer);

            if (i > 3 && this.random.nextInt() > 0.7) {
                this.storedBuffers.add(new StoredBuffer(moveToBuffer));
                this.storedBuffers.add(new StoredBuffer(moveFromBuffer));
            }
        }
    }

    private double[] calculateAttractionVector(int x, int y) {
        double[] attractionVector = new double[2];

        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                double attraction = attractionBuffer[i][j];
                double dx = i - x;
                double dy = j - y;
                double distance = Math.sqrt(dx * dx + dy * dy);

                double normalizedAttraction = attraction / this.totalAttraction;

                double strength;
                if (attractionStrengthTransferFunction == 1) {
                    strength = 1 / (distance + 1);
                } else if (attractionStrengthTransferFunction == 2) {
                    strength = 1 / ((distance * distance) / 2);
                } else if (attractionStrengthTransferFunction == 3) {
                    strength = 1 / Math.pow(normalizedAttraction, 1.6);
                } else if (attractionStrengthTransferFunction == 4) {
                    if (distance < 5) {
                        strength = 1;
                    } else if (distance < 10) {
                        strength = 0.5;
                    } else {
                        strength = 0.0;
                    }
                } else {
                    throw new IllegalArgumentException("Invalid attractionStrengthTransferFunction value: " + attractionStrengthTransferFunction);
                }

                attractionVector[0] += dx * normalizedAttraction * strength;
                attractionVector[1] += dy * normalizedAttraction * strength;
            }
        }

        return attractionVector;
    }

    private void copyBuffer(double[][] buffer, double[][] newBuffer) {
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                buffer[x][y] = newBuffer[x][y];
            }
        }
    }

    private void printBuffer(double[][] buffer) {
        String name = "???";
        if (buffer == attractionBuffer) {
            name = "attraction-buffer";
        } else if (buffer == this.buffer) {
            name = "buffer";
        }
        printBuffer(buffer, name);
    }

    private void printBuffer(double[][] buffer, String name) {
        final String pbuffer = renderAsciiArt(buffer);
        System.out.print(pbuffer);
        System.out.println(" ~ " + name);
        System.out.println();
    }

    public String renderAsciiArt(double[][] buffer) {
        StringBuilder pbuffer = new StringBuilder();
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                double value = Math.min(1.0, Math.max(0.0, buffer[x][y]));
                char cchar = ASCII_LOOKUP.charAt((int) (value * (ASCII_LEN - 1)));
                pbuffer.append(cchar);
            }
            if (y < HEIGHT - 1) {
                pbuffer.append("\n");
            }
        }
        return pbuffer.toString();
    }
}
