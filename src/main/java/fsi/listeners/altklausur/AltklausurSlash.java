package fsi.listeners.altklausur;

import fsi.CommandUtil;
import fsi.config.FsBotConfig;
import fsi.listeners.utility.ISlash;
import fsi.service.altklausur.AltklausurAccess;
import fsi.service.altklausur.LowLevelNextcloudAccess;
import fsi.startup.ApplicationStartupLogger;
import fsi.startup.Bot;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static fsi.startup.BuildProperties.loadClassResourceAsString;

@Log4j2
public class AltklausurSlash extends ListenerAdapter implements ISlash {

    // 10 minutes
    private static final long LIST_COMMAND_DISPLAY_DURATION = 10 * 60 * 1000;
    private static final int MAX_DOWNLOAD_CODES = 10;

    private final AltklausurAccess altklausurAccess;
    private final AltklausurenInfoFileGenerator infoFileGenerator;

    private JDA jda;

    public AltklausurSlash(AltklausurAccess altklausurAccess) {
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName() + " as " + this.getName());
        this.altklausurAccess = altklausurAccess;
        this.infoFileGenerator = new AltklausurenInfoFileGenerator();
    }

    public void setup(JDA jda) {
        this.jda = jda;
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        if (!event.getName().equals(this.getName())) {
            return;
        }

        event.deferReply().queue();

        if (!ensureConnectionToNextcloudExists(event)) {
            return;
        }

        final String subcommand = event.getSubcommandName();
        if (subcommand == null) {
            log.warn("No subcommand received for AltklausurSlash command.");
            event.reply("Du musst einen der Subcommands auswählen!").setEphemeral(true).queue();
            return;
        }

        switch (subcommand) {
            case "list":
                handleListCommand(event);
                break;
            case "download":
                handleDownloadCommand(event);
                break;
            case "refresh":
                handleRefreshCommand(event);
                break;
            case "notify":
                handleNotifyCommand(event);
                break;
            case "upload":
                handleUploadCommand(event);
                break;
            default:
                log.warn("Unknown subcommand '{}' received for AltklausurSlash command.", subcommand);
                event.reply("Unbekannter Subcommand erhalten.").setEphemeral(true).queue();
                break;
        }
    }

    private boolean ensureConnectionToNextcloudExists(SlashCommandInteractionEvent event) {
        // check if ALTKL_NEXTCLOUD_BASE_URL can be accessed
        try {
            altklausurAccess.getLowLevelNextcloudAccess().assertConnectionValid();
            return true;
        } catch (ConnectException e) {
            log.error("Could not connect to Nextcloud server: {}", e.getMessage());
            event.getHook().sendMessage("Der File-Server scheint zurzeit nicht erreichbar zu sein. Bitte versuche es später erneut.").queue();
            return false;
        }
    }

    private void handleListCommand(SlashCommandInteractionEvent event) {
        try {
            altklausurAccess.ensureCache();
        } catch (AltklausurAccess.AltklausurCacheCreationException e) {
            event.getHook().sendMessage("Der lokale Cache des File-Servers muss aktualisiert werden.").queue();
            CommandUtil.logCommandError(event, e, "Es scheint allerdings, dass der File-Server zurzeit nicht erreichbar ist. Versuche es doch später noch einmal.", false);
            return;
        }

        try {
            final Map<AltklausurAccess.ModuleEntry, List<AltklausurAccess.ModuleProfEntry>> modulesWithProfs = altklausurAccess.getCachedModuleProfs();
            final List<String> response = new ArrayList<>();

            for (Map.Entry<AltklausurAccess.ModuleEntry, List<AltklausurAccess.ModuleProfEntry>> entry : modulesWithProfs.entrySet()) {
                final AltklausurAccess.ModuleEntry module = entry.getKey();
                final List<AltklausurAccess.ModuleProfEntry> profs = entry.getValue();
                final StringJoiner charLimitBuilder = new StringJoiner("\n");
                charLimitBuilder.add("- `" + module.getUniqueIdentifier() + "` **" + module.getShortModuleName() + "**: " + module.getLongModuleName());

                if (profs != null && !profs.isEmpty()) {
                    for (AltklausurAccess.ModuleProfEntry prof : profs) {
                        charLimitBuilder.add("  * `" + prof.getUniqueIdentifier() + "` " + prof.getProfDisplayName());
                    }
                }
                response.add(charLimitBuilder.toString());
            }

            response.add("\nWas für eine Liste! Kopiere dir alle `kurz-ids` der Module oder Profs, die du herunterladen möchtest.");
            try {
                final AltklausurAccess.ModuleEntry exampleModule = getRandomCollectionEntry(modulesWithProfs.keySet());
                final AltklausurAccess.ModuleProfEntry exampleProf = getRandomCollectionEntry(modulesWithProfs.values().stream().flatMap(List::stream).filter(AltklausurAccess.ModuleProfEntry::isKnown).collect(Collectors.toList()));
                response.add("Wenn du zum Beispiel alle Klausuren zum Modul **" + exampleModule.getShortModuleName() + "** und zusätzlich im Modul **" + exampleProf.getModule().getShortModuleName() + "** nur die vom Prof **" + exampleProf.getProfDisplayName() + "** herunterladen möchtest:");
                response.add("`/altklausuren download ids:" + exampleModule.getUniqueIdentifier() + " " + exampleProf.getUniqueIdentifier() + " agreement:true`");
            } catch (Exception e) {
                log.warn("Could not generate example command for Altklausuren list command.", e);
                response.add("Und benutze `/altklausuren download ids:<kurz-id-modul> <kurz-id-prof> agreement:true` um die Altklausuren herunterzuladen.");
            }
            response.add("Du musst `agreement:true` angeben, um den Download durchzuführen. Damit wirst du am Ende des Semesters daran erinnert, deine eigenen Altklausuren einzusenden.");

            // Discord has a 2000-character limit for messages, split the response if necessary
            StringJoiner charLimitBuilder = new StringJoiner("\n");
            boolean isFirst = true;
            for (String element : response) {
                if (charLimitBuilder.length() + element.length() >= 2000 - 2) {
                    final String message = charLimitBuilder.toString();
                    charLimitBuilder = new StringJoiner("\n");
                    log.info("Sending partial message with length: {}", message.length());

                    if (isFirst) {
                        isFirst = false;
                        event.getHook().sendMessage(message).queue(msg ->
                                msg.delete().queueAfter(LIST_COMMAND_DISPLAY_DURATION, TimeUnit.MILLISECONDS)
                        );
                        // Discord seems to publish the other chat messages earlier than the hook message, so wait a bit
                        Thread.sleep(500);
                    } else {
                        // to prevent multiple bot-user-icons in the chat, send the next message as a regular message in the channel
                        event.getChannel().sendMessage(message).queue(msg ->
                                msg.delete().queueAfter(LIST_COMMAND_DISPLAY_DURATION, TimeUnit.MILLISECONDS)
                        );
                    }
                }
                charLimitBuilder.add(element);
            }
            if (charLimitBuilder.length() > 0 && charLimitBuilder.length() < 2000) {
                if (isFirst) {
                    event.getHook().sendMessage(charLimitBuilder.toString()).queue(msg ->
                            msg.delete().queueAfter(LIST_COMMAND_DISPLAY_DURATION, TimeUnit.MILLISECONDS)
                    );
                } else {
                    // to prevent multiple bot-user-icons in the chat, send the next message as a regular message in the channel
                    event.getChannel().sendMessage(charLimitBuilder.toString()).queue(msg ->
                            msg.delete().queueAfter(LIST_COMMAND_DISPLAY_DURATION, TimeUnit.MILLISECONDS)
                    );
                }
            }
        } catch (Exception e) {
            CommandUtil.logCommandError(event, e, "Beim Abrufen der Module ist ein Fehler aufgetreten.", false);
        }
    }

    private void handleRefreshCommand(SlashCommandInteractionEvent event) {
        try {
            altklausurAccess.invalidateCache();
            altklausurAccess.ensureCache();
            event.getHook().sendMessage("Altklausuren-Cache wurde aktualisiert!").queue();
        } catch (AltklausurAccess.AltklausurCacheCreationException e) {
            CommandUtil.logCommandError(event, e, "Es scheint, dass der File-Server zurzeit nicht erreichbar ist. Versuche es doch später noch einmal.", false);
        } catch (Exception e) {
            CommandUtil.logCommandError(event, e, "Beim Aktualisieren der Altklausuren ist ein Fehler aufgetreten.", false);
        }
    }

    private void handleDownloadCommand(SlashCommandInteractionEvent event) {
        try {
            altklausurAccess.ensureCache();
        } catch (AltklausurAccess.AltklausurCacheCreationException e) {
            event.getHook().sendMessage("Der lokale Cache des File-Servers muss aktualisiert werden.").queue();
            CommandUtil.logCommandError(event, e, "Es scheint allerdings, dass der File-Server zurzeit nicht erreichbar ist. Versuche es doch später noch einmal.", false);
            return;
        }

        final String ids = event.getOption("ids").getAsString();
        if (ids.isEmpty()) {
            event.getHook().sendMessage("Du musst mindestens eine ID für Module oder Profs angeben!").queue();
            return;
        }
        final boolean agreeToNextGen = event.getOption("agreement").getAsBoolean();
        if (!agreeToNextGen) {
            event.getHook().sendMessage("Du musst mit `agreement:true` zustimmen, dass dein Discord-Name bis zum Ende des Semesters gespeichert wird, um dich dann nach deinen eigenen Altklausuren oder Gedankenprotokollen zu fragen!").queue(msg ->
                    msg.delete().queueAfter(30, TimeUnit.SECONDS)
            );
            return;
        }
        final boolean loggingEnabled = event.getOption("nolog") == null || !event.getOption("nolog").getAsBoolean();

        try {
            final List<String> idList = Arrays.asList(ids.split("[^a-zA-Z0-9]+"));
            if (idList.size() > MAX_DOWNLOAD_CODES) {
                event.getHook().sendMessage("Du kannst maximal `" + MAX_DOWNLOAD_CODES + "` IDs gleichzeitig angeben.").queue();
                return;
            }

            final Set<AltklausurAccess.ModuleProfEntry> allProfModules;
            final Set<String> failedToParseIds = new HashSet<>();
            try {
                allProfModules = idList.stream()
                        .map(id -> {
                            final List<AltklausurAccess.ModuleProfEntry> moduleProfs = altklausurAccess.findCachedModuleByIdOrName(id.trim());
                            if (moduleProfs.isEmpty()) {
                                failedToParseIds.add(id);
                            }
                            return moduleProfs;
                        })
                        .flatMap(List::stream)
                        .collect(Collectors.toSet());

                if (!failedToParseIds.isEmpty()) {
                    postNotFoundErrorMessage(event, failedToParseIds);
                    return;
                }
            } catch (Exception e) {
                log.warn("Could not parse IDs {} for Altklausuren download command: {}", ids, e.getMessage());
                postNotFoundErrorMessage(event, failedToParseIds);
                return;
            }

            final User user = event.getUser();
            log.info("User [{}] requested download of modules: {} --> {}", user.getAsTag(), idList,
                    allProfModules.stream().map(moduleProfEntry -> moduleProfEntry.getUniqueIdentifier() + ":" + moduleProfEntry.getModule().getShortModuleName() + ":" + moduleProfEntry.getProf()).collect(Collectors.joining(", ")));

            event.getHook().sendMessage("Dein Download für die Altklausuren wird jetzt vorbereitet, das kann eine Weile dauern. Du bekommst dann eine DM mit dem Download-Link zugeschickt.").queue();

            final String downloadFileId = LowLevelNextcloudAccess.randomAlphabeticString(5);
            final File tempDir = Bot.obtainTempFile("altklausuren_" + downloadFileId);

            new Thread(() -> {
                try {
                    for (AltklausurAccess.ModuleProfEntry profEntry : allProfModules) {
                        final AltklausurAccess.ModuleEntry module = profEntry.getModule();

                        final File moduleDir = new File(tempDir, module.getShortModuleName());
                        moduleDir.mkdirs();

                        final File profDir = new File(moduleDir, profEntry.getProfDisplayName());
                        profDir.mkdirs();

                        try {
                            altklausurAccess.getLowLevelNextcloudAccess().downloadNextcloudItem(profEntry.getFileItem().getPathWithoutBase(), profDir);
                        } catch (ConnectException e) {
                            log.error("Could not download file for module/prof: {} / {}", module.getShortModuleName(), profEntry.getProfDisplayName(), e);
                            if (loggingEnabled) {
                                event.getChannel().sendMessage("Der File-Server scheint zurzeit nicht erreichbar zu sein. Bitte versuche es später erneut.").queue();
                            }
                            return;
                        } catch (Exception e) {
                            log.error("Could not download file for module/prof: {} / {}", module.getShortModuleName(), profEntry.getProfDisplayName(), e);
                        }
                    }

                    final String nowDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                    final Map<String, Set<String>> moduleProfForMessageMap = allProfModules.stream()
                            .collect(Collectors.groupingBy(moduleProfEntry -> moduleProfEntry.getModule().getShortModuleName(), Collectors.mapping(AltklausurAccess.ModuleProfEntry::getProfDisplayName, Collectors.toSet())));

                    try {
                        this.infoFileGenerator.generateInfoFile(tempDir, nowDate, user.getEffectiveName(), idList, moduleProfForMessageMap.entrySet().stream()
                                .map(entry -> entry.getKey() + " (" + String.join(", ", entry.getValue()) + ")")
                                .collect(Collectors.toList()));
                    } catch (IOException e) {
                        log.error("Could not generate info file for user, skipping file: {}", user.getEffectiveName(), e);
                    }

                    final String remoteFileName = nowDate + "_" + downloadFileId + ".zip";
                    altklausurAccess.getLowLevelNextcloudAccess().compressAndUploadDirectory(tempDir, remoteFileName);

                    final String expirationDate = LocalDate.now().plusDays(7).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                    final LowLevelNextcloudAccess.FileShareLink shareLink = altklausurAccess.getLowLevelNextcloudAccess().createShareLinkForUploadedZipCreatePassword(remoteFileName, expirationDate);

                    final String moduleProfMessage = moduleProfForMessageMap.entrySet().stream()
                            .map(entry -> "**" + entry.getKey() + "** (" + String.join(", ", entry.getValue()) + ")")
                            .collect(Collectors.joining(", "));

                    final String publicChatMessage = "Dein Altklausuren-Download ist nun bereit! Die Zugangsdaten wurden dir privat zugeschickt.\n" +
                            "Im Export enthalten sind: " + moduleProfMessage;
                    final String privateChatMessage = "Hier die Zugangsdaten zu deinen Altklausuren für die Module/Profs: " + moduleProfMessage + "\n" +
                            "Share-Link: " + shareLink.getUrl() + "\n" +
                            "Passwort: `" + shareLink.getPassword() + "`\n" +
                            "Gültig bis: " + expirationDate + "\n\n" +
                            "Hey, wenn du diesen Service nützlich findest, dann verwende doch `/altklausuren upload`, um auch deine Altklausuren zu teilen!\n" +
                            "Als Dankeschön gibt es ein gratis Getränk in der Fachschaft! 🥤 🎉\n" +
                            "Viel Erfolg beim Lernen! 📚 🎓";
                    if (loggingEnabled) {
                        event.getChannel().sendMessage(publicChatMessage).queue();
                    }
                    user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessage(privateChatMessage).queue());

                    new Thread(() -> {
                        try {
                            this.altklausurAccess.appendUserQueryToLogFile(user, idList, allProfModules);
                        } catch (IOException e) {
                            log.error("Could not append user query to remote log file.", e);
                        }
                    }).start();
                } catch (Exception e) {
                    CommandUtil.logCommandError(event, e, "Beim Hochladen der Altklausuren ist ein Fehler aufgetreten.", false);
                }
            }).start();
        } finally {
            if (!loggingEnabled) {
                event.getHook().deleteOriginal().queue();
            }
        }
    }

    private void postNotFoundErrorMessage(SlashCommandInteractionEvent event, Set<String> failedToParseIds) {
        final String joinedIds = failedToParseIds.stream().map(id -> id.isEmpty() ? "<empty>" : id).map(id -> "`" + id + "`").collect(Collectors.joining(", "));
        final List<Pair<String, String>> mostLikelyStringIdCodes = failedToParseIds.stream().map(this::findMostLikelyStringIdCode).filter(pair -> pair.getFirst() != null).collect(Collectors.toList());

        final StringBuilder errorMessage = new StringBuilder("Es scheint, als ob keine Module oder Profs für die gegebenen IDs gefunden werden konnten: " + joinedIds);
        if (!mostLikelyStringIdCodes.isEmpty()) {
            errorMessage.append("\nMeintest du vielleicht:");
            for (Pair<String, String> pair : mostLikelyStringIdCodes) {
                errorMessage.append("\n- `").append(pair.getFirst()).append("` für ").append(pair.getSecond());
            }
        }

        event.getHook().sendMessage(errorMessage.toString()).queue();
    }

    @Data
    private static class Pair<T, U> {
        private final T first;
        private final U second;
    }

    private Pair<String, String> findMostLikelyStringIdCode(String id) {
        final String idLower = id.toLowerCase();
        final Map<String, String> allIds = new HashMap<>();

        for (AltklausurAccess.ModuleEntry module : this.altklausurAccess.getCachedModules()) {
            allIds.put(module.getUniqueIdentifier(), module.getShortModuleName());
            for (AltklausurAccess.ModuleProfEntry prof : this.altklausurAccess.getCachedModuleProfs().get(module)) {
                allIds.put(prof.getUniqueIdentifier(), module.getShortModuleName() + " / " + prof.getProfDisplayName());
            }
        }

        final Map<String, Integer> levenshteinDistances = new HashMap<>();
        for (Map.Entry<String, String> entry : allIds.entrySet()) {
            final String entryLower = entry.getKey().toLowerCase();
            final int distance = LevenshteinDistance.getDefaultInstance().apply(idLower, entryLower);
            levenshteinDistances.put(entry.getKey(), distance);
        }

        final List<Map.Entry<String, Integer>> sortedDistances = levenshteinDistances.entrySet().stream()
                .sorted(Comparator.comparingInt(Map.Entry::getValue))
                .collect(Collectors.toList());

        final Map.Entry<String, Integer> bestMatch = sortedDistances.get(0);
        if (bestMatch.getValue() > 3) {
            return new Pair<>(null, null);
        }

        return new Pair<>(bestMatch.getKey(), allIds.get(bestMatch.getKey()));
    }

    private void handleNotifyCommand(SlashCommandInteractionEvent event) {
        if (!event.getMember().hasPermission(Permission.BAN_MEMBERS) && !event.getMember().hasPermission(Permission.KICK_MEMBERS)) {
            log.warn("User [{} / {}] tried to notify users to send in their altklausuren.", event.getMember().getId(), event.getMember().getEffectiveName());
            event.getHook().sendMessage("**Du bist nicht berechtigt**, alle Nutzer zu benachrichtigen!**").setEphemeral(true).queue();
            return;
        }

        final String fromDateString = event.getOption("from").getAsString();
        final String toDateString = event.getOption("to").getAsString();
        final boolean simulate = event.getOption("simulate").getAsBoolean();
        final boolean youSure = event.getOption("you-sure").getAsBoolean();
        final boolean adminOnlyMsg = event.getOption("admin-only").getAsBoolean();

        if (!fromDateString.matches("\\d{4}-\\d{2}-\\d{2}") || !toDateString.matches("\\d{4}-\\d{2}-\\d{2}")) {
            event.getHook().sendMessage("Die Datumsangaben müssen im Format `yyyy-MM-dd` sein.").queue();
            return;
        }
        final int startYear = Integer.parseInt(fromDateString.substring(0, 4));
        final int startMonth = Integer.parseInt(fromDateString.substring(5, 7));
        final int startDay = Integer.parseInt(fromDateString.substring(8, 10));
        final int endYear = Integer.parseInt(toDateString.substring(0, 4));
        final int endMonth = Integer.parseInt(toDateString.substring(5, 7));
        final int endDay = Integer.parseInt(toDateString.substring(8, 10));

        final TextChannel altklausurenChannel = this.jda.getTextChannelById(FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_CHANNEL));
        if (altklausurenChannel == null) {
            log.error("Could not find channel with ID: {}", FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_CHANNEL));
            event.getHook().sendMessage("Der Channel für die Altklausuren konnte nicht gefunden werden.").queue();
            return;
        }

        final List<AltklausurAccess.AltklausurUserQueryResponse> userQueries = this.altklausurAccess.parseRequestedModules();

        final Map<User, Set<String>> userModules = new HashMap<>();
        int isNotWithinDateRange = 0;
        int invalidUserCount = 0;

        for (AltklausurAccess.AltklausurUserQueryResponse userQuery : userQueries) {
            // userQuery.getUserId() // <@508601845363507200>
            // userQuery.getUserName() // skyball
            // userQuery.getUserEffectiveName() // Skyball

            if (!userQuery.isWithinDateRange(LocalDate.of(startYear, startMonth, startDay), LocalDate.of(endYear, endMonth, endDay))) {
                isNotWithinDateRange++;
                continue;
            }

            final String properUserId = userQuery.getUserId().replaceAll("[^0-9]", "");
            final User user = jda.getUserById(properUserId);

            if (user == null) {
                invalidUserCount++;
                continue;
            }

            final Set<String> userModuleSet = userModules.computeIfAbsent(user, k -> new HashSet<>());
            userModuleSet.addAll(userQuery.getRequestedModules());
        }

        if (userModules.isEmpty()) {
            event.getHook().sendMessage("Es wurden keine Anfragen für Altklausuren in dem angegebenen Zeitraum gefunden.").queue();
            return;
        }

        if (simulate || !youSure) {
            event.getHook().sendMessage(
                    "Es wurden `" + userModules.size() + "` Nutzer gefunden, die im Zeitraum von `" + fromDateString + "` bis `" + toDateString + "` nach Altklausuren gefragt haben.\n" +
                            (isNotWithinDateRange > 0 || invalidUserCount > 0 ? ("Einträge:\n- außerhalb des Datumsbereichs: " + isNotWithinDateRange + "\n- Nutzer, in der Range, die nicht mehr auf dem Server sind: " + invalidUserCount + "\n") : "") +
                            "Um tatsächlich alle Nutzer zu benachrichtigen, setze `simulate:false` und `you-sure:true`."
            ).queue();
            return;
        }

        final String privateChatMessagePattern;
        try {
            privateChatMessagePattern = loadClassResourceAsString("/messages/template-altklausuren-notify-user-private.txt");
        } catch (IOException e) {
            CommandUtil.logCommandError(event, e, "Could not load private chat message template for Altklausuren notify command.", false);
            return;
        }

        int wroteToUserCount = 0;
        int failedToWriteToUserCount = 0;
        for (Map.Entry<User, Set<String>> entry : userModules.entrySet()) {
            final User user = entry.getKey();
            final Set<String> modules = entry.getValue();

            if (adminOnlyMsg) {
                final String adminUserId = FsBotConfig.get(FsBotConfig.Attribute.ADMIN_USER_ID);
                if (adminUserId == null) {
                    log.error("Could not find admin user ID in config: {}", FsBotConfig.Attribute.ADMIN_USER_ID);
                    event.getHook().sendMessage("Die Admin-User-ID konnte nicht gefunden werden.").queue();
                    return;
                }
                if (!user.getId().equals(adminUserId)) {
                    continue;
                }
            }

            if (modules.isEmpty()) {
                continue;
            }

            log.info("Notifying user [{}] to send in their altklausuren: {}", user.getEffectiveName(), modules);

            try {
                // "module, module and module"
                final String moduleString = modules.size() == 1 ? modules.iterator().next() : modules.stream().limit(modules.size() - 1).collect(Collectors.joining(", ")) + " und " + modules.stream().skip(modules.size() - 1).findFirst().orElse("");

                final String privateChatMessage = privateChatMessagePattern
                        .replace("[[CHANNEL]]", altklausurenChannel.getAsMention())
                        .replace("[[USERNAME]]", user.getEffectiveName())
                        .replace("[[MODULES]]", moduleString);
                user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessage(privateChatMessage).queue());
            } catch (Exception e) {
                log.error("Could not send private message to user [{}]: {}", user.getEffectiveName(), e.getMessage());
                failedToWriteToUserCount++;
            }

            wroteToUserCount++;
        }

        event.getHook().sendMessage("`" + wroteToUserCount + "/" + userModules.size() + "` Nutzer wurden benachrichtigt, ihre Altklausuren einzureichen." + (failedToWriteToUserCount > 0 ? " `" + failedToWriteToUserCount + "` Nachrichten konnten nicht gesendet werden." : "")).queue();
    }

    public void handleUploadCommand(SlashCommandInteractionEvent event) {
        final User user = event.getUser();
        final LowLevelNextcloudAccess.FileShareLink uploadFileUrl;
        try {
            uploadFileUrl = altklausurAccess.createUserUploadDirectoryForAltklausuren();
        } catch (Exception e) {
            CommandUtil.logCommandError(event, e, "Beim Erstellen des Upload-Links für die Altklausuren ist ein Fehler aufgetreten.", false);
            event.getHook().sendMessage("AAAH! Ein Fehler! Help!!!").queue();
            return;
        }

        try {
            final String publicChatMessage = "Danke für dein Interesse, deine Altklausuren für die nächste Generation zu teilen! Du hast eine private Nachricht mit dem Upload-Link erhalten.";
            final String privateChatMessage = "Du hast einen Link zum Teilen deiner Altklausuren und Gedankenprotokolle angefragt. Vielen Dank dafür!\n" +
                    "Bitte **trenne deine Dateien in mehrere Ordner pro Modul/Prof**, um es den Fachschaftsmitgliedern einfacher zu machen, diese einzusortieren.\n" +
                    "Dein Upload ist natürlich anonym, du kannst aber gerne eine Datei mit deinem Namen hinterlassen oder deinen Namen in die einzelnen Dateien hinzufügen, um dich so zu verewigen.\n\n" +
                    "Upload-Link: " + uploadFileUrl.getUrl() + "\n" +
                    "Passwort: `" + uploadFileUrl.getPassword() + "`\n\n" +
                    "Vielen Dank für deine Unterstützung! 🥤 🎉";

            event.getHook().sendMessage(publicChatMessage).queue();
            user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessage(privateChatMessage).queue());
        } catch (Exception e) {
            CommandUtil.logCommandError(event, e, "Beim Erstellen des Upload-Links für die Altklausuren ist ein Fehler aufgetreten.", false);
        }
    }

    private <T> T getRandomCollectionEntry(Collection<T> collection) {
        final int randomIndex = new Random().nextInt(collection.size());
        return collection.stream().skip(randomIndex).findFirst().orElse(null);
    }

    @Override
    public SlashCommandData constructCommand() {
        return Commands.slash(this.getName(), "Zugriff auf Altklausuren verwalten.")
                .addSubcommands(
                        new SubcommandData("list", "Listet verfügbare Module für Altklausuren."),
                        new SubcommandData("download", "Lädt Altklausuren für Module oder Professoren herunter.")
                                .addOptions(
                                        new OptionData(OptionType.STRING, "ids", "Eine durch Leertasten getrennte Liste an IDs der Module/Profs.").setRequired(true),
                                        new OptionData(OptionType.BOOLEAN, "agreement", "Dein Discord-Name wird bis zum Ende des Semesters gespeichert, um dich da nach Klausuren zu fragen.").setRequired(true),
                                        new OptionData(OptionType.BOOLEAN, "nolog", "Wenn auf true gesetzt, dann wird von deiner Anfrage nichts in diesen Chat geloggt.").setRequired(false)
                                ),
                        new SubcommandData("refresh", "Aktualisiert die Altklausuren-Cache."),
                        new SubcommandData("notify", "Sendet eine Aufforderung an alle Teilnehmenden, ihre Altklausuren einzusenden.")
                                .addOptions(
                                        new OptionData(OptionType.STRING, "from", "Das Startdatum für die Aufforderung (Format: yyyy-MM-dd).").setRequired(true),
                                        new OptionData(OptionType.STRING, "to", "Das Enddatum für die Aufforderung (Format: yyyy-MM-dd).").setRequired(true),
                                        new OptionData(OptionType.BOOLEAN, "simulate", "Muss auf false gesetzt sein um tatsächlich alle zu benachrichtigen.").setRequired(true),
                                        new OptionData(OptionType.BOOLEAN, "you-sure", "Bist du dir WIRKLICH sicher? Muss true sein.").setRequired(true),
                                        new OptionData(OptionType.BOOLEAN, "admin-only", "Ob die Nachricht nur an Admins gesendet werden soll.").setRequired(true)
                                ),
                        new SubcommandData("upload", "Erstellt einen Upload-Link mit dem du deine Altklausuren teilen kannst.")
                );
    }

    @Override
    public String getName() {
        return "altklausuren";
    }

    @Override
    public String getHelp() {
        return "Verwaltet den Zugriff auf Altklausuren über verschiedene Befehle.\n" +
                "Nutze `/altklausuren list` um eine Übersicht über alle Module und Dozenten zu erhalten und kopiere dir alle kurz-ids der Module oder Profs, die du herunterladen möchtest.\n" +
                "Dann gebe diese in `/altklausuren download ids:<kurz-ids>` ein um diese herunterzuladen.\n" +
                "Mit `/altklausuren upload` kannst du einen Freigabelink anfordern, auf den du deine eigenen Altklausuren zum Teilen mit den nächsten Semestern hochladen kannst.";
    }
}
