package fsi.listeners.stundenplan;

import fsi.CommandUtil;
import fsi.listeners.utility.ISlash;
import fsi.service.stundenplan.*;
import fsi.startup.ApplicationStartupLogger;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.StringSelectInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import net.dv8tion.jda.api.interactions.components.selections.SelectOption;
import net.dv8tion.jda.api.interactions.components.selections.StringSelectMenu;
import net.dv8tion.jda.api.utils.FileUpload;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class StundenplanSlash extends ListenerAdapter implements ISlash {

    private final StundenplanService stundenplanService;

    private final List<String> selectedStudiengaenge = new ArrayList<>();
    private final List<String> selectedSemesters = new ArrayList<>();
    private final List<String> selectedFaecher = new ArrayList<>();

    public StundenplanSlash(StundenplanService stundenplanService) {
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName() + " as " + this.getName());
        this.stundenplanService = stundenplanService;
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        if (!event.getName().equals(this.getName())) {
            return;
        }

        event.deferReply().queue();

        try {
            if (!event.getMember().hasPermission(Permission.BAN_MEMBERS) && !event.getMember().hasPermission(Permission.KICK_MEMBERS)) {
                log.warn("User [{} / {}] tried to create a stundenplan selector without proper permissions.", event.getMember().getId(), event.getMember().getEffectiveName());
                event.getHook().sendMessage("**Du bist nicht berechtigt**, um einen Stundenplan-Selektor zu bauen!").setEphemeral(true).queue();
                return;
            }

            log.info("Creating stundenplan selector for user [{} / {}]", event.getMember().getId(), event.getMember().getEffectiveName());

            final EmbedBuilder embed = createHeaderEmbed(0);

            final List<StundenplanStudiengang> studiengaenge = sortStudiengaenge(this.stundenplanService.fetchStudiengaenge());
            List<StringSelectMenu> studiengangMenus = createDropdownMenus(studiengaenge.stream()
                    .map(s -> createSelectOptionFromValue(s.getKuerzel(), s.getName()))
                    .collect(Collectors.toList()), "studiengang-menu");

            event.getHook().sendMessageEmbeds(embed.build())
                    .setComponents(getComponentsWithButtons(studiengangMenus, false))
                    .queue();

            log.info("Stundenplan selector created successfully in channel [{}] by user [{} / {}]", CommandUtil.formatChannelName(event), event.getMember().getId(), event.getMember().getEffectiveName());
        } catch (Exception e) {
            CommandUtil.logCommandError(event, e, "Es gab einen Fehler beim Erstellen der Stundenplan-Nachricht.", false);
        }
    }

    private static @NotNull SelectOption createSelectOptionFromValue(String value, String displayValue) {
        final String name = value.replaceAll("@.+", "") + " - " + displayValue;
        return SelectOption.of(name.length() > 100 ? name.substring(0, 100) : name, value);
    }

    private List<StundenplanStudiengang> sortStudiengaenge(Collection<StundenplanStudiengang> studiengangs) {
        if (studiengangs == null || studiengangs.isEmpty()) return Collections.emptyList();
        final List<String> priorityKuerzel = Arrays.asList("IB", "IMB", "UIB", "IM", "CSB");
        return studiengangs.stream()
                .sorted(Comparator.comparingInt(s -> {
                    final int index = priorityKuerzel.indexOf(s.getKuerzel());
                    return index == -1 ? Integer.MAX_VALUE : index;
                }))
                .collect(Collectors.toList());
    }

    @Override
    public void onButtonInteraction(ButtonInteractionEvent event) {
        try {
            final String buttonId = event.getButton().getId();
            log.info("Handling button interaction [{}] for user [{} / {}]", buttonId, event.getMember().getId(), event.getMember().getEffectiveName());

            final boolean isNextEvent = "next-button".equals(buttonId);
            final boolean isRestartEvent = "restart-button".equals(buttonId);

            if (isNextEvent) {
                handleNextButton(event);
            } else if (isRestartEvent) {
                handleRestartButton(event);
            }
        } catch (Exception e) {
            CommandUtil.logCommandError(event.getChannel(), e, "Es gab einen Fehler beim Verarbeiten der Button-Interaktion.");
        }
    }

    @Override
    public void onStringSelectInteraction(StringSelectInteractionEvent event) {
        final boolean isStudiengangMenu = event.getComponentId().startsWith("studiengang-menu");
        final boolean isSemesterMenu = event.getComponentId().startsWith("semester-menu");
        final boolean isFachMenu = event.getComponentId().startsWith("fach-menu");

        if (!isStudiengangMenu && !isSemesterMenu && !isFachMenu) {
            return;
        }
        event.deferEdit().queue();

        try {
            if (isStudiengangMenu) {
                selectedStudiengaenge.removeAll(event.getSelectedOptions().stream().map(SelectOption::getValue).collect(Collectors.toList()));
                selectedStudiengaenge.addAll(event.getValues());
            } else if (isSemesterMenu) {
                selectedSemesters.removeAll(event.getSelectedOptions().stream().map(SelectOption::getValue).collect(Collectors.toList()));
                selectedSemesters.addAll(event.getValues());
            } else if (isFachMenu) {
                selectedFaecher.removeAll(event.getSelectedOptions().stream().map(SelectOption::getValue).collect(Collectors.toList()));
                selectedFaecher.addAll(event.getValues());
            }
        } catch (Exception e) {
            CommandUtil.logCommandError(event.getChannel(), e, "Es gab einen Fehler beim Verarbeiten der Dropdown-Interaktion.");
        }
    }

    private void handleNextButton(ButtonInteractionEvent event) {
        List<ActionRow> actionRows = event.getMessage().getActionRows();
        if (!actionRows.isEmpty() && actionRows.get(0).getComponents().get(0) instanceof StringSelectMenu) {
            String currentComponentId = ((StringSelectMenu) actionRows.get(0).getComponents().get(0)).getId();

            try {
                if (currentComponentId.startsWith("studiengang-menu")) {
                    handleStudiengangSelection(event, actionRows);
                } else if (currentComponentId.startsWith("semester-menu")) {
                    handleSemesterSelection(event, actionRows);
                } else if (currentComponentId.startsWith("fach-menu")) {
                    handleFachSelection(event, actionRows);
                }
            } catch (IOException e) {
                CommandUtil.logCommandError(event.getChannel(), e, "Es gab einen Fehler beim Verarbeiten der Button-Interaktion.");
            }
        }
    }

    private void handleStudiengangSelection(ButtonInteractionEvent event, List<ActionRow> actionRows) throws IOException {
        log.info("Selected Studiengänge: {}", selectedStudiengaenge);
        final List<StundenplanSemester> semesters = this.stundenplanService.fetchSemester(
                selectedStudiengaenge.stream()
                        .map(kuerzel -> new StundenplanStudiengang(kuerzel, null, null))
                        .collect(Collectors.toList())
        );

        List<StringSelectMenu> semesterMenus = createDropdownMenus(semesters.stream()
                .map(s -> createSelectOptionFromValue(s.getKuerzel(), s.getName()))
                .collect(Collectors.toList()), "semester-menu");

        EmbedBuilder embed = createHeaderEmbed(1);

        log.info("Adding [{}] semester menus from {}", semesterMenus.size(), semesters.stream().map(StundenplanSemester::getKuerzel).collect(Collectors.toList()));
        event.editMessageEmbeds(embed.build())
                .setComponents(getComponentsWithButtons(semesterMenus, false))
                .queue();
    }

    private void handleSemesterSelection(ButtonInteractionEvent event, List<ActionRow> actionRows) throws IOException {
        log.info("Selected Semesters: {}", selectedSemesters);

        List<StundenplanSemester> fetchedSemesters = this.stundenplanService.fetchSemester(
                selectedStudiengaenge.stream()
                        .map(kuerzel -> new StundenplanStudiengang(kuerzel, null, null))
                        .collect(Collectors.toList())
        );
        List<StundenplanSemester> filteredSemesters = fetchedSemesters.stream()
                .filter(s -> selectedSemesters.contains(s.getKuerzel()))
                .collect(Collectors.toList());

        final List<StundenplanFach> faecher = this.stundenplanService.fetchFaecher(filteredSemesters);
        final Set<String> kuerzelSet = new HashSet<>();
        faecher.removeIf(f -> !kuerzelSet.add(f.getKuerzel()));

        List<StringSelectMenu> fachMenus = createDropdownMenus(faecher.stream()
                .map(f -> createSelectOptionFromValue(f.getKuerzel() + "@" + f.getStudiengang(), f.getName()))
                .collect(Collectors.toList()), "fach-menu");

        EmbedBuilder embed = createHeaderEmbed(2);

        log.info("Adding [{}] fach menus from {}", fachMenus.size(), faecher.stream().map(StundenplanFach::getKuerzel).collect(Collectors.toList()));
        event.editMessageEmbeds(embed.build())
                .setComponents(getComponentsWithButtons(fachMenus, true))
                .queue();
    }

    private void handleFachSelection(ButtonInteractionEvent event, List<ActionRow> actionRows) throws IOException {
        log.info("Selected Fächer: {}", selectedFaecher);

        final StundenplanPlan.Stundenplan stundenplan = this.stundenplanService.fetchStundenplan(selectedFaecher.stream().map(val -> {
                    final String[] split = val.split("@");
                    if (split.length != 2) {
                        log.warn("Invalid Fach value: [{}]", val);
                        return null;
                    }
                    return new StundenplanFach(split[1], split[0], null);
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
        log.info("Extracted timetable: {}", stundenplan);
        final BufferedImage image;
        try {
            image = stundenplan.render(selectedSemesters.stream().sorted().collect(Collectors.joining(", ")));
        } catch (Exception e) {
            log.error("Failed to render stundenplan for user [{} / {}]", event.getMember().getId(), event.getMember().getEffectiveName(), e);
            return;
        }

        handleRestartButton(event);

        // save image to file and send user a message with the image
        ImageIO.write(image, "png", new File("stundenplan.png"));
        event.getUser().openPrivateChannel().queue(channel -> {
            channel.sendFiles(FileUpload.fromData(new File("stundenplan.png"))).queue();
        });
    }

    private void handleRestartButton(ButtonInteractionEvent event) throws IOException {
        EmbedBuilder embed = createHeaderEmbed(0);

        List<StringSelectMenu> studiengangMenus = createDropdownMenus(sortStudiengaenge(this.stundenplanService.fetchStudiengaenge()).stream()
                .map(s -> createSelectOptionFromValue(s.getKuerzel(), s.getName()))
                .collect(Collectors.toList()), "studiengang-menu");

        event.editMessageEmbeds(embed.build())
                .setComponents(getComponentsWithButtons(studiengangMenus, false))
                .queue();

        selectedStudiengaenge.clear();
        selectedSemesters.clear();
        selectedFaecher.clear();
    }

    private List<StringSelectMenu> createDropdownMenus(List<SelectOption> options, String menuIdPrefix) {
        List<StringSelectMenu> menus = new ArrayList<>();
        for (int i = 0; i < options.size(); i += 25) {
            List<SelectOption> sublist = options.subList(i, Math.min(i + 25, options.size()));
            menus.add(StringSelectMenu.create(menuIdPrefix + (i / 25))
                    .setPlaceholder(i == 0 ? "Wähle deine Option(en)" : "... hier gibt's noch mehr")
                    .addOptions(sublist)
                    .setMaxValues(sublist.size())
                    .build());
        }
        if (menus.size() > 5) {
            log.warn("Too many items for stundenplan selection, max is 5 * 25 = 125 items, but got [{}], limiting to 125", options.size());
            return menus.stream().limit(5).collect(Collectors.toList());
        }
        return menus;
    }

    private static EmbedBuilder createHeaderEmbed(int step) {
        final String title;
        final String description;

        if (step == 0) {
            title = "Erstelle deinen eigenen Stundenplan";
            description = "Mit diesem Tool kannst du dir deinen eigenen Stundenplan zuschicken lassen.\n" +
                    "Wähle dazu deine relevanten Studiengänge aus den Dropdown-Menüs aus.\n" +
                    "Dann klicke auf 'Next' um weiterzumachen.\n" +
                    "(Discord erlaubt nur 25 Elemente pro Auswahl, darum mehrere)";
        } else if (step == 1) {
            title = "Wähle deine Semester aus";
            description = "Wähle deine Semester aus den Dropdown-Menüs und klicke dann auf 'Next'.";
        } else if (step == 2) {
            title = "Wähle deine Module aus";
            description = "Wähle deine Module aus den Dropdown-Menüs und klicke dann auf 'Stundenplan erstellen'.";
        } else {
            title = "Stundenplan-Erstellung";
            description = "Klicke auf \"Restart\", um einen neuen Stundenplan zu erstellen.";
        }

        return new EmbedBuilder()
                .setTitle(title)
                .setDescription(description)
                .setColor(0xffffff);
    }

    private List<ActionRow> getComponentsWithButtons(List<StringSelectMenu> menus, boolean letzteAktion) {
        List<ActionRow> components = new ArrayList<>();
        menus.forEach(menu -> components.add(ActionRow.of(menu)));
        components.add(ActionRow.of(
                Button.danger("restart-button", "Restart"),
                Button.primary("next-button", letzteAktion ? "Stundenplan erstellen" : "Next")
        ));
        return components;
    }

    @Override
    public SlashCommandData constructCommand() {
        return Commands.slash(this.getName(), "Ein Stundenplan-Picker für Nutzer erstellen");
    }

    @Override
    public String getName() {
        return "stundenplan";
    }

    @Override
    public String getHelp() {
        return "Dieser Befehl erlaubt es, eine Kombination aus einem Embed (Titel, Beschreibung) und Dropdown-Elementen zu erzeugen, mit der ein Nutzer Optionen auswählen kann.";
    }
}
