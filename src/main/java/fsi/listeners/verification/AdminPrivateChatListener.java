package fsi.listeners.verification;

import fsi.CommandUtil;
import fsi.config.FsBotConfig;
import fsi.startup.ApplicationStartupLogger;
import fsi.startup.BuildProperties;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import okhttp3.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Log4j2
public class AdminPrivateChatListener extends ListenerAdapter {

    private JDA jda;
    private String adminUserId;

    public AdminPrivateChatListener() {
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName());
        CommandUtil.setAdminPrivateChat(this);
    }

    public void setup(JDA jda) {
        this.jda = jda;

        this.adminUserId = FsBotConfig.get(FsBotConfig.Attribute.ADMIN_USER_ID);
        if (this.adminUserId == null) {
            throw new IllegalArgumentException("Admin user ID not found in configuration");
        }
        final User adminUser = jda.getUserById(this.adminUserId);
        if (adminUser == null) {
            throw new IllegalArgumentException("Admin user not found using ID: " + this.adminUserId);
        }

        ApplicationStartupLogger.subStep(log, "Found admin user: @" + adminUser.getName() + " [" + adminUser.getId() + "]");

        if (!COMMAND_ACTIONS.keySet().equals(COMMAND_DESCRIPTION.keySet())) {
            ApplicationStartupLogger.subStepWarn(log, "COMMAND_ACTIONS and COMMAND_DESCRIPTION keys do not match");
            throw new IllegalArgumentException("COMMAND_ACTIONS and COMMAND_DESCRIPTION keys do not match:\n" +
                    "COMMAND_ACTIONS: " + COMMAND_ACTIONS.keySet() + "\n" +
                    "COMMAND_DESCRIPTION: " + COMMAND_DESCRIPTION.keySet());
        }

        sendAdminMessageSuccess("Bot started successfully in version " +
                "`" + BuildProperties.getProperties().getProperty(BuildProperties.PROJECT_VERSION) + "`");
    }

    private User getAdminUser() {
        return jda.getUserById(this.adminUserId);
    }

    public void sendAdminMessage(String message) {
        final User adminUser = getAdminUser();
        if (adminUser != null) {
            adminUser.openPrivateChannel().queue(channel -> channel.sendMessage(message).queue());
        }
    }

    public void sendAdminMessageWarning(String message) {
        sendAdminMessage(":warning: " + message);
    }

    public void sendAdminMessageSuccess(String message) {
        sendAdminMessage(":white_check_mark: " + message);
    }

    private Map<String, String> extractCommandArguments(String command, String... allowedArgs) {
        // argument syntax is -argname value -argname value
        final Map<String, String> arguments = new HashMap<>();
        final String[] split = command.split(" ");

        String currentArgName = null;
        String currentArgValue = null;
        for (int i = 0; i < split.length; i++) {
            if (split[i].startsWith("-")) {
                final String argName = split[i].substring(1);

                // check if the argument is allowed
                boolean found = false;
                for (String allowedArg : allowedArgs) {
                    if (argName.equals(allowedArg)) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    if (currentArgName != null) {
                        arguments.put(currentArgName, currentArgValue);
                    }
                    currentArgName = argName;
                    currentArgValue = null;
                    continue;
                }
            }

            if (currentArgName != null) {
                if (currentArgValue == null) {
                    currentArgValue = split[i];
                } else {
                    currentArgValue += " " + split[i];
                }
            }
        }

        if (currentArgName != null) {
            arguments.put(currentArgName, currentArgValue);
        }

        return arguments;
    }

    final Map<String, Consumer<String>> COMMAND_ACTIONS = new LinkedHashMap<>();
    final Map<String, String> COMMAND_DESCRIPTION = new LinkedHashMap<>();

    {
        COMMAND_ACTIONS.put("help", this::help);
        COMMAND_DESCRIPTION.put("help", "Show this help message");
        COMMAND_ACTIONS.put("info", this::info);
        COMMAND_DESCRIPTION.put("info", "Show bot information");
        COMMAND_ACTIONS.put("log", this::logCommand);
        COMMAND_DESCRIPTION.put("log", "`[lines=<count>] [start=<substring>]` - Show an area of <lines> lines around the log line containing <substring> or the last <lines> lines");
        COMMAND_ACTIONS.put("download-update", this::downloadUpdate);
        COMMAND_DESCRIPTION.put("download-update", "`<remote-url>` `<remote-sha256>` `<local-name>` - Download an update from the given URL, but do not apply it");
        COMMAND_ACTIONS.put("update", this::update);
        COMMAND_DESCRIPTION.put("update", "`<file-name>` - Apply the update from the given file and terminate the program");
        COMMAND_ACTIONS.put("exit", this::exit);
        COMMAND_DESCRIPTION.put("exit", "Exit the program, type `exit confirm` to confirm");
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (!event.isFromType(ChannelType.PRIVATE)) {
            return;
        }

        final String userId = event.getAuthor().getId();
        if (!userId.equals(this.adminUserId)) {
            return;
        }

        final String userInput = event.getMessage().getContentRaw();
        for (Map.Entry<String, Consumer<String>> entry : COMMAND_ACTIONS.entrySet()) {
            if (userInput.startsWith(entry.getKey())) {
                final String command = userInput.substring(entry.getKey().length()).trim();
                entry.getValue().accept(command);
                log.info("Admin message received [{}]", entry.getKey());
                return;
            }
        }

        // sendAdminMessageWarning("Unknown command: " + userInput);
        log.warn("Unknown admin message received [{}]", userInput);
    }

    private void help(String userInput) {
        final StringJoiner joiner = new StringJoiner("\n");
        joiner.add("Available commands:");
        for (String command : COMMAND_ACTIONS.keySet()) {
            joiner.add("- `" + command + "` - " + COMMAND_DESCRIPTION.get(command));
        }
        sendAdminMessage(joiner.toString());
    }

    private void downloadUpdate(String userInput) {
        final Map<String, String> arguments = extractCommandArguments(userInput, "remote-url", "remote-sha256", "local-name");
        if (!arguments.containsKey("remote-url") || !arguments.containsKey("remote-sha256") || !arguments.containsKey("local-name")) {
            sendAdminMessageWarning("Missing arguments: remote-url, remote-sha256, local-name");
            return;
        }
        log.info("Download update arguments: {}", arguments);

        final String remoteUrl = arguments.get("remote-url");
        final String remoteSha256 = arguments.get("remote-sha256");
        final String localName = arguments.get("local-name");

        final String cleanLocalName = new File(localName).getName();

        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder().url(remoteUrl).build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                sendAdminMessageWarning("Failed to download file: " + e.getMessage());
                log.error("Failed to download file from URL: {}", remoteUrl, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    sendAdminMessageWarning("Failed to download file: " + response.message());
                    log.error("Failed to download file from URL: {} - Response: {}", remoteUrl, response.message());
                    return;
                }

                try (ResponseBody body = response.body(); InputStream inputStream = body.byteStream(); FileOutputStream outputStream = new FileOutputStream(cleanLocalName)) {
                    byte[] buffer = new byte[8192];
                    int bytesRead;
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }

                    outputStream.flush();
                }

                try (InputStream inputStream = Files.newInputStream(new File(cleanLocalName).toPath())) {
                    // sha256sum <file-path>
                    String downloadedFileHash = DigestUtils.sha256Hex(inputStream);
                    if (!downloadedFileHash.equalsIgnoreCase(remoteSha256)) {
                        sendAdminMessageWarning("Downloaded file hash does not match the expected hash:\nGot: `" + downloadedFileHash + "`\nExpected: `" + remoteSha256 + "`");
                        log.error("Hash mismatch for downloaded file: expected [{}], got [{}]", remoteSha256, downloadedFileHash);

                        // remove from system
                        Files.deleteIfExists(new File(cleanLocalName).toPath());
                        return;
                    }
                }

                sendAdminMessageSuccess("File downloaded and verified successfully:\n`" + new File(cleanLocalName).getAbsolutePath() + "`");
                log.info("File downloaded and verified successfully: {}", cleanLocalName);
            }
        });
    }

    private static @NotNull String getApplicationStartPath() {
        return new File(AdminPrivateChatListener.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getAbsolutePath();
    }

    private static String getApplicationPid() {
        try {
            // fetch the pid of this process
            final Process currentProcess = Runtime.getRuntime().exec("jps -l");
            final InputStream currentProcessInputStream = currentProcess.getInputStream();
            final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[1024];
            while ((nRead = currentProcessInputStream.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            final String currentProcessOutput = new String(buffer.toByteArray());
            final String[] split = currentProcessOutput.split(" ");
            return split[0];
        } catch (IOException e) {
            log.error("Failed to fetch current process PID: {}", e.getMessage());
            return null;
        }
    }

    private void info(String userInput) {
        final String version = BuildProperties.getProperties().getProperty(BuildProperties.PROJECT_VERSION);
        final String jarFilePath = getApplicationStartPath();
        final String pid = getApplicationPid();
        final String message = "Bot Version: " + version + "\n" +
                "Launched from path: `" + jarFilePath + "`\n" +
                "PID: `" + pid + "`";
        sendAdminMessage(message);
        log.info("Bot Version: {}, launched from: {}", version, jarFilePath);
    }

    private void update(String userInput) {
        final Map<String, String> arguments = extractCommandArguments(userInput, "file-name");
        if (!arguments.containsKey("file-name")) {
            sendAdminMessageWarning("Missing argument: file-name");
            return;
        }

        final String fileName = arguments.get("file-name");
        File updateFile = new File(fileName);
        if (!updateFile.exists()) {
            sendAdminMessageWarning("Update file not found: " + fileName);
            log.error("Update file not found: {}", fileName);
            return;
        }

        sendAdminMessage("Applying update from file: " + fileName);
        log.info("Applying update from file: {}", fileName);

        try {
            ProcessBuilder pb = new ProcessBuilder("java", "-jar", updateFile.getAbsolutePath());

            // Do not inherit I/O; redirect output and error to avoid blocking
            // pb.redirectOutput(ProcessBuilder.Redirect.DISCARD);
            // pb.redirectError(ProcessBuilder.Redirect.DISCARD);

            final Process process = pb.start();

            // Close the process's output stream to free resources to allow this process to exit
            if (process.getOutputStream() != null) {
                process.getOutputStream().close();
            }

            sendAdminMessageSuccess("New process started successfully.");
            log.info("New process started successfully.");
        } catch (IOException e) {
            sendAdminMessageWarning("Failed to start new process: " + e.getMessage());
            log.error("Failed to start new process: {}", e.getMessage(), e);
            return;
        }

        sendAdminMessage("Exiting current application.");
        log.info("Exiting current application.");
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            log.error("Interrupted while waiting to exit", e);
        }
        System.exit(0);
    }

    private void exit(String userInput) {
        if (userInput.contains("confirm")) {
            sendAdminMessage("Exiting application.");
            log.info("Exiting application.");
            System.exit(0);
        } else {
            sendAdminMessageWarning("Please confirm the exit by typing `exit confirm`");
            log.warn("Exit command not confirmed");
        }
    }

    private void logCommand(String userInput) {
        final Map<String, String> arguments = extractCommandArguments(userInput, "lines", "start");

        int paddingLinesCount = 10;
        if (arguments.containsKey("lines")) {
            try {
                paddingLinesCount = Integer.parseInt(arguments.get("lines"));
            } catch (NumberFormatException e) {
                sendAdminMessageWarning("Invalid value for `lines` argument. It must be a number.");
                return;
            }
        }

        final String start = arguments.getOrDefault("start", null);
        final File logFile = new File("logs/application.log");

        if (!logFile.exists()) {
            sendAdminMessageWarning("Log file not found in `" + logFile.getAbsolutePath() + "`.");
            log.error("Log file not found at path: {}", logFile.getAbsolutePath());
            return;
        }

        // find the start line to read from
        int startLine = -1;
        if (start == null) {
            try (BufferedReader reader = new BufferedReader(new FileReader(logFile))) {
                int lineCount = 0;
                while (reader.readLine() != null) {
                    lineCount++;
                }
                startLine = Math.max(0, lineCount - paddingLinesCount);
            } catch (IOException e) {
                sendAdminMessageWarning("Error reading the log file: " + e.getMessage());
                log.error("Error reading the log file: {}", e.getMessage(), e);
                return;
            }
        } else {
            try (BufferedReader reader = new BufferedReader(new FileReader(logFile))) {
                String line;
                int lineCount = 0;
                while ((line = reader.readLine()) != null) {
                    if (line.contains(start)) {
                        startLine = Math.max(0, lineCount - paddingLinesCount);
                        break;
                    }
                    lineCount++;
                }
            } catch (IOException e) {
                sendAdminMessageWarning("Error reading the log file: " + e.getMessage());
                log.error("Error reading the log file: {}", e.getMessage(), e);
                return;
            }
        }

        if (startLine == -1) {
            sendAdminMessageWarning("No matching logs found.");
            return;
        }

        // extract the relevant lines
        final List<String> result = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(logFile))) {
            String line;
            int lineIndex = 0;
            while ((line = reader.readLine()) != null) {
                if (lineIndex >= startLine) {
                    result.add(line);
                }
                if (lineIndex >= startLine + paddingLinesCount * 2) {
                    break;
                }
                lineIndex++;
            }
        } catch (IOException e) {
            sendAdminMessageWarning("Error reading the log file: " + e.getMessage());
            log.error("Error reading the log file: {}", e.getMessage(), e);
            return;
        }


        if (result.isEmpty()) {
            sendAdminMessageWarning("No matching logs found.");
        } else {
            sendAdminMessageSuccess("Log Output:");

            final int maxMessageLength = 2000 - 8;
            StringJoiner activeLineJoiner = new StringJoiner("\n", "```\n", "\n```");
            for (String line : result) {
                if (activeLineJoiner.length() + line.length() > maxMessageLength) {
                    sendAdminMessage(activeLineJoiner.toString());
                    activeLineJoiner = new StringJoiner("\n", "```\n", "\n```");
                }
                activeLineJoiner.add(line);
            }
            if (activeLineJoiner.length() > 5) {
                sendAdminMessage(activeLineJoiner.toString());
            }
        }
    }
}
