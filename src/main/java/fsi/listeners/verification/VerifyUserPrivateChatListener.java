package fsi.listeners.verification;

import fsi.CommandUtil;
import fsi.config.FsBotConfig;
import fsi.service.StudentVerificationService;
import fsi.startup.ApplicationStartupLogger;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class VerifyUserPrivateChatListener extends ListenerAdapter {

    private final StudentVerificationService verificationService;

    private JDA jda;

    public VerifyUserPrivateChatListener(StudentVerificationService verificationService) {
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName());
        this.verificationService = verificationService;
    }

    public void setup(JDA jda) {
        this.jda = jda;
        this.getVerifyChannel();
        this.getSuccessfulRedirectChannel();
    }

    private TextChannel getVerifyChannel() {
        final TextChannel verifyChannel = this.jda.getTextChannelById(FsBotConfig.get(FsBotConfig.Attribute.VERIFY_CHANNEL_ID));
        if (verifyChannel == null) {
            throw new IllegalArgumentException("Verify channel not found using ID: " + FsBotConfig.get(FsBotConfig.Attribute.VERIFY_CHANNEL_ID));
        }
        return verifyChannel;
    }

    private TextChannel getSuccessfulRedirectChannel() {
        final TextChannel verifyChannel = this.jda.getTextChannelById(FsBotConfig.get(FsBotConfig.Attribute.VERIFY_REDIRECT_USER_ON_VERIFICATION));
        if (verifyChannel == null) {
            throw new IllegalArgumentException("Verify channel not found using ID: " + FsBotConfig.get(FsBotConfig.Attribute.VERIFY_REDIRECT_USER_ON_VERIFICATION));
        }
        return verifyChannel;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        final String userId = event.getAuthor().getId();
        final String userInput = event.getMessage().getContentRaw();

        if (!this.verificationService.isVerificationActive(userId)) {
            return;
        }

        log.info("User [{}] attempts to complete user verification using verification code: {}", userId, userInput);

        if (this.verificationService.completeVerification(userId, userInput)) {
            log.info("User [{}] verification code was correct, updating roles", userId);

            try {
                final Guild faGuild = event.getJDA().getGuildById(FsBotConfig.get(FsBotConfig.Attribute.PRIMARY_BOT_GUILD_ID));
                final Member member = faGuild.getMemberById(userId);

                final List<Role> updatedUserRoles = FsBotConfig.getCsvStringList(FsBotConfig.Attribute.VERIFY_ADD_USER_ROLES_ON_VERIFICATION).stream()
                        .map(faGuild::getRoleById)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
                updatedUserRoles.addAll(member.getRoles());

                final List<Role> removeRoles = FsBotConfig.getCsvStringList(FsBotConfig.Attribute.VERIFY_REMOVE_USER_ROLES_ON_VERIFICATION).stream()
                        .map(faGuild::getRoleById)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
                updatedUserRoles.removeAll(removeRoles);

                faGuild.modifyMemberRoles(member, updatedUserRoles).queue();

                event.getChannel().sendMessage("**Der Code scheint richtig zu sein**, du bist damit verifiziert!\nDu kannst nun auf den Server der Fachschaft " + this.getSuccessfulRedirectChannel().getAsMention() + " zurückkehren und deine Rollen auswählen.").queue();
                log.info("User [{}] has been verified. Active roles are now: {}",
                        userId, updatedUserRoles.stream().map(role -> "[" + role.getId() + ":" + role.getName() + "]").collect(Collectors.joining(", ")));
            } catch (Exception e) {
                CommandUtil.logCommandError(event, e, "Ups! Es gab einen Fehler, deine Rollen zu aktualisieren.");
            }
        } else {
            this.verificationService.cancelVerification(userId);
            event.getChannel().sendMessage("**Der Code war leider falsch**. Bitte fordere einen neuen Code an, indem du den Verify-Command im " + this.getVerifyChannel().getAsMention() + " Channel benutzt.").queue();
            log.info("User [{}] entered an incorrect verification code [{}]. Verification cancelled.", userId, userInput);
        }
    }
}
