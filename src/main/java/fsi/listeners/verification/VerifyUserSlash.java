package fsi.listeners.verification;

import fsi.CommandUtil;
import fsi.config.FsBotConfig;
import fsi.listeners.utility.ISlash;
import fsi.service.MailSendingService;
import fsi.service.StudentVerificationService;
import fsi.startup.ApplicationStartupLogger;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;

@Log4j2
public class VerifyUserSlash extends ListenerAdapter implements ISlash {

    private final StudentVerificationService verificationService;
    private final MailSendingService mailService;

    private JDA jda;

    public VerifyUserSlash(StudentVerificationService verificationService, MailSendingService mailService) {
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName() + " as " + this.getName());
        this.verificationService = verificationService;
        this.mailService = mailService;
    }

    public void setup(JDA jda) {
        this.jda = jda;
        this.getVerifyChannel();
    }

    private TextChannel getVerifyChannel() {
        final TextChannel verifyChannel = this.jda.getTextChannelById(FsBotConfig.get(FsBotConfig.Attribute.VERIFY_CHANNEL_ID));
        if (verifyChannel == null) {
            throw new IllegalArgumentException("Verify channel not found using ID: " + FsBotConfig.get(FsBotConfig.Attribute.VERIFY_CHANNEL_ID));
        }
        return verifyChannel;
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        if (!event.getName().equals(this.getName())) {
            return;
        }

        if (!event.getChannel().getId().equals(FsBotConfig.get(FsBotConfig.Attribute.VERIFY_CHANNEL_ID))) {
            log.info("Verify command received from user [{}] in channel [{}], but the command is not allowed in this channel, only in [{}]",
                    event.getUser().getName(), CommandUtil.formatChannelName(event), FsBotConfig.get(FsBotConfig.Attribute.VERIFY_CHANNEL_ID));
            event.reply("Dieser Command ist nur im " + this.getVerifyChannel().getAsMention() + " Channel erlaubt!").setEphemeral(true).queue();
            return;
        }

        event.deferReply().queue();
        log.info("User [{}] started user verification using the verify command, generating code and sending mail", event.getUser().getName());

        try {
            final Member member = event.getMember();
            final String memberId = member.getId();

            if (event.getOption("matrikelnummer") == null) {
                event.getHook().editOriginal("... du hast es geschafft, deine Matrikelnummer wegzulassen? Beeindruckend.").queue();
                return;
            }
            final String matrikelnummer = event.getOption("matrikelnummer").getAsString();

            final String verificationCode;
            try {
                verificationCode = verificationService.startVerification(memberId);
            } catch (StudentVerificationService.TimeoutStillActiveException e) {
                event.getHook().editOriginal("Du musst noch **" + e.getRemainingCooldownMillis() / 1000 + " Sekunden** warten, bevor du dich erneut verifizieren kannst.").queue();
                return;
            } catch (Exception e) {
                CommandUtil.logCommandError(event, e, "Ups! Es scheint, dass beim Starten der Verifizierung ein Fehler aufgetreten ist.", false);
                return;
            }

            final String emailTo = matrikelnummer + "@stud.hs-mannheim.de";
            final String emailSubject = "Dein FS-Discord Verify-Code";

            final String emailBody = "Hey,\n\n" +
                    "Du hast den Verify-Command benutzt und einen Code zur Verifizierung angefragt!\n" +
                    "Schicke diesen Code nun per Direktnachricht an den Fachschafts-Bot:\n\n" +
                    "   > " + verificationCode + "\n\n" +
                    "Wenn es dabei Probleme gibt, wende dich an einen Admin oder jemanden von der Fachschaft.\n\n" +
                    "Mit freundlichen Grüßen\n" +
                    "Deine Fachschaft";

            try {
                mailService.sendMail(emailTo, emailSubject, emailBody);
            } catch (Exception e) {
                CommandUtil.logCommandError(event, e, "Ups! Es scheint, dass beim Versenden der Mail ein Fehler aufgetreten ist.", false);
                return;
            }

            final String userId = event.getUser().getId();
            final User user = event.getJDA().getUserById(userId);
            if (user != null) {
                final String message = "**Ein Code wurde an deine Hochschul-Mail gesendet!**\nDu hast 5 Minuten um mit diesem Code auf diese Nachricht zu antworten, um dich zu verifizieren.";
                user.openPrivateChannel().queue(channel -> channel.sendMessage(message).queue());
            }
        } catch (Exception e) {
            CommandUtil.logCommandError(event, e, "Ups! Es scheint, dass beim Starten der Verifizierung ein Fehler aufgetreten ist.", false);
        } finally {
            // for privacy reasons, delete the original message containing the matrikelnummer
            event.getHook().deleteOriginal().queue();
        }
    }

    @Override
    public SlashCommandData constructCommand() {
        return Commands.slash(this.getName(), "Verifiziere, dass du an der Hochschule Mannheim studierst!")
                .addOptions(
                        new OptionData(OptionType.INTEGER, "matrikelnummer", "Deine Matrikelnummer an der Hochschule Mannheim")
                                .setRequired(true)
                );
    }

    @Override
    public String getName() {
        return "verify";
    }

    @Override
    public String getHelp() {
        return "Mit dem Verify-Command kannst du dich verifizieren, um vollständigen Zugriff auf den Server zu erhalten!\n" +
                "Über deine Matrikelnummer wird die eine Mail mit einem Code zugesendet, den du an den Bot schicken musst.\n\n" +
                "Beispiel: `/verify 2121212`";
    }
}
