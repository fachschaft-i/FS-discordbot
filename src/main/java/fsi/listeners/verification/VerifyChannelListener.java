package fsi.listeners.verification;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class VerifyChannelListener extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        // FIXME: Why is this required?
        if (event.getChannel().getId().equals("952260026494500955")) {
            if (!event.getAuthor().getId().equals("949236408911032371")) {
                event.getMessage().delete().queue();
            }
        }
    }
}
