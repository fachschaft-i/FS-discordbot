package fsi.listeners.selfrole;

import fsi.CommandUtil;
import fsi.startup.ApplicationStartupLogger;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.component.StringSelectInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.selections.SelectOption;
import net.dv8tion.jda.api.interactions.components.selections.StringSelectMenu;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class SelfroleListener extends ListenerAdapter {

    public SelfroleListener() {
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName());
    }

    @Override
    public void onStringSelectInteraction(StringSelectInteractionEvent event) {
        if (!"selfrole".equals(event.getComponentId())) {
            return;
        }

        try {
            log.info("Handling selfrole selection for user [{} / {}]", event.getMember().getId(), event.getMember().getEffectiveName());

            final Guild guild = event.getGuild();
            if (guild == null) {
                log.error("Guild is null for selfrole selector for user [{} / {}]", event.getMember().getId(), event.getMember().getEffectiveName());
                event.getChannel().sendMessage("... dein Server existiert nicht? Excuse me? Bitte melde dich doch bei einem Entwickler deswegen.").queue();
                return;
            }

            final int maxRoles = event.getSelectMenu().getMaxValues();

            final List<Role> selectedRoles = new ArrayList<>();
            final List<String> failedToFindRoles = new ArrayList<>();

            for (String value : event.getValues()) {
                final Role role = guild.getRoleById(value);
                if (role != null) {
                    selectedRoles.add(role);
                } else {
                    failedToFindRoles.add(value);
                }
            }

            if (!failedToFindRoles.isEmpty()) {
                log.error("Roles with IDs {} not found whilst adding via selfrole selector for user [{} / {}]", failedToFindRoles, event.getMember().getId(), event.getMember().getEffectiveName());
                event.getChannel().sendMessage("Einige Rollen konnten nicht gefunden werden: " + String.join(", ", failedToFindRoles) + "\nBitte melde dich bei einem Entwickler, dass er dieses Drop-Down aktualisiert.").queue();
                return;
            }

            final List<Role> availableRolesFromDropDown = new ArrayList<>();
            for (SelectOption optionRole : event.getSelectMenu().getOptions()) {
                final Role role = event.getGuild().getRoleById(optionRole.getValue());
                if (role != null) {
                    availableRolesFromDropDown.add(role);
                }
            }

            final List<Role> removeRoles = new ArrayList<>();
            for (Role role : availableRolesFromDropDown) {
                if (!selectedRoles.contains(role)) {
                    removeRoles.add(role);
                }
            }

            final List<Role> memberRoles = new ArrayList<>(event.getMember().getRoles());
            memberRoles.addAll(selectedRoles);

            deduplicateRoles(memberRoles);
            deduplicateRolesUsing(availableRolesFromDropDown, memberRoles);
            deduplicateRolesUsing(removeRoles, memberRoles);

            memberRoles.removeAll(removeRoles);

            final List<Role> totalUserRolesAvailableInSelection = new ArrayList<>();
            for (Role memberRole : memberRoles) {
                if (availableRolesFromDropDown.contains(memberRole)) {
                    totalUserRolesAvailableInSelection.add(memberRole);
                }
            }

            final List<Role> removedRoles = new ArrayList<>();
            while (totalUserRolesAvailableInSelection.size() > maxRoles && !totalUserRolesAvailableInSelection.isEmpty()) {
                final Role roleToRemove = totalUserRolesAvailableInSelection.get(0);
                removedRoles.add(roleToRemove);
                totalUserRolesAvailableInSelection.remove(roleToRemove);
                memberRoles.remove(roleToRemove);
            }
            selectedRoles.removeAll(removedRoles);

            guild.modifyMemberRoles(event.getMember(), memberRoles).queue(
                    success -> log.info("Successfully updated roles for user [{} / {}] using selfrole selector: +{}", event.getMember().getId(), event.getMember().getEffectiveName(), selectedRoles),
                    error -> log.error("Failed to update roles for user [{} / {}] using selfrole selector: +{}", event.getMember().getId(), event.getMember().getEffectiveName(), selectedRoles)
            );
        } catch (Exception e) {
            CommandUtil.logCommandError(event.getChannel(), e, "Es gab einen Fehler beim Verarbeiten der Selfrole-Auswahl.");
        } finally {
            final int maxRoles = event.getSelectMenu().getMaxValues();

            final StringSelectMenu resetMenu = StringSelectMenu.create("selfrole")
                    .setPlaceholder("Wähle aus...")
                    .setMinValues(0)
                    .setMaxValues(maxRoles)
                    .addOptions(event.getSelectMenu().getOptions())
                    .build();

            event.editComponents(ActionRow.of(resetMenu)).queue();
        }
    }

    private void deduplicateRoles(List<Role> roles) {
        final List<Role> deduplicatedRoles = new ArrayList<>();
        for (Role role : roles) {
            final boolean alreadyAdded = deduplicatedRoles.stream().anyMatch(deduplicatedRole -> deduplicatedRole.getIdLong() == role.getIdLong());
            if (!alreadyAdded) {
                deduplicatedRoles.add(role);
            }
        }
        roles.clear();
        roles.addAll(deduplicatedRoles);
    }

    private void deduplicateRolesUsing(List<Role> modifyRoles, List<Role> referenceRoles) {
        final List<Role> deduplicatedRoles = new ArrayList<>();
        for (Role role : modifyRoles) {
            final Role preferredRole = referenceRoles.stream().filter(referenceRole -> referenceRole.getIdLong() == role.getIdLong()).findFirst().orElse(role);
            final boolean alreadyAdded = deduplicatedRoles.stream().anyMatch(deduplicatedRole -> deduplicatedRole.getIdLong() == preferredRole.getIdLong());
            if (!alreadyAdded) {
                deduplicatedRoles.add(preferredRole);
            }
        }
        modifyRoles.clear();
        modifyRoles.addAll(deduplicatedRoles);
    }
}
