package fsi.listeners.selfrole;

import fsi.CommandUtil;
import fsi.listeners.utility.ISlash;
import fsi.startup.ApplicationStartupLogger;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.interactions.components.selections.SelectOption;
import net.dv8tion.jda.api.interactions.components.selections.StringSelectMenu;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
public class SelfroleSlash extends ListenerAdapter implements ISlash {

    private final static int MAX_OPTIONS_COUNT = 25;
    private final static int OTHER_OPTIONS_COUNT = 3;
    private final static int MAX_ROLES_PER_SELFROLE_SELECTION_COUNT = MAX_OPTIONS_COUNT - OTHER_OPTIONS_COUNT;

    public SelfroleSlash() {
        ApplicationStartupLogger.subStep(log, "Creating " + this.getClass().getSimpleName() + " as " + this.getName());
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        if (!event.getName().equals(this.getName())) {
            return;
        }

        event.deferReply().queue();

        try {
            if (!event.getMember().hasPermission(Permission.BAN_MEMBERS) && !event.getMember().hasPermission(Permission.KICK_MEMBERS)) {
                log.warn("User [{} / {}] tried to create a selfrole selector without proper permissions.", event.getMember().getId(), event.getMember().getEffectiveName());
                event.getHook().sendMessage("**Du bist nicht berechtigt**, um einen Selfrole-Selektor zu bauen!").setEphemeral(true).queue();
                return;
            }

            final String title = event.getOption("titel").getAsString();
            final String description = event.getOption("beschreibung").getAsString().replace("\\n", "\n");
            final int maxRoles = event.getOption("max") != null ? (int) event.getOption("max").getAsDouble() : MAX_ROLES_PER_SELFROLE_SELECTION_COUNT;

            final List<Role> roles = new ArrayList<>();
            for (int i = 1; i < MAX_ROLES_PER_SELFROLE_SELECTION_COUNT + 1; i++) {
                final OptionMapping mapping = event.getOption(getNumberedRoleOptionName(i));
                if (mapping != null) {
                    roles.add(mapping.getAsRole());
                }
            }

            if (roles.isEmpty()) {
                log.warn("[{} / {}] tried to create a selfrole selector [{}] without any roles, blocked the attempt.", event.getMember().getId(), event.getMember().getEffectiveName(), title);
                event.getHook().sendMessage("Du musst **mindestens eine Rolle angeben**, sonst kann ich daraus kein Drop-Down bauen.\nDu kannst **bis zu " + MAX_ROLES_PER_SELFROLE_SELECTION_COUNT + " Rollen** über weitere Parameter hinzufügen.").setEphemeral(true).queue();
                return;
            }

            final List<SelectOption> roleDropDownOptions = roles.stream()
                    .map(role -> SelectOption.of(role.getName(), role.getId()))
                    .collect(Collectors.toList());

            log.info("Creating selfrole selector with title: [{}], description: [{}], max roles: [{}], roles: {}", title, description.replace("\n", "\\n"), maxRoles, roles);

            final EmbedBuilder explanationEmbed = new EmbedBuilder()
                    .setTitle(title)
                    .setDescription(description)
                    .setColor(0xffffff);

            final StringSelectMenu dropDownMenu = StringSelectMenu.create("selfrole")
                    .setPlaceholder("Wähle aus...")
                    .setMinValues(0)
                    .setMaxValues(maxRoles)
                    .addOptions(roleDropDownOptions)
                    .build();

            event.getGuild().getTextChannelById(event.getChannelIdLong())
                    .sendMessageEmbeds(explanationEmbed.build())
                    .setActionRow(dropDownMenu)
                    .queue();

            log.info("Selfrole selector created successfully in channel [{}] by user [{} / {}]", CommandUtil.formatChannelName(event), event.getMember().getId(), event.getMember().getEffectiveName());
            event.getHook().deleteOriginal().queue();
        } catch (Exception e) {
            CommandUtil.logCommandError(event, e, "Es gab einen Fehler beim Erstellen der Selfrole-Nachricht.", false);
        }
    }

    @Override
    public SlashCommandData constructCommand() {
        final SlashCommandData slashCommandData = Commands.slash(this.getName(), "Ein Rollen-Picker für Nutzer erstellen")
                .addOptions(
                        new OptionData(OptionType.STRING, "titel", "Titel für das Embed über der Auswahl").setRequired(true),
                        new OptionData(OptionType.STRING, "beschreibung", "Text-Inhalt für das Embed über der Auswahl").setRequired(true),
                        new OptionData(OptionType.NUMBER, "max", "Wie viele Rollen dürfen maximal ausgewählt werden?").setRequired(false)
                );

        ApplicationStartupLogger.subStep(log, "Selfrole command max roles count: " + MAX_ROLES_PER_SELFROLE_SELECTION_COUNT);
        for (int i = 1; i < MAX_ROLES_PER_SELFROLE_SELECTION_COUNT + 1; i++) {
            slashCommandData.addOptions(new OptionData(OptionType.ROLE, getNumberedRoleOptionName(i), "Rolle #" + i + " (optional)").setRequired(false));
        }

        return slashCommandData;
    }

    private String getNumberedRoleOptionName(int i) {
        return "role-" + (i <= 9 ? "0" + i : "" + i);
    }

    @Override
    public String getName() {
        return "selfrole";
    }

    @Override
    public String getHelp() {
        return "Dieser Befehl erlaubt es, eine Kombination aus einem Embed (Titel, Beschreibung) und einem Dropdown-Element (Rollenselektion) zu erzeugen, mit der ein Nutzer sich selbst Rollen vergeben kann.\n" +
                "Im Moment können bis zu " + MAX_ROLES_PER_SELFROLE_SELECTION_COUNT + " Rollen pro Drop-Down hinzugefügt werden.\n" +
                "Um diesen Befehl ausführen zu können, musst du genügend Berechtigungen besitzen.";
    }
}
