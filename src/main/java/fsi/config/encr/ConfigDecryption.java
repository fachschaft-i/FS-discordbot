package fsi.config.encr;

import fsi.config.SystemInfo;
import lombok.extern.log4j.Log4j2;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.nio.file.Files;
import java.security.PublicKey;
import java.util.Scanner;

@Log4j2
public class ConfigDecryption {

    public static void main(String[] args) throws Exception {
        testDecryption();
    }

    protected static void testDecryption() {
        final Scanner scanner = new Scanner(System.in);
        log.info("");
        log.info("FS-I Bot configuration file decryption tool");

        log.info("Enter the directory where the encrypted files are located (public_key_encrypted.pem, aes_key.rsa, env.aes):");
        String directory = scanner.nextLine();
        if (directory.isEmpty()) {
            directory = "crypt_config";
        }

        log.info("Do you want to use the [s]ystem fingerprint or enter a [c]ustom key for AES encryption?");
        final String useSystemFingerprintOrCustomKey = scanner.nextLine();
        final ConfigEncryption.AesKeyProvider systemAesKey;
        if ("s".equalsIgnoreCase(useSystemFingerprintOrCustomKey)) {
            systemAesKey = ConfigEncryption::generateAESKeyFromSystemFingerprint;
        } else if ("c".equalsIgnoreCase(useSystemFingerprintOrCustomKey)) {
            systemAesKey = () -> {
                log.info("Enter the custom key for AES encryption:");
                final String customKey = scanner.nextLine();
                return AES256Encryption.generateKeyFromBytes(AES256Encryption.repeatOrCropBytesUntilLengthReached(customKey.getBytes(), 32));
            };
        } else {
            log.error("Invalid input. Please enter 's' or 'e'.");
            return;
        }

        try {
            final String decryptedEnv = performDecryption(new File(directory), systemAesKey);
            log.info("Decrypted .env file:\n{}", decryptedEnv);
        } catch (Exception e) {
            log.error("Error during decryption: {}", e.getMessage());
        }
    }

    public static String performDecryption(File baseDir, ConfigEncryption.AesKeyProvider systemAesKey) throws Exception {
        if (!baseDir.exists()) {
            throw new IllegalArgumentException("Directory does not exist: " + baseDir.getAbsolutePath());
        }
        final SecretKey aesS = systemAesKey.getAesKey();
        final PublicKey rsaPublicKey = decryptPublicKeyWithAES(baseDir, aesS);
        final SecretKey aesR = decryptAESKeyWithRSA(baseDir, rsaPublicKey);
        final byte[] decryptedEnvBytes = decryptEnvFileWithAES(baseDir, aesR);
        return new String(decryptedEnvBytes);
    }

    private static PublicKey decryptPublicKeyWithAES(File baseDir, SecretKey aesS) throws Exception {
        final byte[] encryptedPublicKeyBytes = Files.readAllBytes(new File(baseDir, "public_key_encrypted.pem").toPath());
        final byte[] decryptedPublicKeyBytes = AES256Encryption.decryptByteArray(encryptedPublicKeyBytes, aesS);
        return RSAEncryption.getPublicKeyFromBytes(decryptedPublicKeyBytes);
    }

    private static SecretKey decryptAESKeyWithRSA(File baseDir, PublicKey rsaPublicKey) throws Exception {
        final byte[] encryptedAesRBytes = Files.readAllBytes(new File(baseDir, "aes_key.rsa").toPath());
        final byte[] decryptedAesRBytes = RSAEncryption.decryptByteArray(encryptedAesRBytes, rsaPublicKey);
        return new SecretKeySpec(decryptedAesRBytes, "AES");
    }

    private static byte[] decryptEnvFileWithAES(File baseDir, SecretKey aesR) throws Exception {
        final byte[] encryptedEnvBytes = Files.readAllBytes(new File(baseDir, "env.aes").toPath());
        return AES256Encryption.decryptByteArray(encryptedEnvBytes, aesR);
    }
}
