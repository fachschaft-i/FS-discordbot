package fsi.config.encr;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Base64;

public class AES256Encryption {

    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/GCM/NoPadding";
    private static final int KEY_SIZE = 256;
    private static final int IV_SIZE = 12;
    private static final int TAG_SIZE = 128;

    public static SecretKey generateKey() throws Exception {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM);
        keyGenerator.init(KEY_SIZE);
        return keyGenerator.generateKey();
    }

    public static SecretKey generateKeyFromBytes(byte[] keyBytes) {
        return new SecretKeySpec(keyBytes, ALGORITHM);
    }

    private static byte[] generateIV() {
        byte[] iv = new byte[IV_SIZE];
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        return iv;
    }

    public static String encryptString(String input, SecretKey key) throws Exception {
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        byte[] iv = generateIV();
        GCMParameterSpec spec = new GCMParameterSpec(TAG_SIZE, iv);
        cipher.init(Cipher.ENCRYPT_MODE, key, spec);
        byte[] encrypted = cipher.doFinal(input.getBytes(StandardCharsets.UTF_8));
        byte[] encryptedWithIV = new byte[IV_SIZE + encrypted.length];
        System.arraycopy(iv, 0, encryptedWithIV, 0, IV_SIZE);
        System.arraycopy(encrypted, 0, encryptedWithIV, IV_SIZE, encrypted.length);
        return Base64.getEncoder().encodeToString(encryptedWithIV);
    }

    public static byte[] encryptByteArray(byte[] input, SecretKey key) throws Exception {
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        byte[] iv = generateIV();
        GCMParameterSpec spec = new GCMParameterSpec(TAG_SIZE, iv);
        cipher.init(Cipher.ENCRYPT_MODE, key, spec);
        byte[] encrypted = cipher.doFinal(input);
        byte[] encryptedWithIV = new byte[IV_SIZE + encrypted.length];
        System.arraycopy(iv, 0, encryptedWithIV, 0, IV_SIZE);
        System.arraycopy(encrypted, 0, encryptedWithIV, IV_SIZE, encrypted.length);
        return encryptedWithIV;
    }

    public static String decryptString(String input, SecretKey key) throws Exception {
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        byte[] decoded = Base64.getDecoder().decode(input);
        byte[] iv = new byte[IV_SIZE];
        System.arraycopy(decoded, 0, iv, 0, IV_SIZE);
        GCMParameterSpec spec = new GCMParameterSpec(TAG_SIZE, iv);
        cipher.init(Cipher.DECRYPT_MODE, key, spec);
        byte[] original = cipher.doFinal(decoded, IV_SIZE, decoded.length - IV_SIZE);
        return new String(original, StandardCharsets.UTF_8);
    }

    public static byte[] decryptByteArray(byte[] input, SecretKey key) throws Exception {
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        byte[] iv = new byte[IV_SIZE];
        System.arraycopy(input, 0, iv, 0, IV_SIZE);
        GCMParameterSpec spec = new GCMParameterSpec(TAG_SIZE, iv);
        cipher.init(Cipher.DECRYPT_MODE, key, spec);
        return cipher.doFinal(input, IV_SIZE, input.length - IV_SIZE);
    }

    public static void encryptFile(String inputFile, String outputFile, SecretKey key) throws Exception {
        byte[] inputBytes = Files.readAllBytes(Paths.get(inputFile));
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        byte[] iv = generateIV();
        GCMParameterSpec spec = new GCMParameterSpec(TAG_SIZE, iv);
        cipher.init(Cipher.ENCRYPT_MODE, key, spec);
        byte[] encryptedBytes = cipher.doFinal(inputBytes);
        try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
            outputStream.write(iv);
            outputStream.write(encryptedBytes);
        }
    }

    public static void decryptFile(String inputFile, String outputFile, SecretKey key) throws Exception {
        byte[] inputBytes = Files.readAllBytes(Paths.get(inputFile));
        byte[] iv = new byte[IV_SIZE];
        System.arraycopy(inputBytes, 0, iv, 0, IV_SIZE);
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        GCMParameterSpec spec = new GCMParameterSpec(TAG_SIZE, iv);
        cipher.init(Cipher.DECRYPT_MODE, key, spec);
        byte[] originalBytes = cipher.doFinal(inputBytes, IV_SIZE, inputBytes.length - IV_SIZE);
        try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
            outputStream.write(originalBytes);
        }
    }

    public static byte[] decryptFileToByteArray(String inputFile, SecretKey key) throws Exception {
        byte[] inputBytes = Files.readAllBytes(Paths.get(inputFile));
        return decryptBytes(inputBytes, key);
    }

    public static String decryptFileToString(String inputFile, SecretKey key) throws Exception {
        byte[] inputBytes = Files.readAllBytes(Paths.get(inputFile));
        byte[] decryptedBytes = decryptBytes(inputBytes, key);
        return new String(decryptedBytes);
    }

    private static byte[] decryptBytes(byte[] inputBytes, SecretKey key) throws Exception {
        byte[] iv = new byte[IV_SIZE];
        System.arraycopy(inputBytes, 0, iv, 0, IV_SIZE);
        GCMParameterSpec spec = new GCMParameterSpec(TAG_SIZE, iv);
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, key, spec);
        return cipher.doFinal(inputBytes, IV_SIZE, inputBytes.length - IV_SIZE);
    }

    public static byte[] repeatOrCropBytesUntilLengthReached(byte[] input, int length) {
        if (input.length == length) {
            return input;
        }

        if (input.length > length) {
            byte[] cropped = new byte[length];
            System.arraycopy(input, 0, cropped, 0, length);
            return cropped;
        } else {
            byte[] repeated = new byte[length];
            for (int i = 0; i < length; i++) {
                repeated[i] = input[i % input.length];
            }
            return repeated;
        }
    }

    public static void main(String[] args) throws Exception {
        SecretKey key = generateKey();
        String originalString = "Sensitive data";
        String encryptedString = encryptString(originalString, key);
        String decryptedString = decryptString(encryptedString, key);

        System.out.println("Original: " + originalString);
        System.out.println("Encrypted: " + encryptedString);
        System.out.println("Decrypted: " + decryptedString);
    }
}

