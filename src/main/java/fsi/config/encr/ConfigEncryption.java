package fsi.config.encr;

import fsi.config.SystemInfo;
import lombok.extern.log4j.Log4j2;

import javax.crypto.SecretKey;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.util.Scanner;

@Log4j2
public class ConfigEncryption {

    public static void main(String[] args) throws Exception {
        final Scanner scanner = new Scanner(System.in);
        log.info("");
        log.info("FS-I Bot configuration file encryption tool");

        log.info("Enter what you want to do:");
        log.info(" - [e]ncrypt a new configuration file");
        log.info(" - [d]ecrypt an existing configuration file");
        log.info(" - [s]how the system fingerprint");
        final String encryptOrShowFingerprint = scanner.nextLine();
        if ("e".equalsIgnoreCase(encryptOrShowFingerprint)) {
            performEncryption(scanner);
        } else if ("d".equalsIgnoreCase(encryptOrShowFingerprint)) {
            ConfigDecryption.testDecryption();
        } else if ("s".equalsIgnoreCase(encryptOrShowFingerprint)) {
            log.info("System fingerprint: {}", SystemInfo.createSystemFingerprint());
            log.info("System fingerprint (as key): {}", ConfigEncryption.generateAESKeyFromSystemFingerprint().getEncoded());
            log.info("MAC Address: {}", SystemInfo.getMacAddress());
            log.info("CPU: {}", SystemInfo.getCpuInfo());
            log.info("OS: {}", SystemInfo.getOperatingSystem());
            log.info("OS Version (unused): {}", SystemInfo.getOperatingSystemVersion());
            log.info("User: {}", SystemInfo.getUserInfo());
            // log.info("Additional: {}", SystemInfo.getAdditionalInfo()); // do NOT show the additional info unless necessary
        } else {
            log.error("Invalid input. Please enter 'e' or 's'.");
        }
    }

    private static void performEncryption(Scanner scanner) throws Exception {
        log.info("Do you want to [g]enerate new keys or [l]oad existing keys?");
        final String generateNewPairOrLoad = scanner.nextLine();
        final RsaKeyProvider rsaKeyProvider;
        final boolean writeKeys;
        if ("g".equalsIgnoreCase(generateNewPairOrLoad)) {
            rsaKeyProvider = RSAEncryption::generateKeyPair;
            writeKeys = true;
        } else if ("l".equalsIgnoreCase(generateNewPairOrLoad)) {
            rsaKeyProvider = () -> {
                log.info("Enter the path to the public key file (leave empty for public_key.pem):");
                String input = scanner.nextLine();
                final String publicKeyPath = input.isEmpty() ? "public_key.pem" : input;
                log.info("Enter the path to the private key file (leave empty for private_key.pem):");
                input = scanner.nextLine();
                final String privateKeyPath = input.isEmpty() ? "private_key.pem" : input;
                return new KeyPair(RSAEncryption.loadPublicKey(publicKeyPath), RSAEncryption.loadPrivateKey(privateKeyPath));
            };
            writeKeys = false;
        } else {
            log.error("Invalid input. Please enter 'g' or 'l'.");
            return;
        }

        log.info("Do you want to use the [s]ystem fingerprint or enter a [c]ustom key for AES encryption?");
        final String useSystemFingerprintOrCustomKey = scanner.nextLine();
        final AesKeyProvider systemAesKey;
        if ("s".equalsIgnoreCase(useSystemFingerprintOrCustomKey)) {
            systemAesKey = ConfigEncryption::generateAESKeyFromSystemFingerprint;
        } else if ("c".equalsIgnoreCase(useSystemFingerprintOrCustomKey)) {
            systemAesKey = () -> {
                log.info("Enter the custom key for AES encryption:");
                final String customKey = scanner.nextLine();
                return AES256Encryption.generateKeyFromBytes(AES256Encryption.repeatOrCropBytesUntilLengthReached(customKey.getBytes(), 32));
            };
        } else {
            log.error("Invalid input. Please enter 's' or 'e'.");
            return;
        }

        performEncryption(rsaKeyProvider, writeKeys, systemAesKey);

        // move the public_key_encrypted.pem, aes_key.rsa, and env.aes to a subdir
        final File encryptedFilesDir = new File("crypt_config");
        // delete the directory if it exists
        if (encryptedFilesDir.exists()) {
            for (File file : encryptedFilesDir.listFiles()) {
                file.delete();
            }
            encryptedFilesDir.delete();
        }
        if (!encryptedFilesDir.exists()) {
            encryptedFilesDir.mkdir();
        }
        Files.move(Paths.get("public_key_encrypted.pem"), Paths.get(encryptedFilesDir.getPath(), "public_key_encrypted.pem"));
        Files.move(Paths.get("aes_key.rsa"), Paths.get(encryptedFilesDir.getPath(), "aes_key.rsa"));
        Files.move(Paths.get("env.aes"), Paths.get(encryptedFilesDir.getPath(), "env.aes"));

        log.info("");
        log.info("Encryption completed successfully.");
        log.info("Your data is available in the following files:");
        log.info(" public_key.pem, private_key.pem, public_key_encrypted.pem, aes_key.rsa, env.aes");
        log.info("");
        log.info("Next steps for each of the files:");
        log.info(" 1. Store public_key.pem and especially private_key.pem in a secure location.");
        log.info("    You will need them again when encrypting new configuration files.");
        log.info(" 2. Store public_key_encrypted.pem, aes_key.rsa, and env.aes on the target machine in a directory");
        log.info("    \"crypt_config\" in the same directory as the bot jar file.");
    }

    public interface RsaKeyProvider {
        KeyPair getKeyPair() throws Exception;
    }

    public interface AesKeyProvider {
        SecretKey getAesKey() throws Exception;
    }

    public static void performEncryption(RsaKeyProvider rsaKeyProvider, boolean writeRsaKeys, AesKeyProvider systemAesKey) throws Exception {
        final KeyPair keyPair = rsaKeyProvider.getKeyPair();
        if (writeRsaKeys) {
            saveRSAKeys(keyPair);
        }

        final SecretKey aesS = systemAesKey.getAesKey();
        final byte[] encryptedPublicKeyBytes = encryptPublicKeyWithAES(aesS);
        saveEncryptedPublicKey(encryptedPublicKeyBytes);

        final SecretKey aesR = generateAESKey();
        final byte[] encryptedAesRBytes = encryptAESKeyWithRSA(aesR);
        saveEncryptedAESKey(encryptedAesRBytes);

        final byte[] encryptedEnvBytes = encryptEnvFileWithAES(aesR);
        saveEncryptedEnvFile(encryptedEnvBytes);
    }

    private static void saveRSAKeys(KeyPair keyPair) throws Exception {
        RSAEncryption.saveKey(keyPair.getPublic(), "public_key.pem");
        RSAEncryption.saveKey(keyPair.getPrivate(), "private_key.pem");
    }

    public static SecretKey generateAESKeyFromSystemFingerprint() throws Exception {
        final String systemFingerprint = SystemInfo.createSystemFingerprint();
        return AES256Encryption.generateKeyFromBytes(AES256Encryption.repeatOrCropBytesUntilLengthReached(systemFingerprint.getBytes(), 32));
    }

    private static byte[] encryptPublicKeyWithAES(SecretKey aesS) throws Exception {
        final byte[] publicKeyBytes = Files.readAllBytes(Paths.get("public_key.pem"));
        return AES256Encryption.encryptByteArray(publicKeyBytes, aesS);
    }

    private static void saveEncryptedPublicKey(byte[] encryptedPublicKeyBytes) throws Exception {
        Files.write(Paths.get("public_key_encrypted.pem"), encryptedPublicKeyBytes);
    }

    private static SecretKey generateAESKey() throws Exception {
        return AES256Encryption.generateKey();
    }

    private static byte[] encryptAESKeyWithRSA(SecretKey aesR) throws Exception {
        final byte[] aesRBytes = aesR.getEncoded();
        final PrivateKey rsaPrivateKey = RSAEncryption.loadPrivateKey("private_key.pem");
        return RSAEncryption.encryptByteArray(aesRBytes, rsaPrivateKey);
    }

    private static void saveEncryptedAESKey(byte[] encryptedAesRBytes) throws Exception {
        Files.write(Paths.get("aes_key.rsa"), encryptedAesRBytes);
    }

    private static byte[] encryptEnvFileWithAES(SecretKey aesR) throws Exception {
        final byte[] envBytes = Files.readAllBytes(Paths.get(".env"));
        return AES256Encryption.encryptByteArray(envBytes, aesR);
    }

    private static void saveEncryptedEnvFile(byte[] encryptedEnvBytes) throws Exception {
        Files.write(Paths.get("env.aes"), encryptedEnvBytes);
    }
}
