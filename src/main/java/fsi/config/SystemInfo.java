package fsi.config;

import lombok.extern.log4j.Log4j2;
import okhttp3.OkHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Collections;
import java.util.StringJoiner;

@Log4j2
public final class SystemInfo {

    public static String getMacAddress() throws SocketException {
        for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
            if (networkInterface.getHardwareAddress() != null) {
                StringBuilder mac = new StringBuilder();
                for (byte b : networkInterface.getHardwareAddress()) {
                    mac.append(String.format("%02X", b));
                }
                return mac.toString();
            }
        }
        return null;
    }

    public static String getCpuInfo() throws Exception {
        final String os = getOperatingSystem().toLowerCase();
        if (os.contains("win")) {
            final String result = execCommand("wmic cpu get name");
            if (result.contains("Name") && result.contains("\n")) {
                return result.split("\n")[1].trim();
            }
            return result;
        } else if (os.contains("nix") || os.contains("nux")) {
            return execCommand("cat /proc/cpuinfo | grep 'model name' | head -1 | cut -d':' -f2");
        } else if (os.contains("mac")) {
            return execCommand("sysctl -n machdep.cpu.brand_string");
        }
        return null;
    }

    public static String getOperatingSystem() {
        return System.getProperty("os.name");
    }

    public static String getOperatingSystemVersion() {
        return System.getProperty("os.version");
    }

    public static String getUserInfo() {
        return System.getProperty("user.name");
    }

    public static String getAdditionalInfo() throws Exception {
        return "YAN_WAS_HERE" + fetchUrl("https://gitlab.com/Yan_Wittmann_HSMA/fsi-bot-data/-/raw/main/fgprsffx/602b72c6-6e65-4c01-ad91-40c8772d95e4");
    }

    public static String createSystemFingerprint() throws Exception {
        final String macAddress = getMacAddress();
        final String cpuInfo = getCpuInfo();
        final String osInfo = getOperatingSystem();// + " " + getOperatingSystemVersion();
        final String userInfo = getUserInfo();
        final String additionalInfo = getAdditionalInfo();

        final String combined = macAddress + cpuInfo + osInfo + userInfo + additionalInfo;

        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        final byte[] hash = digest.digest(combined.getBytes(StandardCharsets.UTF_8));

        final StringBuilder hexString = new StringBuilder();
        for (byte b : hash) {
            hexString.append(String.format("%02x", b));
        }
        return hexString.toString();
    }

    private static String fetchUrl(String url) throws Exception {
        final OkHttpClient okHttpClient = new OkHttpClient();
        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        final okhttp3.Response response = okHttpClient.newCall(request).execute();
        return response.body().string();
    }

    private static String execCommand(String command) throws Exception {
        final String[] cmd;
        final String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("win")) {
            cmd = new String[]{"cmd.exe", "/c", command};
        } else if (os.contains("nix") || os.contains("nux")) {
            cmd = new String[]{"sh", "-c", command};
        } else {
            cmd = new String[]{"sh", "-c", command};
        }

        final Process process = new ProcessBuilder(cmd).start();
        final BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
        final BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

        final StringJoiner output = new StringJoiner("\n");
        String line;

        while ((line = stdInput.readLine()) != null) {
            final String trimmedLine = line.trim();
            if (!trimmedLine.isEmpty()) {
                output.add(trimmedLine);
            }
        }

        final StringJoiner errorOutput = new StringJoiner("\n");
        while ((line = stdError.readLine()) != null) {
            errorOutput.add(line);
        }

        final int exitCode = process.waitFor();
        if (exitCode != 0) {
            throw new RuntimeException("Command execution failed: " + errorOutput);
        }

        return output.toString().trim();
    }
}

