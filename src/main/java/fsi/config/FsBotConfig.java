package fsi.config;

import fsi.config.encr.ConfigDecryption;
import fsi.config.encr.ConfigEncryption;
import fsi.startup.ApplicationStartupLogger;
import io.github.cdimascio.dotenv.Dotenv;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
public class FsBotConfig {

    private static final Dotenv dotenv;
    private static final Map<String, String> cryptConfig = new LinkedHashMap<>();

    static {
        ApplicationStartupLogger.fullStep(log, "Loading application configuration");
        ApplicationStartupLogger.subStep(log, "... from .env (dotenv)");
        dotenv = Dotenv.load();
        ApplicationStartupLogger.subStep(log, "Loaded " + dotenv.entries().size() + " properties from .env (dotenv)");

        final File cryptConfigDirectory = new File("crypt_config");
        if (cryptConfigDirectory.exists()) {
            ApplicationStartupLogger.fullStep(log, "... from crypt config");
            try {
                final String inputCryptConfig = ConfigDecryption.performDecryption(cryptConfigDirectory, ConfigEncryption::generateAESKeyFromSystemFingerprint);
                final String[] split = inputCryptConfig.split("\n");
                for (String line : split) {
                    final String[] splitLine = line.trim().split("=");
                    if (splitLine.length == 2) {
                        cryptConfig.put(splitLine[0], splitLine[1]);
                    }
                }
                ApplicationStartupLogger.subStep(log, "Loaded " + cryptConfig.size() + " properties from crypt config");
            } catch (Exception e) {
                ApplicationStartupLogger.subStepWarn(log, "Error during loading of crypt config: " + e.getMessage());
                log.error("Error during loading of crypt config from directory: {}", cryptConfigDirectory.getAbsolutePath(), e);
            }
        }

        final AtomicInteger namedOptionsFoundCount = new AtomicInteger(0);
        for (Attribute key : Attribute.values()) {
            final String value = get(key);
            final boolean found = value != null;
            if (!found) {
                ApplicationStartupLogger.subStepWarn(log, "Named attribute was not found in .env (dotenv): " + Arrays.toString(key.getKey()) + " " + key.getDescription());
            } else {
                namedOptionsFoundCount.incrementAndGet();
            }
        }

        if (namedOptionsFoundCount.get() != Attribute.values().length) {
            ApplicationStartupLogger.subStepWarn(log, "Only [" + namedOptionsFoundCount.get() + " / " + Attribute.values().length + "] all named attributes were found during loading of .env (dotenv) or crypt config., see logs above for more details.");
        } else {
            ApplicationStartupLogger.subStep(log, "All [" + namedOptionsFoundCount.get() + " / " + Attribute.values().length + "] named attributes were found during loading of .env (dotenv) or crypt config.");
        }
    }

    public static String get(Attribute key) {
        for (String candidate : key.getKey()) {
            final String dotenvValue = dotenv.get(candidate);
            if (dotenvValue != null) {
                return dotenvValue;
            }
            final String cryptConfigValue = cryptConfig.get(candidate);
            if (cryptConfigValue != null) {
                return cryptConfigValue;
            }
        }

        log.error("Attribute {} was not set in .env (dotenv) or crypt config. Here's a hint on what it's value should look like: {}", Arrays.toString(key.getKey()), key.getDescription());

        return null;
    }

    public static long getLong(Attribute key) {
        final String value = get(key);
        assertValueNotNull(key, value);
        return Long.parseLong(value);
    }

    public static String[] getCsvStringArray(Attribute key) {
        final String value = get(key);
        assertValueNotNull(key, value);
        final String[] split = value.split(", ");
        if (split.length == 1 && split[0].isEmpty()) {
            return new String[0];
        } else {
            return split;
        }
    }

    public static List<String> getCsvStringList(Attribute key) {
        return Arrays.asList(getCsvStringArray(key));
    }

    private static void assertValueNotNull(Attribute key, String value) {
        if (value == null) {
            throw new IllegalStateException("Expected FS-I bot config value for attribute " + Arrays.toString(key.getKey()) + " to not be null. Here's a hint on what it's value should look like: " + key.getDescription());
        }
    }

    public static void init() {
    }

    @Getter
    public enum Attribute {
        DISCORD_BOT_TOKEN("The token obtained by visiting the Discord Developer Portal and creating a bot there.", "TOKEN"),

        MAIL_CLIENT_ID("See readme.md > bot configuration > google mail for how to obtain this value.", "CLIENTID"),
        MAIL_CLIENT_SECRET("See readme.md > bot configuration > google mail for how to obtain this value.", "CLIENTSECRET"),
        MAIL_REFRESH_TOKEN("See readme.md > bot configuration > google mail for how to obtain this value.", "REFRESHTOKEN"),
        MAIL_SENDER_EMAIL_ADDRESS("The E-Mail address used to generate the refresh token. See readme.md > bot configuration > google mail for how to obtain this value.", "MAIL"),
        MAIL_SENDER_USER("The part before the @gmail.com in the E-Mail address used to generate the refresh token."),

        MAIL_SMTP_HOST("Usually: smtp.gmail.com"),
        MAIL_SMTP_PORT("Usually: 587"),
        MAIL_SMTP_STARTTLS_ENABLE("Usually: true"),
        MAIL_SMTP_STARTTLS_REQUIRED("Usually: true"),
        MAIL_SMTP_AUTH_MECHANISMS("Usually: XOAUTH2"),

        PRIMARY_BOT_GUILD_ID("The Guild ID of the Server the bot will operate on."),

        VERIFY_CHANNEL_ID("The Channel ID of the channel that the 'verify' command will be limited to."),
        VERIFY_COOLDOWN_DURATION_MS("The time in milliseconds between attempts of the user to verify themselves using the 'verify' command"),
        VERIFY_ADD_USER_ROLES_ON_VERIFICATION("A CSV list of Role IDs that will be added to a user upon successful verification."),
        VERIFY_REMOVE_USER_ROLES_ON_VERIFICATION("A CSV list of Role IDs that will be removed from a user upon successful verification."),
        VERIFY_REDIRECT_USER_ON_VERIFICATION("The Channel ID of the channel the user will be presented the option to jump to upon successful verification."),

        VOICE_CHANNEL_ID_CREATE_CHANNEL("The Voice Channel ID of the audio channel that will create a new channel using the user's name and move them to there upon joining."),
        VOICE_CHANNEL_IDS_IGNORE("All audio channels with the term 'Lerngruppe' in their name are monitored for their users and removed when their member count reaches 0. This attribute is a CSV List of Voice Channel IDs that will be ignored in this process no matter what name they have."),

        ALTKL_NEXTCLOUD_BASE_URL("The base URL of the Nextcloud instance."),
        ALTKL_NEXTCLOUD_WEB_DAV("The WebDAV URL of the Nextcloud instance."),
        ALTKL_NEXTCLOUD_KLAUSUREN_PATH("The path on the Nextcloud instance where the altklausur files are available."),
        ALTKL_NEXTCLOUD_QUERY_LOG_PATH("The path on the Nextcloud instance where a text file lies that should have the user queries appended to."),
        ALTKL_NEXTCLOUD_UPLOAD_SHARE_ZIPS_PATH("The file path on the Nextcloud instance where the files will be uploaded to."),
        ALTKL_NEXTCLOUD_CACHE_VALIDITY_DURATION("The time in milliseconds after which the cache of the altklausur files will be invalidated and re-fetched."),
        ALTKL_NEXTCLOUD_USER_UPLOAD_SHARE_ALTKLAUSUREN_PATH("The path on the Nextcloud instance where the user uploaded altklausur directories will be created in."),
        ALTKL_NEXTCLOUD_CHANNEL("The Channel ID of the channel where the altklausur commands should be posted in."),
        ALTKL_NEXTCLOUD_USER("The username to use when authenticating with the Nextcloud instance."),
        ALTKL_NEXTCLOUD_PASSWORD("The password to use when authenticating with the Nextcloud instance."),

        ADMIN_USER_ID("The User ID of the bot administrator who will have special access to certain functions.");

        private final String[] key;
        private final String description;

        Attribute() {
            this.key = new String[]{this.name()};
            this.description = "Attribute " + Arrays.toString(this.key);
        }

        Attribute(String description, String... additionalKeys) {
            final List<String> keys = new ArrayList<>();
            keys.add(this.name());
            keys.addAll(Arrays.asList(additionalKeys));
            this.key = keys.toArray(new String[0]);
            this.description = description;
        }

        Attribute(String description) {
            this.key = new String[]{this.name()};
            this.description = description;
        }
    }
}
