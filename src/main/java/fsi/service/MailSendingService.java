package fsi.service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.MemoryDataStoreFactory;
import fsi.config.FsBotConfig;
import fsi.startup.ApplicationStartupLogger;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

@Log4j2
public class MailSendingService {

    private final Properties mailProperties;

    public MailSendingService() {
        ApplicationStartupLogger.fullStep(log,"Creating " + this.getClass().getSimpleName());

        this.mailProperties = System.getProperties();
        this.mailProperties.put("mail.smtp.host", FsBotConfig.get(FsBotConfig.Attribute.MAIL_SMTP_HOST));
        this.mailProperties.put("mail.smtp.port", FsBotConfig.get(FsBotConfig.Attribute.MAIL_SMTP_PORT));
        this.mailProperties.put("mail.smtp.starttls.enable", FsBotConfig.get(FsBotConfig.Attribute.MAIL_SMTP_STARTTLS_ENABLE));
        this.mailProperties.put("mail.smtp.starttls.required", FsBotConfig.get(FsBotConfig.Attribute.MAIL_SMTP_STARTTLS_REQUIRED));
        this.mailProperties.put("mail.smtp.auth.mechanisms", FsBotConfig.get(FsBotConfig.Attribute.MAIL_SMTP_AUTH_MECHANISMS));
    }

    public void verifyAuthenticated() throws IOException {
        ApplicationStartupLogger.subStep(log, "Validating email access (internet access required)");
        final String accessToken = getAccessToken();
        if (accessToken == null) {
            ApplicationStartupLogger.subStepWarn(log, "Access token returned from authentication is null, but no exception was thrown. Please analyze this issue further.");
        }
    }

    public void sendMail(String to, String subject, String body) throws MessagingException, IOException {
        log.info("Preparing mail to [{}] with subject [{}]", to, subject);

        final Session session = Session.getInstance(mailProperties);

        final MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(FsBotConfig.get(FsBotConfig.Attribute.MAIL_SENDER_EMAIL_ADDRESS)));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.setSubject(subject);
        message.setText(body);

        final String googleAuthUser = FsBotConfig.get(FsBotConfig.Attribute.MAIL_SENDER_USER);
        log.info("Sending mail to [{}] with subject [{}]", to, subject);
        Transport.send(message, googleAuthUser, getAccessToken());
        log.info("Mail sent successfully to [{}]", to);
    }

    private String getAccessToken() throws IOException {
        final String clientId = FsBotConfig.get(FsBotConfig.Attribute.MAIL_CLIENT_ID);
        final String clientSecret = FsBotConfig.get(FsBotConfig.Attribute.MAIL_CLIENT_SECRET);
        final String refreshToken = FsBotConfig.get(FsBotConfig.Attribute.MAIL_REFRESH_TOKEN);

        final List<String> scopes = Collections.singletonList("https://mail.google.com/");
        final GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                new NetHttpTransport(),
                GsonFactory.getDefaultInstance(),
                clientId,
                clientSecret,
                scopes
        ).setDataStoreFactory(MemoryDataStoreFactory.getDefaultInstance()).build();

        final TokenResponse tokenResponse = new TokenResponse().setRefreshToken(refreshToken);
        final Credential credential = flow.createAndStoreCredential(tokenResponse, clientId);
        credential.refreshToken();

        return credential.getAccessToken();
    }
}
