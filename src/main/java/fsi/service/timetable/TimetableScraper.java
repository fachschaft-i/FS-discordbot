package fsi.service.timetable;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class TimetableScraper {

    private static final String BASE_URL = "https://services.informatik.hs-mannheim.de/stundenplan/";
    private final OkHttpClient client = new OkHttpClient();

    private final File targetDir;
    private int avgDelayBetweenRequests = 3;

    public TimetableScraper(File targetDir) {
        this.targetDir = targetDir;
        if (!targetDir.exists()) {
            targetDir.mkdirs();
        }
    }

    public static void main(String[] args) {
        final TimetableScraper scraper = new TimetableScraper(new File("target/time"));
        scraper.setAvgDelayBetweenRequests(2);
        scraper.run();
    }

    public void setAvgDelayBetweenRequests(int avgDelayBetweenRequests) {
        if (avgDelayBetweenRequests < 2) {
            throw new IllegalArgumentException("Average delay between requests must be at least 2 seconds.");
        }
        this.avgDelayBetweenRequests = avgDelayBetweenRequests;
    }

    public void ensureCache() {
        if (!this.targetDir.exists() || this.targetDir.listFiles().length < 4) {
            this.run();
        }
    }

    public void clearCache() {
        if (this.targetDir.isDirectory()) {
            for (File file : this.targetDir.listFiles()) {
                file.delete();
            }
            this.allEntries.clear();
        }
    }

    public void run() {
        if (!targetDir.exists()) {
            targetDir.mkdirs();
        }

        try {
            log.info("Starting timetable scraping process.");
            String mainContent = fetchContent(BASE_URL);
            List<SemesterProgram> semesterPrograms = parseOverviewTable(mainContent);

            for (SemesterProgram program : semesterPrograms) {
                log.info("Processing study program: {} Semester: {} ({})", program.getStudyProgram(), program.getSemesterNumber(), program.getSemesterShortName());
                String timetableContent = fetchContent(program.getUrl());
                List<TimetableEntry> entries = parseTimetable(timetableContent, program);
                writeJsonToFile(program, entries);
                randomDelay();
            }

            log.info("Timetable scraping process completed successfully.");

        } catch (Exception e) {
            log.error("An error occurred during the scraping process: {}", e.getMessage(), e);
        }
    }

    private String fetchContent(String url) throws IOException {
        log.debug("Fetching content from URL: {}", url);
        Request request = new Request.Builder().url(url).build();
        try (Response response = client.newCall(request).execute()) {
            String content = response.body().string();
            log.debug("Content fetched successfully from URL: {}", url);
            return content;
        }
    }

    private List<SemesterProgram> parseOverviewTable(String content) {
        log.info("Parsing the overview table to extract study programs and semesters.");
        List<SemesterProgram> programs = new ArrayList<>();
        Document doc = Jsoup.parse(content);
        Elements rows = doc.select("table tbody tr");

        for (Element row : rows) {
            Elements cells = row.select("td");
            if (cells.size() < 2) {
                continue;
            }
            // Get the study program name
            Element programLink = cells.get(1).selectFirst("a[href]");
            if (programLink != null) {
                String studyProgramName = programLink.text();
                // Process semester links
                Elements semesterLinks = row.select("td > a[href]");
                for (Element semLink : semesterLinks) {
                    String semHref = semLink.attr("href");
                    String semText = semLink.text();

                    if (semText.contains(" ")) {
                        // Row title, not a semester
                        continue;
                    }

                    String semesterShortName = semText; // e.g., "1BCB"
                    String semesterNumber = extractSemesterNumber(semText);
                    if (semesterNumber.equals("0")) {
                        log.warn("Skipping, failed to extract semester number from: {}", semText);
                        continue;
                    }

                    String fullUrl = BASE_URL + semHref;

                    SemesterProgram semesterProgram = new SemesterProgram(
                            studyProgramName, semesterShortName, semesterNumber, fullUrl);
                    programs.add(semesterProgram);
                    log.debug("Found semester program: {} Semester: {} Short Name: {} URL: {}", studyProgramName, semesterNumber, semesterShortName, fullUrl);
                }
            }
        }

        log.info("Total semester programs found: {}", programs.size());
        return programs;
    }

    private final static Pattern SEMESTER_PATTERN = Pattern.compile("(\\d+).*");

    private String extractSemesterNumber(String semText) {
        final Matcher matcher = SEMESTER_PATTERN.matcher(semText);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return "0";
    }

    private List<TimetableEntry> parseTimetable(String content, SemesterProgram program) {
        log.info("Parsing timetable data for {} Semester: {}", program.getStudyProgram(), program.getSemesterShortName());
        List<TimetableEntry> entries = new ArrayList<>();
        Document doc = Jsoup.parse(content);
        Elements rows = doc.select("body table tr");

        // Skip header row if present
        int startIndex = 0;
        if (!rows.isEmpty() && !rows.get(0).select("th").isEmpty()) {
            startIndex = 1;
        }

        for (int i = startIndex; i < rows.size(); i++) {
            Element row = rows.get(i);
            Elements cols = row.select("td");

            if (cols.size() < 6) {
                log.warn("Row does not have enough columns to parse: {}", row.text());
                continue;
            }

            String timeText = cols.get(0).text();
            String[] timeParts = timeText.substring(2).split(" - ");
            if (timeParts.length != 2) {
                log.warn("Time format is incorrect: {}", timeText);
                if (timeText.matches(".+(\\d{2}:\\d{2}) - (\\d{2}:\\d{2}).+")) {
                    timeParts = timeText.replaceAll(".+(\\d{2}:\\d{2}) - (\\d{2}:\\d{2}).+", "$1 - $2").split(" - ");
                    log.info("Using {} instead.", (Object) timeParts);
                } else {
                    log.warn("Skipping row due to incorrect time format: {}", timeText);
                    continue;
                }
            }

            String startTime = timeParts[0];
            String endTime = timeParts[1];

            for (int j = 1; j <= 5; j++) {
                Element dayCol = cols.get(j);
                if (dayCol != null && !dayCol.text().trim().isEmpty()) {
                    Elements spans = dayCol.select("span[title]");
                    if (spans.size() >= 3) {
                        int weekdayIndex = j - 1; // Map to 0 (Monday) to 4 (Friday)

                        // Get lecture long name from title attribute
                        String lectureLongName = spans.get(0).attr("title").split(" \\(")[0];

                        // Get lecture short name from text content
                        String lectureShortName = spans.get(0).text();
                        String professorShortName = spans.get(2).text().replace("(", "").replace(")", "");

                        String locationTitle = spans.get(1).attr("title");
                        String[] locationParts = parseLocation(locationTitle);
                        String building = locationParts[0];
                        String room = locationParts[1];
                        String professor = spans.get(2).attr("title").replace("Dr. ", "");

                        TimetableEntry entry = new TimetableEntry(
                                startTime, endTime, weekdayIndex,
                                lectureLongName, lectureShortName,
                                building, room,
                                professor, professorShortName,
                                program.getSemesterShortName(),
                                program.getStudyProgram(),
                                program.getSemesterNumber()
                        );
                        entries.add(entry);

                        log.debug("Parsed entry: {}", entry);
                    } else {
                        log.warn("Not enough spans to parse in day column.");
                    }
                }
            }
        }

        return entries;
    }

    private void writeJsonToFile(SemesterProgram program, List<TimetableEntry> entries) {
        final JSONArray json = new JSONArray(entries.stream().map(TimetableEntry::toJson).collect(Collectors.toList()));

        final String sanitizedSemesterShortName = program.getSemesterShortName().replaceAll("[^a-zA-Z0-9]", "_");
        final File targetFile = new File(this.targetDir, sanitizedSemesterShortName + ".json");
        try (FileWriter file = new FileWriter(targetFile)) {
            file.write(json.toString(2));
            log.info("Successfully wrote JSON file: {}", targetFile.getAbsolutePath());
        } catch (IOException e) {
            log.error("Failed to write JSON file: {}", targetFile.getAbsolutePath(), e);
        }
    }

    private String[] parseLocation(String location) {
        final String[] parts;
        if (location.startsWith("Hochhaus")) {
            parts = location.split("Raum");
        } else {
            parts = location.split(", Raum");
        }

        final String building = parts[0].trim().replace("Bau ", "");
        final String room = parts.length > 1 ? parts[1].trim() : "";

        return new String[]{building, room};
    }

    private void randomDelay() throws InterruptedException {
        int delay = randomBetween(Math.max(0, avgDelayBetweenRequests - 2), avgDelayBetweenRequests + 2);
        log.info("Sleeping for {} seconds to respect server load.", delay);
        TimeUnit.SECONDS.sleep(delay);
    }

    private int randomBetween(int min, int max) {
        return (int) (Math.random() * (max - min)) + min;
    }

    // START: QUERY METHODS

    private final List<TimetableEntry> allEntries = new ArrayList<>();

    private void ensureEntriesAreParsed() throws IOException {
        if (!allEntries.isEmpty()) {
            return;
        }

        for (File file : this.targetDir.listFiles()) {
            final String content = new String(Files.readAllBytes(file.toPath()));
            final JSONArray jsonArray = new JSONArray(content);

            for (int i = 0; i < jsonArray.length(); i++) {
                final JSONObject jsonObject = jsonArray.getJSONObject(i);
                final TimetableEntry entry = TimetableEntry.fromJson(jsonObject);

                allEntries.add(entry);
            }
        }
    }

    /**
     * Finds entries based on a provided filter predicate.
     *
     * @param filter A predicate to filter the entries.
     * @return A list of entries that match the filter.
     */
    public List<TimetableEntry> findEntries(Predicate<TimetableEntry> filter) throws IOException {
        this.ensureEntriesAreParsed();
        return this.allEntries.stream().filter(filter).collect(Collectors.toList());
    }

    /**
     * Finds all entries for a certain professor, searching by:
     * - Short name equals
     * - Long name equals
     * - Long name contains
     *
     * @param professorName The professor's name to search for.
     * @return A list of entries matching the professor.
     */
    public List<TimetableEntry> findEntriesForProfessor(String professorName) throws IOException {
        final List<TimetableEntry> shortNameEquals = findEntries(entry -> entry.getProfessorShortName().equalsIgnoreCase(professorName));
        if (!shortNameEquals.isEmpty()) return shortNameEquals;
        final List<TimetableEntry> longNameEquals = findEntries(entry -> entry.getProfessorLongName().equalsIgnoreCase(professorName));
        if (!longNameEquals.isEmpty()) return longNameEquals;
        final List<TimetableEntry> longNameContains = findEntries(entry -> entry.getProfessorLongName() != null && entry.getProfessorLongName().toLowerCase().contains(professorName.toLowerCase()));
        if (!longNameContains.isEmpty()) return longNameContains;

        return new ArrayList<>();
    }

    // END: QUERY METHODS

    @Data
    public static class TimetableEntry {
        private final String start;
        private final String end;
        private final int weekday; // 0 (Monday) to 6 (Sunday)
        private final String lectureLongName;
        private final String lectureShortName;
        private final String building;
        private final String room;
        private final String professorLongName;
        private final String professorShortName;
        private final String semesterShortName;
        private final String semesterLongName;
        private final String semesterNumber;

        public String getRawRoomNumber() {
            if (this.getRoom() != null && !this.getRoom().isEmpty()) {
                return this.getRoom().replaceAll("[A-Za-z-]+", "");
            }
            return "000";
        }

        public JSONObject toJson() {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("start", getStart());
            jsonObject.put("end", getEnd());
            jsonObject.put("weekday", getWeekday());
            jsonObject.put("lectureLongName", getLectureLongName());
            jsonObject.put("lectureShortName", getLectureShortName());
            jsonObject.put("building", getBuilding());
            jsonObject.put("room", getRoom());
            jsonObject.put("professorLongName", getProfessorLongName());
            jsonObject.put("professorShortName", getProfessorShortName());
            jsonObject.put("semesterShortName", getSemesterShortName());
            jsonObject.put("semesterLongName", getSemesterLongName());
            jsonObject.put("semesterNumber", getSemesterNumber());
            return jsonObject;
        }

        public static TimetableEntry fromJson(JSONObject jsonObject) {
            return new TimetableEntry(
                    jsonObject.getString("start"),
                    jsonObject.getString("end"),
                    jsonObject.getInt("weekday"),
                    jsonObject.getString("lectureLongName"),
                    jsonObject.getString("lectureShortName"),
                    jsonObject.getString("building"),
                    jsonObject.getString("room"),
                    jsonObject.getString("professorLongName"),
                    jsonObject.getString("professorShortName"),
                    jsonObject.getString("semesterShortName"),
                    jsonObject.getString("semesterLongName"),
                    jsonObject.getString("semesterNumber")
            );
        }

        public static List<TimetableEntry> sortEntriesByTime(List<TimetableEntry> inputEntries) {
            return inputEntries.stream()
                    .sorted(Comparator.comparing(TimetableEntry::getWeekday).thenComparing(TimetableEntry::getStart).thenComparing(TimetableEntry::getBuilding).thenComparing(TimetableEntry::getRoom))
                    .collect(Collectors.toList());
        }

        public static Map<String, List<TimetableEntry>> mergeSimilarEntries(List<TimetableEntry> inputEntries) {
            final Map<String, List<TimetableEntry>> merged = new LinkedHashMap<>();

            for (TimetableEntry entry : inputEntries) {
                final String key = entry.getProfessorShortName() + "-" + entry.getLectureShortName() + "-" + entry.getBuilding() + "-" + entry.getRoom();
                merged.computeIfAbsent(key, e -> new ArrayList<>()).add(entry);
            }

            return merged;
        }

        public boolean isTimeSlotOccupied(String time) {
            LocalTime slotTime = LocalTime.parse(time.trim());
            LocalTime startTime = LocalTime.parse(this.getStart().trim());
            LocalTime endTime = LocalTime.parse(this.getEnd().trim());

            return !slotTime.isBefore(startTime) && !slotTime.isAfter(endTime);
        }
    }

    @Data
    public static class SemesterProgram {
        private final String studyProgram;
        private final String semesterShortName; // e.g., "1BCB"
        private final String semesterNumber;    // e.g., "1"
        private final String url;

        public String getSemesterLongName() {
            return studyProgram; // Assuming studyProgram is the long name
        }
    }
}
