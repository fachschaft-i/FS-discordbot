package fsi.service.timetable;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class ProfessorScraper {

    private static final String BASE_URL = "https://www.hs-mannheim.de/professorenliste/professoren.php";
    private final OkHttpClient client = new OkHttpClient();

    private final File targetFile;

    public ProfessorScraper(File targetFile) {
        this.targetFile = targetFile;
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }
    }

    public static void main(String[] args) {
        ProfessorScraper scraper = new ProfessorScraper(new File("target/prof/professors.json"));
        scraper.run();
    }

    public void ensureCache() {
        if (!targetFile.exists()) {
            this.run();
        }
    }

    public void clearCache() {
        if (targetFile.exists()) {
            this.targetFile.delete();
            this.allEntries.clear();
        }
    }

    public void run() {
        try {
            log.info("Starting professor scraping process.");
            String content = fetchContent(BASE_URL);
            List<Professor> professors = parseProfessors(content);
            writeJsonToFile(professors);
            log.info("Professor scraping process completed successfully.");
        } catch (Exception e) {
            log.error("An error occurred during the scraping process: {}", e.getMessage(), e);
        }
    }

    private String fetchContent(String url) throws IOException {
        log.debug("Fetching content from URL: {}", url);
        Request request = new Request.Builder().url(url).build();
        try (Response response = client.newCall(request).execute()) {
            String content = response.body().string();
            log.debug("Content fetched successfully from URL: {}", url);
            return content;
        }
    }

    private List<Professor> parseProfessors(String content) {
        log.info("Parsing professor information.");
        List<Professor> professors = new ArrayList<>();
        Document doc = Jsoup.parse(content);

        // Select all table rows containing professor information
        Elements rows = doc.select("table.stripedcollines > tbody > tr");

        for (Element row : rows) {
            // Skip rows that are headers or navigation links
            if (row.select("th").size() > 0 || row.select("td[colspan]").size() > 0) {
                continue;
            }

            Elements cells = row.select("td");
            if (cells.size() != 6) {
                continue;
            }

            // Check if the first cell contains "Prof.", which indicates a header row
            String firstCellText = cells.get(0).text().trim();
            if (firstCellText.contains("Prof.")) {
                continue;
            }

            final String nameCell = cells.get(0).html();
            final String facultyCell = cells.get(1).html();
            final String functionCell = cells.get(2).text();
            final String roomCell = cells.get(3).text();
            final String contactCell = cells.get(4).text();
            final String consultationCell = cells.get(5).text();

            final Professor professor = new Professor();

            parseNameAndEmail(nameCell, professor);
            professor.setFaculty(parseFaculty(facultyCell));
            professor.setSpecialFunction(functionCell.trim());
            professor.setRoom(roomCell.trim());
            professor.setContact(contactCell.trim());
            if (professor.getContact().startsWith("292-")) {
                professor.setContact("0621-" + professor.getContact());
            }
            professor.setConsultationHours(consultationCell.trim());
            professor.setPersonalWebsite(parsePersonalWebsite(nameCell));

            professors.add(professor);
            log.debug("Parsed professor: {}", professor);
        }

        log.info("Total professors parsed: {}", professors.size());
        return professors;
    }

    private void parseNameAndEmail(String nameCell, Professor professor) {
        Document doc = Jsoup.parse(nameCell);

        // Remove all <a> tags with name attributes to isolate the name text
        doc.select("a[name]").remove();

        // Get the remaining text, which should contain the full name and possibly other info
        String fullText = doc.text().trim();

        // Extract the full name (before "E-Mail:")
        int emailIndex = fullText.indexOf("E-Mail:");
        String namePart;
        if (emailIndex >= 0) {
            namePart = fullText.substring(0, emailIndex).trim();
        } else {
            namePart = fullText.trim();
        }

        // Extract short name (Kürzel) from parentheses
        Matcher matcher = Pattern.compile("\\(([^)]+)\\)").matcher(namePart);
        String shortName = null;
        if (matcher.find()) {
            shortName = matcher.group(1);
            professor.setShortName(shortName);
            // Remove the short name from the namePart
            namePart = namePart.replace("(" + shortName + ")", "").trim();
        }

        // Set the full name
        professor.setFullName(namePart);

        // Extract email
        Element emailElement = doc.selectFirst("a[href^=javascript:fhma]");
        if (emailElement != null) {
            String email = emailElement.text().trim();
            professor.setEmail(email + "@hs-mannheim.de");
        } else {
            professor.setEmail(null);
        }
    }

    private List<String> parseFaculty(String facultyCell) {
        List<String> faculties = new ArrayList<>();
        Document doc = Jsoup.parse(facultyCell);
        Elements facultyLinks = doc.select("a");
        for (Element link : facultyLinks) {
            String text = link.text().trim();
            if (!text.isEmpty()) {
                // Remove non-breaking spaces
                text = text.replace("\u00a0", "");
                faculties.add(text);
            }
        }
        return faculties;
    }

    private String parsePersonalWebsite(String nameCell) {
        Document doc = Jsoup.parse(nameCell);
        Element websiteElement = doc.selectFirst("a[href^=http]");
        if (websiteElement != null) {
            return websiteElement.attr("href");
        }
        return null;
    }

    private void writeJsonToFile(List<Professor> professors) {
        log.info("Writing professor information to JSON file: {}", targetFile.getAbsolutePath());
        JSONArray jsonArray = new JSONArray();
        for (Professor professor : professors) {
            jsonArray.put(professor.toJson());
        }

        try (FileWriter file = new FileWriter(targetFile)) {
            file.write(jsonArray.toString(2));
            log.info("Successfully wrote JSON file: {}", targetFile.getAbsolutePath());
        } catch (IOException e) {
            log.error("Failed to write JSON file: {}", targetFile.getAbsolutePath(), e);
        }
    }

    // START: QUERY METHODS

    private final List<Professor> allEntries = new ArrayList<>();

    private void ensureEntriesAreParsed() throws IOException {
        if (!allEntries.isEmpty()) {
            return;
        }

        final String content = new String(Files.readAllBytes(targetFile.toPath()));
        final JSONArray jsonArray = new JSONArray(content);

        for (int i = 0; i < jsonArray.length(); i++) {
            final JSONObject jsonObject = jsonArray.getJSONObject(i);
            final Professor entry = Professor.fromJson(jsonObject);

            allEntries.add(entry);
        }
    }

    public List<Professor> findEntries(Predicate<Professor> filter) throws IOException {
        this.ensureEntriesAreParsed();
        return this.allEntries.stream().filter(filter).collect(Collectors.toList());
    }

    /**
     * Finds the entry for a certain professor, searching by:
     * - Short name equals
     * - Long name equals
     * - Long name contains
     *
     * @param professorName The professor's name to search for.
     * @return A matching professor or null.
     */
    public Professor findEntryForProfessor(String professorName) throws IOException {
        final List<Professor> profs = findEntriesForProfessor(professorName);
        if (profs.isEmpty()) {
            return null;
        } else {
            return profs.get(0);
        }
    }

    public List<Professor> findEntriesForProfessor(String professorName) throws IOException {
        final List<Professor> shortNameEquals = findEntries(entry -> entry.getShortName().equalsIgnoreCase(professorName));
        if (!shortNameEquals.isEmpty()) return shortNameEquals;
        final List<Professor> longNameEquals = findEntries(entry -> entry.getFullName().equalsIgnoreCase(professorName));
        if (!longNameEquals.isEmpty()) return longNameEquals;
        final List<Professor> longNameContains = findEntries(entry -> entry.getFullName() != null && entry.getFullName().toLowerCase().contains(professorName.toLowerCase()));
        if (!longNameContains.isEmpty()) return longNameContains;

        return new ArrayList<>();
    }

    // END: QUERY METHODS

    @Data
    public static class Professor {
        private String fullName;
        private String shortName; // Kürzel
        private String email;
        private List<String> faculty;
        private String specialFunction;
        private String room;
        private String contact; // Telephone / Fax
        private String consultationHours;
        private String personalWebsite;

        public JSONObject toJson() {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("fullName", hasText(fullName) ? fullName : JSONObject.NULL);
            jsonObject.put("shortName", hasText(shortName) ? shortName : JSONObject.NULL);
            jsonObject.put("email", hasText(email) ? email : JSONObject.NULL);
            jsonObject.put("faculty", faculty != null && !faculty.isEmpty() ? faculty : new JSONArray());
            jsonObject.put("specialFunction", hasText(specialFunction) ? specialFunction : JSONObject.NULL);
            jsonObject.put("room", hasText(room) ? room : JSONObject.NULL);
            jsonObject.put("contact", hasText(contact) ? contact : JSONObject.NULL);
            jsonObject.put("consultationHours", hasText(consultationHours) ? consultationHours : JSONObject.NULL);
            jsonObject.put("personalWebsite", hasText(personalWebsite) ? personalWebsite : JSONObject.NULL);
            return jsonObject;
        }

        public static Professor fromJson(JSONObject jsonObject) {
            Professor professor = new Professor();
            professor.setFullName(jsonObject.optString("fullName", null));
            professor.setShortName(jsonObject.optString("shortName", null));
            professor.setEmail(jsonObject.optString("email", null));

            JSONArray facultyArray = jsonObject.optJSONArray("faculty");
            if (facultyArray != null) {
                List<String> facultyList = new ArrayList<>();
                for (int i = 0; i < facultyArray.length(); i++) {
                    facultyList.add(facultyArray.getString(i));
                }
                professor.setFaculty(facultyList);
            } else {
                professor.setFaculty(new ArrayList<>());
            }

            professor.setSpecialFunction(jsonObject.optString("specialFunction", null));
            professor.setRoom(jsonObject.optString("room", null));
            professor.setContact(jsonObject.optString("contact", null));
            professor.setConsultationHours(jsonObject.optString("consultationHours", null));
            professor.setPersonalWebsite(jsonObject.optString("personalWebsite", null));

            return professor;
        }

        private static boolean hasText(String text) {
            return text != null && !text.trim().isEmpty();
        }
    }
}
