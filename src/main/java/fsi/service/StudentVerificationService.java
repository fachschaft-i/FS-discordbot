package fsi.service;

import fsi.config.FsBotConfig;
import fsi.startup.ApplicationStartupLogger;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.security.SecureRandom;
import java.time.Instant;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

@Log4j2
public class StudentVerificationService {

    private final HashMap<String, String> activeVerificationCodes = new HashMap<>();
    private final HashMap<String, Long> userCooldownMap = new HashMap<>();

    public StudentVerificationService() {
        ApplicationStartupLogger.fullStep(log, "Creating " + this.getClass().getSimpleName());
        ApplicationStartupLogger.subStep(log, "Using configuration: " +
                FsBotConfig.Attribute.VERIFY_COOLDOWN_DURATION_MS + "=" + FsBotConfig.getLong(FsBotConfig.Attribute.VERIFY_COOLDOWN_DURATION_MS));
    }

    public String startVerification(String userId) {
        final long currentTimeMillis = Instant.now().toEpochMilli();
        final long cooldownDurationMillis = FsBotConfig.getLong(FsBotConfig.Attribute.VERIFY_COOLDOWN_DURATION_MS);

        if (!isCooldownExpired(userId, currentTimeMillis, cooldownDurationMillis)) {
            long remainingCooldownMillis = cooldownDurationMillis - (currentTimeMillis - userCooldownMap.get(userId));
            throw new TimeoutStillActiveException(remainingCooldownMillis);
        }

        final String verificationCode = generateRandomString(10);
        activeVerificationCodes.put(userId, verificationCode);
        userCooldownMap.put(userId, currentTimeMillis);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                activeVerificationCodes.remove(userId);
            }
        }, 5 * 60 * 1000);

        return verificationCode;
    }

    public boolean isVerificationActive(String userId) {
        return activeVerificationCodes.containsKey(userId);
    }

    public boolean completeVerification(String userId, String code) {
        final String storedCode = activeVerificationCodes.get(userId);
        if (storedCode != null && storedCode.equals(code)) {
            activeVerificationCodes.remove(userId);
            return true;
        }
        return false;
    }

    public void cancelVerification(String userId) {
        activeVerificationCodes.remove(userId);
    }

    private boolean isCooldownExpired(String userId, long currentTimeMillis, long cooldownDurationMillis) {
        return !userCooldownMap.containsKey(userId) || (currentTimeMillis - userCooldownMap.get(userId)) >= cooldownDurationMillis;
    }

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();
    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
    private static final String NUMBER = "0123456789";
    private static final String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;

    private String generateRandomString(int length) {
        if (length < 1) {
            throw new IllegalArgumentException("Length must be greater than 0");
        }

        final StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int rndIndex = SECURE_RANDOM.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndIndex);
            sb.append(rndChar);
        }
        return sb.toString();
    }

    public class TimeoutStillActiveException extends IllegalStateException {
        @Getter
        private final long remainingCooldownMillis;

        public TimeoutStillActiveException(long remainingCooldownMillis) {
            super(String.format("Cooldown still active. Please wait %d seconds before trying again.", remainingCooldownMillis / 1000));
            this.remainingCooldownMillis = remainingCooldownMillis;
        }
    }
}
