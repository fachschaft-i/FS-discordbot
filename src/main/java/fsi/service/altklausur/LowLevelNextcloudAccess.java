package fsi.service.altklausur;

import fsi.config.FsBotConfig;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import okio.BufferedSink;
import okio.Okio;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.ConnectException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Log4j2
public class LowLevelNextcloudAccess {

    private final OkHttpClient client = new OkHttpClient();

    public static List<String> readLines(File file) {
        try {
            return Files.readAllLines(file.toPath());
        } catch (IOException e) {
            throw new RuntimeException("Failed to read lines from file: " + file.getAbsolutePath(), e);
        }
    }

    public void downloadNextcloudItem(String path, File downloadDir) throws IOException {
        if (path == null || path.isEmpty()) {
            throw new IllegalArgumentException("Path must not be null or empty");
        }
        if (downloadDir == null) {
            throw new IllegalArgumentException("Download directory must not be null");
        }
        if (!downloadDir.exists() && !downloadDir.mkdirs()) {
            throw new IOException("Failed to create download directory: " + downloadDir.getAbsolutePath());
        } else if (!downloadDir.isDirectory()) {
            throw new IllegalArgumentException("Download directory is not a directory: " + downloadDir.getAbsolutePath());
        }

        final List<NextcloudFileItem> items = listDirectory(path);

        for (NextcloudFileItem item : items) {
            if (item.isDirectory()) {
                final File nextDownloadDir = new File(downloadDir, item.getName());

                if (!nextDownloadDir.exists() && !nextDownloadDir.mkdirs()) {
                    throw new IOException("Failed to create directory: " + nextDownloadDir.getAbsolutePath());
                }

                log.info("Nextcloud download is a directory, downloading sub-items into: {}", nextDownloadDir.getAbsolutePath());
                downloadNextcloudItem(item.getPathWithoutBase(), nextDownloadDir);
            } else {
                downloadSingleFile(item.getPathWithoutBase(), downloadDir, 0);
            }
        }
    }

    private void downloadSingleFile(String path, File downloadDir, int attempt) throws IOException {
        String fileName = new File(path).getName();
        File localFile = new File(downloadDir, fileName);

        String fileUrl = FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_BASE_URL)
                + "/remote.php/dav/files/"
                + FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_USER) + "/" + path;

        try {
            Request request = authenticationHeader(new Request.Builder().url(fileUrl)).build();

            log.info("Downloading nextcloud file {} --> {}", fileUrl, localFile.getAbsolutePath());

            try (Response response = client.newCall(request).execute()) {
                assertResponseSuccessful(response);

                try (BufferedSink sink = Okio.buffer(Okio.sink(localFile))) {
                    sink.writeAll(response.body().source());
                }
            }
        } catch (Exception e) {
            log.error("Failed to download nextcloud file: {}", fileUrl, e);
            attempt++;
            if (attempt > 3) {
                throw new IOException("Failed to download file after 3 attempts: " + fileUrl, e);
            } else {
                log.info("Retrying download of file: {}", fileUrl);
                downloadSingleFile(path, downloadDir, attempt);
            }
        }
    }

    public List<NextcloudFileItem> listDirectory(String directoryPath) throws IOException {
        final String effectiveDirectoryPath;
        if (directoryPath == null) {
            throw new IllegalArgumentException("Directory path must not be null");
        } else if (directoryPath.isEmpty()) {
            throw new IllegalArgumentException("Directory path must not be empty");
        } else if (directoryPath.charAt(0) == '/') {
            effectiveDirectoryPath = directoryPath.substring(1);
        } else {
            effectiveDirectoryPath = directoryPath;
        }

        final String basePath = "/remote.php/dav/files/" + FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_USER) + "/";
        final Request request = authenticationHeader(new Request.Builder()
                .url(FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_BASE_URL) + basePath + effectiveDirectoryPath)
                .method("PROPFIND", RequestBody.create(
                        "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n" +
                                "<d:propfind xmlns:d=\"DAV:\">\n" +
                                "<d:prop>\n" +
                                "<d:tag/>\n" +
                                "<d:displayname/>\n" +
                                "<d:resourcetype/>\n" +
                                "</d:prop>\n" +
                                "</d:propfind>", MediaType.parse("application/xml"))))
                .build();

        final List<NextcloudFileItem> contents = new ArrayList<>();

        try (Response response = client.newCall(request).execute()) {
            this.assertResponseSuccessful(response);

            final Document doc = responseToDocument(response);
            final NodeList responseNodes = doc.getElementsByTagName("d:response");
            for (int i = 0; i < responseNodes.getLength(); i++) {
                final Element element = (Element) responseNodes.item(i);
                final String name = element.getElementsByTagName("d:displayname").item(0).getTextContent();
                final String path = urlDecode(element.getElementsByTagName("d:href").item(0).getTextContent());
                final String pathWithoutBase = path.substring(basePath.length());
                final boolean isDirectory = element.getElementsByTagName("d:collection").getLength() > 0;
                final boolean isFile = !isDirectory;

                {
                    // nextcloud returns the parent directory as a child of itself, we skip this, we are not interested in the parent directory
                    final String pathWithoutBaseNormalized = pathWithoutBase.replaceAll("/$", "");
                    final String effectiveDirectoryPathNormalized = effectiveDirectoryPath.replaceAll("/$", "");
                    final boolean isItemParent = isDirectory && pathWithoutBaseNormalized.equals(effectiveDirectoryPathNormalized);
                    if (isItemParent) {
                        continue;
                    }
                }

                contents.add(new NextcloudFileItem(effectiveDirectoryPath, name, path, pathWithoutBase, isFile, isDirectory));
            }
        } catch (IOException e) {
            throw new IOException("Failed to fetch directory contents for path: " + effectiveDirectoryPath, e);
        } catch (SAXException | ParserConfigurationException e) {
            throw new RuntimeException("Failed to parse XML response for directory contents: " + effectiveDirectoryPath, e);
        }

        log.info("Listed nextcloud file item [{}] with [{}] sub-items", directoryPath, contents.size());

        return contents;
    }

    public void compressAndUploadDirectory(File uploadDirectory, String remoteName) throws IOException {
        if (!remoteName.endsWith(".zip")) {
            throw new IllegalArgumentException("Remote name must end with .zip");
        }

        log.info("Compressing directory [{}] to zip file for uploading to nextcloud as [{}]", uploadDirectory, remoteName);
        final File zipFile = new File(uploadDirectory.getParentFile(), remoteName);
        try (ZipOutputStream zos = new ZipOutputStream(Files.newOutputStream(zipFile.toPath()))) {
            compressFiles(uploadDirectory, uploadDirectory.getName(), zos);
        }

        if (!zipFile.exists()) {
            throw new IOException("Failed to create zip file at " + zipFile.getAbsolutePath());
        }

        log.info("Zip file [{}] created with size [{}] bytes", zipFile.getName(), zipFile.length());
        if (zipFile.length() == 0) {
            throw new IOException("Zip file is empty.");
        }

        uploadShareZipFile(zipFile, remoteName);
    }

    private void uploadShareZipFile(File localFile, String remoteName) throws IOException {
        final String uploadLocation = FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_UPLOAD_SHARE_ZIPS_PATH) + "/" + remoteName;
        uploadFile(localFile, uploadLocation, MediaType.parse("application/zip"));
    }

    public void uploadFile(File localFile, String remotePath, MediaType mediaType) throws IOException {
        byte[] localFileBytes;
        try (InputStream inputStream = Files.newInputStream(localFile.toPath());
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            localFileBytes = outputStream.toByteArray();
        }

        final RequestBody requestBody = RequestBody.create(localFileBytes, mediaType);

        final String remoteBasePath = "/remote.php/dav/files/" + FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_USER) + "/";
        final String uploadUrl = FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_BASE_URL) + remoteBasePath + remotePath;

        log.info("Uploading file [{}] as [{}] to nextcloud as [{}] to URL: {}", localFile.getName(), mediaType, remotePath, uploadUrl);

        final Request request = authenticationHeader(new Request.Builder()
                .url(uploadUrl)
                .put(requestBody))
                .build();

        try (Response response = client.newCall(request).execute()) {
            this.assertResponseSuccessful(response);
            log.info("Successfully uploaded file [{}] as [{}] to nextcloud as [{}]", localFile.getName(), mediaType, remotePath);
        }
    }

    private void compressFiles(File directory, String baseDirPath, ZipOutputStream zos) throws IOException {
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                compressFiles(file, baseDirPath + "/" + file.getName(), zos);
            } else {
                addToZip(baseDirPath, file, zos);
            }
        }
    }

    private void addToZip(String baseDirPath, File file, ZipOutputStream zos) throws IOException {
        String entryName = baseDirPath + "/" + file.getName();
        zos.putNextEntry(new ZipEntry(entryName));
        Files.copy(file.toPath(), zos);
        zos.closeEntry();
    }

    public FileShareLink createShareLinkForUploadedZipCreatePassword(String zipName, String expirationDate) throws IOException, ParserConfigurationException, SAXException {
        return this.createShareLinkForFile(FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_UPLOAD_SHARE_ZIPS_PATH) + "/" + zipName, expirationDate, FileSharePermission.READ);
    }

    public FileShareLink createShareLinkForFile(String remoteSharePath, String expirationDate, FileSharePermission permission) throws IOException, ParserConfigurationException, SAXException {
        final String password = randomAlphabeticString(16);

        final RequestBody requestBody = new FormBody.Builder()
                .add("path", remoteSharePath)
                .add("expireDate", expirationDate)
                .add("shareType", "3")
                .add("password", password)
                .add("permissions", permission.value)
                .build();

        final String uploadUrl = FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_BASE_URL) + "/ocs/v2.php/apps/files_sharing/api/v1/shares";

        log.info("Creating share link at URL: {}", uploadUrl);

        final Request request = authenticationHeader(new Request.Builder()
                .url(uploadUrl)
                .post(requestBody)
                .addHeader("OCS-APIRequest", "true"))
                .build();

        try (Response response = client.newCall(request).execute()) {
            this.assertResponseSuccessful(response);

            final Document doc = responseToDocument(response);

            final NodeList urlNodes = doc.getElementsByTagName("url");
            if (urlNodes.getLength() == 0) {
                throw new IllegalStateException("Failed to find share link in response");
            }
            final String url = urlNodes.item(0).getTextContent();

            return new FileShareLink(url, password);
        }
    }

    public enum FileSharePermission {
        READ("1"),
        EDIT("31"),
        UPLOAD("4");

        public final String value;

        FileSharePermission(String value) {
            this.value = value;
        }
    }

    public void createDirectory(String path) throws IOException {
        final String remoteBasePath = "/remote.php/dav/files/" + FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_USER) + "/";
        final String createUrl = FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_BASE_URL) + remoteBasePath + path;

        log.info("Creating directory at URL: {}", createUrl);

        final Request request = authenticationHeader(new Request.Builder()
                .url(createUrl)
                .method("MKCOL", null))
                .build();

        try (Response response = client.newCall(request).execute()) {
            this.assertResponseSuccessful(response);
            log.info("Successfully created directory at URL: {}", createUrl);
        }
    }

    public static @NotNull String randomAlphabeticString(int length) {
        return new SecureRandom().ints(length, 0, 52)
                .mapToObj(i -> i < 26 ? String.valueOf((char) ('a' + i)) : String.valueOf((char) ('A' + i - 26)))
                .collect(Collectors.joining());
    }

    public static @NotNull String randomAlphaNumericString(int length, long seed) {
        return new Random(seed).ints(length, 0, 62)
                .mapToObj(i -> i < 26 ? String.valueOf((char) ('a' + i)) : i < 52 ? String.valueOf((char) ('A' + i - 26)) : String.valueOf(i - 52))
                .collect(Collectors.joining());
    }

    public static @NotNull String randomAlphaNumericString(int length) {
        return new SecureRandom().ints(length, 0, 62)
                .mapToObj(i -> i < 26 ? String.valueOf((char) ('a' + i)) : i < 52 ? String.valueOf((char) ('A' + i - 26)) : String.valueOf(i - 52))
                .collect(Collectors.joining());
    }

    private String urlDecode(String path) {
        try {
            return URLDecoder.decode(path, "UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void assertConnectionValid() throws ConnectException {
        Request request = authenticationHeader(new Request.Builder()
                // .url(FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_BASE_URL) + "/ocs/v1.php/cloud/capabilities")
                .url(FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_BASE_URL))
                .get())
                .build();

        try (Response response = client.newCall(request).execute()) {
            this.assertResponseSuccessful(response);
        } catch (IOException e) {
            throw new ConnectException("Failed to connect to nextcloud: " + e.getMessage());
        }
    }

    @Data
    public static class NextcloudFileItem {
        private final String parent;
        private final String name;
        private final String path;
        private final String pathWithoutBase;
        private final boolean isFile;
        private final boolean isDirectory;
    }

    @Data
    public static class FileShareLink {
        private final String url;
        private final String password;
    }

    private Request.Builder authenticationHeader(Request.Builder requestBuilder) {
        return requestBuilder.addHeader("Authorization", Credentials.basic(
                FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_USER),
                FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_PASSWORD)));
    }

    private Document responseToDocument(Response response) throws IOException, SAXException, ParserConfigurationException {
        final String responseBody = response.body().string();
        return DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .parse(new InputSource(new StringReader(responseBody)));
    }

    private void assertResponseSuccessful(Response response) {
        if (!response.isSuccessful()) {
            String body;
            try {
                body = response.body().string();
            } catch (IOException e) {
                body = "Failed to read response body: " + e.getMessage();
            }
            throw new IllegalStateException("Request was not successful: " + response + "\n" + body);
        }
    }
}
