package fsi.service.altklausur;

import fsi.config.FsBotConfig;
import fsi.startup.Bot;
import lombok.Data;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.entities.User;
import okhttp3.MediaType;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class AltklausurAccess {

    @Getter
    private final LowLevelNextcloudAccess lowLevelNextcloudAccess;

    @Getter
    private List<AltklausurAccess.ModuleEntry> cachedModules;
    @Getter
    private Map<AltklausurAccess.ModuleEntry, List<AltklausurAccess.ModuleProfEntry>> cachedModuleProfs;

    private long lastCacheTime = 0;

    public AltklausurAccess() {
        this.lowLevelNextcloudAccess = new LowLevelNextcloudAccess();
    }

    public void appendUserQueryToLogFile(User user, List<String> idList, Set<ModuleProfEntry> allProfModules) throws IOException {
        final String query = String.format("%s aka %s aka %s requested download of modules: %s --> %s",
                user.getAsMention(),
                user.getEffectiveName(),
                user.getName(),
                idList,
                allProfModules.stream().map(moduleProfEntry -> moduleProfEntry.getUniqueIdentifier() + ":" + moduleProfEntry.getModule().getShortModuleName() + ":" + moduleProfEntry.getProf()).collect(Collectors.joining(", ")));

        final String remoteQueryLogFileName = new File(FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_QUERY_LOG_PATH)).getName();
        final File logFile = new File(Bot.obtainTempFile("altklausur_query_log"), remoteQueryLogFileName);
        this.lowLevelNextcloudAccess.downloadNextcloudItem(FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_QUERY_LOG_PATH), logFile.getParentFile());

        // append query to log file
        final String queryLine = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " - " + query + "\n";
        try (FileWriter fileWriter = new FileWriter(logFile, true)) {
            fileWriter.write(queryLine);
        }

        this.lowLevelNextcloudAccess.uploadFile(logFile, FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_QUERY_LOG_PATH), MediaType.get("text/plain"));
    }

    public List<AltklausurUserQueryResponse> parseRequestedModules() {
        final String remoteQueryLogFileName = new File(FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_QUERY_LOG_PATH)).getName();
        final File logFile = new File(Bot.obtainTempFile("altklausur_query_log"), remoteQueryLogFileName);
        try {
            this.lowLevelNextcloudAccess.downloadNextcloudItem(FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_QUERY_LOG_PATH), logFile.getParentFile());
        } catch (IOException e) {
            log.error("Failed to download query log file for parsing", e);
            return Collections.emptyList();
        }

        final List<AltklausurUserQueryResponse> responses = new ArrayList<>();
        final List<String> lines = LowLevelNextcloudAccess.readLines(logFile);
        for (String line : lines) {
            final String[] parts = line.split(" - ", 2);
            if (parts.length != 2) {
                log.warn("Skipping line with unexpected format when splitting by ' - ' separator: [{}]", line);
                continue;
            }

            final String[] dateParts = parts[0].split("-", 3);
            if (dateParts.length != 3) {
                log.warn("Skipping line with unexpected format when splitting by '-' separator: [{}]", parts[0]);
                continue;
            }
            final int year = Integer.parseInt(dateParts[0]);
            final int month = Integer.parseInt(dateParts[1]);
            final int day = Integer.parseInt(dateParts[2]);

            final String[] queryParts = parts[1].split("requested download of modules: ", 2);
            if (queryParts.length != 2) {
                log.warn("Skipping line with unexpected format when splitting by 'requested download of modules: ' separator: [{}]", parts[1]);
                continue;
            }

            final String[] userParts = queryParts[0].split(" aka ", 3);
            if (userParts.length != 3) {
                log.warn("Skipping line with unexpected format when splitting by ' aka ' separator: [{}]", queryParts[0]);
                continue;
            }

            final String userId = userParts[0];
            final String userEffectiveName = userParts[1];
            final String userName = userParts[2];

            // [bAESMB, ifMk03] --> oGHY4w:WEB:Specht, eYmNAq:WEB:Groeschel, ifMk03:SE1:Klaus, maSMD5:WEB:Dopatka, l2cxRn:WEB:Smits IM6pkj:SE1:Knauber, yKWuF2:MA1:Todorov, muI9h2:PR1:Klaus, EXRrTC:GAT:Hauske]
            final String modulePart = queryParts[1].replaceAll("\\[[^\\[]+\\] --> (.+)", "$1");
            if (modulePart.isEmpty()) {
                log.warn("Skipping line with unexpected format when extracting module part: [{}]", queryParts[1]);
                continue;
            }
            final List<String> requestedModules = Arrays.stream(modulePart.split(", "))
                    .map(module -> module.replaceAll("([^:]+):([^:]+):([^:]+)", "$2"))
                    .distinct()
                    .collect(Collectors.toList());

            responses.add(new AltklausurUserQueryResponse(year, month, day, userId, userEffectiveName, userName, requestedModules));
        }

        return responses;
    }

    @Data
    public static class AltklausurUserQueryResponse {
        private final int year, month, day;
        private final String userId;
        private final String userEffectiveName;
        private final String userName;
        private final List<String> requestedModules;

        public boolean isWithinDateRange(LocalDate startDate, LocalDate endDate) {
            final LocalDate queryDate = LocalDate.of(year, month, day);
            return !queryDate.isBefore(startDate) && !queryDate.isAfter(endDate);
        }
    }

    public LowLevelNextcloudAccess.FileShareLink createUserUploadDirectoryForAltklausuren() throws IOException, ParserConfigurationException, SAXException {
        final String expirationDate = LocalDate.now().plusDays(7).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        final String currentDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        final String shareDir = FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_USER_UPLOAD_SHARE_ALTKLAUSUREN_PATH) + "/" + currentDate + "_" + LowLevelNextcloudAccess.randomAlphaNumericString(8);
        this.lowLevelNextcloudAccess.createDirectory(shareDir);
        return this.lowLevelNextcloudAccess.createShareLinkForFile(shareDir, expirationDate, LowLevelNextcloudAccess.FileSharePermission.UPLOAD);
    }

    protected List<ModuleEntry> listModules() throws IOException {
        final List<LowLevelNextcloudAccess.NextcloudFileItem> moduleDirItems = this.lowLevelNextcloudAccess.listDirectory(FsBotConfig.get(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_KLAUSUREN_PATH));

        // ABPM - Advanced Business Process Modelling --> ABPM / Advanced Business Process Modelling
        // ACD - Anwendungscontainer und Docker --> ACD / Anwendungscontainer und Docker
        // CPR - C_C++ Programmierung --> CPR / C_C++ Programmierung
        // MED1+MED2 - Medizin 1 und 2 --> MED1+MED2 / Medizin 1 und 2
        // NF (BGL) - Betriebswirtschaftliche Grundlagen --> NF (BGL) / Betriebswirtschaftliche Grundlagen

        final List<ModuleEntry> moduleEntries = new ArrayList<>();

        for (LowLevelNextcloudAccess.NextcloudFileItem moduleDirItem : moduleDirItems) {
            if (!moduleDirItem.isDirectory()) {
                continue;
            }

            final String fileName = prepareModuleFileName(moduleDirItem.getName());
            if (fileName.equals("_temp")) {
                continue;
            }

            final String[] parts = fileName.split(" - ", 2);

            if (parts.length != 2) {
                log.warn("Skipping module directory with unexpected name [{}] when splitting by ' - ' separator whilst listing modules", fileName);
                continue;
            }

            final String shortModuleName = parts[0].trim();
            final String longModuleName = parts[1].trim();

            final ModuleEntry moduleEntry = new ModuleEntry(shortModuleName, longModuleName, moduleDirItem);
            moduleEntries.add(moduleEntry);
        }

        return moduleEntries;
    }

    protected List<ModuleProfEntry> listModuleProfs(ModuleEntry module) throws IOException {
        final List<LowLevelNextcloudAccess.NextcloudFileItem> moduleDirItems = this.lowLevelNextcloudAccess.listDirectory(module.getFileItem().getPathWithoutBase());

        final List<ModuleProfEntry> moduleProfEntries = new ArrayList<>();

        for (LowLevelNextcloudAccess.NextcloudFileItem moduleDirItem : moduleDirItems) {
            if (!moduleDirItem.isDirectory()) {
                continue;
            }

            final String prof = moduleDirItem.getName();

            final ModuleProfEntry moduleProfEntry = new ModuleProfEntry(prof, module, moduleDirItem);
            moduleProfEntries.add(moduleProfEntry);
        }

        return moduleProfEntries;
    }

    public Map.Entry<ModuleEntry, List<ModuleProfEntry>> findCachedModuleByName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Module name cannot be null or empty");
        }
        return this.cachedModules.stream()
                .filter(module -> module.getShortModuleName().equalsIgnoreCase(name))
                .findFirst()
                .map(module -> new AbstractMap.SimpleEntry<>(module, this.cachedModuleProfs.getOrDefault(module, Collections.emptyList())))
                .orElse(new AbstractMap.SimpleEntry<>(null, Collections.emptyList()));
    }

    public synchronized void invalidateCache() {
        log.info("Manual cache invalidation for AltklausurAccess");
        this.lastCacheTime = 0;
    }

    public synchronized void ensureCache() throws AltklausurCacheCreationException {
        final long validityTime = FsBotConfig.getLong(FsBotConfig.Attribute.ALTKL_NEXTCLOUD_CACHE_VALIDITY_DURATION);
        if (this.lastCacheTime + validityTime < System.currentTimeMillis()) {
            log.info("Cache for AltklausurAccess is outdated, refreshing");
            createCache();
            this.lastCacheTime = System.currentTimeMillis();
        }
    }

    private void createCache() throws AltklausurCacheCreationException {
        try {
            final List<ModuleEntry> modules = listModules();
            final Map<ModuleEntry, List<ModuleProfEntry>> profs = new HashMap<>();
            // int count = 0, maxCount = 5;
            for (ModuleEntry module : modules) {
                /*if (count++ >= maxCount) {
                    log.info("Reached max count of [{}] modules, skipping the rest", maxCount);
                    break;
                }*/
                profs.put(module, listModuleProfs(module));
            }

            this.cachedModules = modules;
            this.cachedModuleProfs = profs;
            log.info("Cache for AltklausurAccess has been created: [{}] modules and total of [{}] module profs", modules.size(), profs.values().stream().mapToInt(List::size).sum());
        } catch (IOException e) {
            throw new AltklausurCacheCreationException("Failed to create cache for AltklausurAccess", e);
        }
    }

    public static class AltklausurCacheCreationException extends RuntimeException {
        public AltklausurCacheCreationException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private String prepareModuleFileName(String moduleName) {
        return moduleName
                .replace("ue", "ü").replace("ae", "ä").replace("oe", "ö")
                .replace("UE", "Ü").replace("AE", "Ä").replace("OE", "Ö")
                .replaceAll("([a-zA-Z]+)_([a-zA-Z])", "$1-$2")
                .replaceAll("^([a-zA-Z0-9+()]+) *- *", "$1 - ");
    }

    public List<ModuleProfEntry> findCachedModuleByIdOrName(String id) {
        // attempts:
        // 1. find prof by prof id
        // 2. find all profs by module id
        // 3. find module by module short name
        // 4. find prof by prof short name
        final ModuleProfEntry foundProf = this.cachedModuleProfs.values().stream()
                .flatMap(List::stream)
                .filter(prof -> prof.getUniqueIdentifier().equals(id))
                .findFirst()
                .orElse(null);
        if (foundProf != null) {
            return Collections.singletonList(foundProf);
        }

        final ModuleEntry foundModule = this.cachedModules.stream()
                .filter(module -> module.getUniqueIdentifier().equals(id))
                .findFirst()
                .orElse(null);
        if (foundModule != null) {
            return this.cachedModuleProfs.getOrDefault(foundModule, Collections.emptyList());
        }

        final ModuleProfEntry foundProfByShortName = this.cachedModuleProfs.values().stream()
                .flatMap(List::stream)
                .filter(prof -> prof.getProf().equalsIgnoreCase(id))
                .findFirst()
                .orElse(null);
        if (foundProfByShortName != null) {
            return Collections.singletonList(foundProfByShortName);
        }

        final ModuleEntry foundModuleByShortName = this.cachedModules.stream()
                .filter(module -> module.getShortModuleName().equalsIgnoreCase(id))
                .findFirst()
                .orElse(null);
        if (foundModuleByShortName != null) {
            return this.cachedModuleProfs.getOrDefault(foundModuleByShortName, Collections.emptyList());
        }

        return Collections.emptyList();
    }

    @Data
    public static class ModuleEntry {
        private final String shortModuleName;
        private final String longModuleName;
        private final LowLevelNextcloudAccess.NextcloudFileItem fileItem;

        public String getUniqueIdentifier() {
            return LowLevelNextcloudAccess.randomAlphaNumericString(6, this.shortModuleName.hashCode());
        }
    }

    @Data
    public static class ModuleProfEntry {
        private final String prof;
        private final ModuleEntry module;
        private final LowLevelNextcloudAccess.NextcloudFileItem fileItem;

        public String getProfDisplayName() {
            if (this.isUnknown()) {
                return "Unbekannter Professor";
            } else {
                return this.prof;
            }
        }

        public boolean isUnknown() {
            return this.prof == null || this.prof.equalsIgnoreCase("unknown");
        }

        public boolean isKnown() {
            return !isUnknown();
        }

        public String getUniqueIdentifier() {
            return LowLevelNextcloudAccess.randomAlphaNumericString(6, this.prof.hashCode() + this.module.getShortModuleName().hashCode());
        }
    }
}
