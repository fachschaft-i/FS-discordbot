package fsi.service.stundenplan;

import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Log4j2
public class StundenplanService {

    private final static String BASE_URL = "https://services.informatik.hs-mannheim.de/stundenplan/";

    private final static Pattern STUDIENGANG_EXTRACTION_PATTERN = Pattern.compile("^[A-Z_-]+ +([A-Zäüö]+)[ -]+([^(]+)(?: +\\(([^)]+)\\))?$", Pattern.CASE_INSENSITIVE);
    private final static Pattern SEMESTER_EXTRACTION_PATTERN = Pattern.compile("^(\\d*?[A-Z_-]+) - ([^(]+)(?: +\\(([^)]+)\\))?$", Pattern.CASE_INSENSITIVE);
    private final static Pattern FACH_EXTRACTION_PATTERN = Pattern.compile("^(\\d*[A-Z_]+) - ([A-Z0-9_]+) \\(([^)]+)\\)$", Pattern.CASE_INSENSITIVE);

    public List<StundenplanStudiengang> fetchStudiengaenge() throws IOException {
        log.info("Fetching studiengaenge");
        final String url = BASE_URL + "?eigener_plan=1";
        final Document document = Jsoup.connect(url).get();
        final Elements options = document.select("select[name='stdg_p[]'] option");

        final List<StundenplanStudiengang> studiengaenge = new ArrayList<>();

        for (Element option : options) {
            final String text = option.text();
            final Matcher matcher = STUDIENGANG_EXTRACTION_PATTERN.matcher(text);

            if (matcher.find() && matcher.groupCount() >= 2) {
                final String kuerzel = matcher.group(1);
                final String name = matcher.group(2);
                final String art = matcher.groupCount() == 3 ? matcher.group(3) : null;
                studiengaenge.add(new StundenplanStudiengang(kuerzel, name, art));
            } else {
                log.warn("Failed to parse studiengang: {}", text);
            }
        }

        return studiengaenge;
    }

    public List<StundenplanSemester> fetchSemester(List<StundenplanStudiengang> studiengaenge) throws IOException {
        log.info("Fetching semester for studiengaenge: {}", studiengaenge.stream().map(StundenplanStudiengang::getKuerzel).collect(Collectors.joining(",")));
        if (studiengaenge.isEmpty()) {
            return new ArrayList<>();
        }

        final String url = BASE_URL + "?eigener_plan=2&pplan=weiter&" + studiengaenge.stream().map(s -> "stdg_p[]=" + s.getKuerzel()).collect(Collectors.joining("&"));
        final Document document = Jsoup.connect(url).get();
        final Elements options = document.select("select[name='sem_p[]'] option");

        final List<StundenplanSemester> semesterList = new ArrayList<>();

        for (Element option : options) {
            final String text = option.text();
            final Matcher matcher = SEMESTER_EXTRACTION_PATTERN.matcher(text);

            if (matcher.find() && matcher.groupCount() >= 2) {
                final String kuerzel = matcher.group(1);
                final String name = matcher.group(2);
                final String art = matcher.groupCount() == 3 ? matcher.group(3) : null;
                semesterList.add(new StundenplanSemester(kuerzel, name, art));
            } else {
                log.warn("Failed to parse semester: {}", text);
            }
        }

        return semesterList;
    }

    public List<StundenplanFach> fetchFaecher(List<StundenplanSemester> semesterList) throws IOException {
        log.info("Fetching faecher for semesters: {}", semesterList.stream().map(StundenplanSemester::getKuerzel).collect(Collectors.joining(",")));

        if (semesterList.isEmpty()) {
            return new ArrayList<>();
        }

        final String url = BASE_URL + "?eigener_plan=3&pplan=weiter&" + semesterList.stream().map(s -> "sem_p[]=" + s.getKuerzel()).collect(Collectors.joining("&"));
        final Document document = Jsoup.connect(url).get();
        final Elements options = document.select("select[name='fach_p[]'] option");

        final List<StundenplanFach> fachList = new ArrayList<>();

        for (Element option : options) {
            final String text = option.text();
            final Matcher matcher = FACH_EXTRACTION_PATTERN.matcher(text);

            if (matcher.find() && matcher.groupCount() == 3) {
                final String studiengang = matcher.group(1);
                final String kuerzel = matcher.group(2);
                final String name = matcher.group(3);
                fachList.add(new StundenplanFach(studiengang, kuerzel, name));
            } else {
                log.warn("Failed to parse fach: {}", text);
            }
        }

        return fachList;
    }

    public StundenplanPlan.Stundenplan fetchStundenplan(List<StundenplanFach> faecher) throws IOException {
        if (faecher.isEmpty()) {
            return new StundenplanPlan.Stundenplan(new ArrayList<>(), new ArrayList<>());
        }

        log.info("Fetching stundenplan for faecher: {}", faecher.stream().map(StundenplanFach::getKuerzel).collect(Collectors.toList()));
        final String url = BASE_URL + "?msp=1&pplan=weiter" + faecher.stream().map(f -> "&fach_p[]=" + f.getKuerzel() + "@" + f.getStudiengang()).collect(Collectors.joining());
        log.info("Fetching stundenplan from URL: {}", url);

        final Document document = Jsoup.connect(url).get();
        final Element table = document.selectFirst("table");

        if (table == null) {
            log.error("No table found in the document");
            return new StundenplanPlan.Stundenplan(new ArrayList<>(), new ArrayList<>());
        }

        final Elements rows = table.select("tr");
        if (rows.isEmpty()) {
            log.error("No rows found in the table");
            return new StundenplanPlan.Stundenplan(new ArrayList<>(), new ArrayList<>());
        }

        // Parse the header row to get the weekdays
        final Elements headerCells = rows.get(0).select("th");
        final List<String> wochentage = headerCells.stream().skip(1).map(Element::text).collect(Collectors.toList());

        // Initialize list to hold days
        final List<StundenplanPlan.StundenplanTag> tage = new ArrayList<>();
        for (String wochentag : wochentage) {
            tage.add(new StundenplanPlan.StundenplanTag(wochentag, new ArrayList<>()));
        }
        final List<StundenplanPlan.StundenplanZeit> zeiten = new ArrayList<>();

        // Parse each row for hours and entries
        for (int i = 1; i < rows.size(); i++) {
            final Element row = rows.get(i);
            final Elements cells = row.select("td");

            if (cells.isEmpty()) {
                continue;
            }

            // The first cell contains the time slot
            final String zeit = cells.get(0).text();
            final Matcher zeitMatcher = Pattern.compile("^0?(\\d+) (\\d{2}:\\d{2}) - (\\d{2}:\\d{2})$").matcher(zeit);
            final String stunde, startZeit, endZeit;
            if (zeitMatcher.find() && zeitMatcher.groupCount() == 3) {
                stunde = zeitMatcher.group(1);
                startZeit = zeitMatcher.group(2);
                endZeit = zeitMatcher.group(3);
            } else {
                log.warn("Failed to parse time slot: {}", zeit);
                stunde = null;
                startZeit = null;
                endZeit = null;
            }
            final StundenplanPlan.StundenplanZeit zeitInst = new StundenplanPlan.StundenplanZeit(stunde, startZeit, endZeit);
            zeiten.add(zeitInst);

            for (int j = 1; j < cells.size(); j++) {
                final Element cell = cells.get(j);
                final Elements spans = cell.select("span");

                if (spans.isEmpty()) {
                    continue;
                }

                if (spans.size() % 3 != 0) {
                    log.warn("Unexpected number of spans [{}] in cell {}", spans.size(), cell.text());
                    continue;
                }

                final List<StundenplanPlan.StundenplanEintrag> eintraege = new ArrayList<>();
                for (int k = 0; k < spans.size(); k += 3) {
                    final String fachLang = spans.get(k + 0).attr("title").trim();
                    final String fachKuerzel = spans.get(k + 0).text().trim();
                    final String raumLang = spans.get(k + 1).attr("title").trim();
                    final String raumKuerzel = spans.get(k + 1).text().trim();
                    final String profLang = spans.get(k + 2).attr("title").trim();
                    final String profKuerzel = spans.get(k + 2).text().replace("(", "").replace(")", "").trim();

                    StundenplanPlan.StundenplanEintrag eintrag = new StundenplanPlan.StundenplanEintrag(fachLang, fachKuerzel, raumLang, raumKuerzel, profLang, profKuerzel);
                    eintraege.add(eintrag);
                }
                tage.get(j - 1).getStunden().add(new StundenplanPlan.StundenplanStunde(zeitInst, eintraege));
            }
        }

        return new StundenplanPlan.Stundenplan(tage, zeiten);
    }

    public static void main(String[] args) {
        final StundenplanService service = new StundenplanService();

        try {
            /*final StundenplanPlan.Stundenplan stundenplan = service.fetchStundenplan(Arrays.asList(
                    new StundenplanFach("6IB", "GNN", "Grundlagen Neuronale Netze"),
                    new StundenplanFach("7IB", "KRV", "Kryptographische Verfahren"),
                    new StundenplanFach("6IB", "KDT", "Kodierungstheorie")
            ));*/
            final StundenplanPlan.Stundenplan stundenplan = service.fetchStundenplan(Arrays.asList(
                    new StundenplanFach("2UIB", "MA2_TUT", "MA2_TUT@2UIB"),
                    new StundenplanFach("2UIB", "PR2", "PR2@2UIB"),
                    new StundenplanFach("2UIB", "PR2_L", "PR2_L@2UIB"),
                    new StundenplanFach("2UIB", "SE1", "SE1@2UIB"),
                    new StundenplanFach("2UIB", "WI2", "WI2@2UIB"),
                    new StundenplanFach("2IM_SE", "DMS", "DMS@2IM_SE"),
                    new StundenplanFach("2IM_SE", "BDEA", "BDEA@2IM_SE"),
                    new StundenplanFach("2IM_SE", "AMR", "AMR@2IM_SE"),
                    new StundenplanFach("2IM_SE", "CPD", "CPD@2IM_SE")
            ));

            System.out.println(stundenplan.toMessage());

            final BufferedImage image = stundenplan.render("test");
            ImageIO.write(image, "png", new BufferedOutputStream(Files.newOutputStream(Paths.get("timetable.png"))));

            if (true) return;

            final List<StundenplanStudiengang> studiengaenge = service.fetchStudiengaenge();
            final List<StundenplanStudiengang> filteredStudiengaenge = studiengaenge.stream().filter(s -> s.getKuerzel().equals("IB") || s.getKuerzel().equals("IMB")).collect(Collectors.toList());
            final List<StundenplanSemester> semester = service.fetchSemester(filteredStudiengaenge);
            final List<StundenplanSemester> filteredSemester = semester.stream().filter(s -> s.getKuerzel().equals("6IB") || s.getKuerzel().equals("2IMB")).collect(Collectors.toList());
            final List<StundenplanFach> faecher = service.fetchFaecher(filteredSemester);
            for (StundenplanFach fach : faecher) {
                log.info(fach);
            }
        } catch (IOException e) {
            log.error("Failed to fetch studiengaenge", e);
        }
    }
}
