package fsi.service.stundenplan;

import lombok.Data;

@Data
public class StundenplanStudiengang {
    private final String kuerzel;
    private final String name;
    private final String art;
}
