package fsi.service.stundenplan;

import lombok.Data;

@Data
public class StundenplanFach {
    private final String studiengang;
    private final String kuerzel;
    private final String name;
}
