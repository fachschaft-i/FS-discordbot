package fsi.service.stundenplan;

import lombok.Getter;
import lombok.Setter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class SimpleTableRenderer {

    @Getter
    public static class CellContent {
        private final String text;
        private final Font font;
        private final String alignment; // "left", "center", "right"

        public CellContent(String text, Font font, String alignment) {
            this.text = text;
            this.font = font;
            this.alignment = alignment;
        }

        public int getFontSize() {
            return font.getSize();
        }

        public CellContent deriveContent(String newText) {
            return new CellContent(newText, font, alignment);
        }

        public CellContent deriveContent(Font newFont) {
            return new CellContent(text, newFont, alignment);
        }
    }

    @Setter
    private int innerPaddingVertical = 5;
    @Setter
    private int innerPaddingHorizontal = 5;
    @Setter
    private int outerPadding = 4;

    public BufferedImage renderTable(List<List<CellContent>> timetable) {
        BufferedImage dummyImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = dummyImage.createGraphics();

        int[] maxColWidths = calculateMaxColWidths(timetable, g2d);
        int[] maxRowHeights = calculateMaxRowHeights(timetable, g2d);

        Dimension canvasSize = calculateCanvasSize(maxColWidths, maxRowHeights);
        BufferedImage image = new BufferedImage(canvasSize.width, canvasSize.height, BufferedImage.TYPE_INT_ARGB);
        g2d = image.createGraphics();

        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, canvasSize.width, canvasSize.height);
        g2d.setColor(Color.BLACK);

        int y = outerPadding;
        for (int row = 0; row < timetable.size(); row++) {
            int x = outerPadding;
            for (int col = 0; col < timetable.get(row).size(); col++) {
                CellContent content = timetable.get(row).get(col);
                g2d.setFont(content.getFont());
                if (col >= maxColWidths.length || row >= maxRowHeights.length) {
                    System.out.println("Error: " + col + " " + row + " " + maxColWidths.length + " " + maxRowHeights.length);
                    continue;
                }
                drawMultilineAlignedString(g2d, content.getText(), x, y, maxColWidths[col], maxRowHeights[row], content.getAlignment());
                g2d.drawRect(x, y, maxColWidths[col], maxRowHeights[row]);
                x += maxColWidths[col];
            }
            y += maxRowHeights[row];
        }

        g2d.dispose();
        return image;
    }

    private Dimension getMultilineTextSize(CellContent content, Graphics2D g2d) {
        g2d.setFont(content.getFont());
        FontMetrics metrics = g2d.getFontMetrics();
        String[] lines = content.getText().split("\n");
        int maxWidth = 0;
        int totalHeight = 0;

        for (String line : lines) {
            int width = metrics.stringWidth(line);
            maxWidth = Math.max(maxWidth, width);
            totalHeight += metrics.getHeight();
        }

        return new Dimension(maxWidth + innerPaddingHorizontal * 2, totalHeight + innerPaddingVertical * 2);
    }

    private int[] calculateMaxColWidths(List<List<CellContent>> timetable, Graphics2D g2d) {
        int numCols = timetable.get(0).size();
        int[] maxColWidths = new int[numCols];

        for (List<CellContent> row : timetable) {
            for (int col = 0; col < numCols; col++) {
                Dimension size = getMultilineTextSize(row.get(col), g2d);
                maxColWidths[col] = Math.max(maxColWidths[col], size.width);
            }
        }

        return maxColWidths;
    }

    private int[] calculateMaxRowHeights(List<List<CellContent>> timetable, Graphics2D g2d) {
        int numRows = timetable.size();
        int[] maxRowHeights = new int[numRows];

        for (int row = 0; row < numRows; row++) {
            for (CellContent content : timetable.get(row)) {
                Dimension size = getMultilineTextSize(content, g2d);
                maxRowHeights[row] = Math.max(maxRowHeights[row], size.height);
            }
        }

        return maxRowHeights;
    }

    private Dimension calculateCanvasSize(int[] maxColWidths, int[] maxRowHeights) {
        int totalWidth = Arrays.stream(maxColWidths).sum() + outerPadding * 2;
        int totalHeight = Arrays.stream(maxRowHeights).sum() + outerPadding * 2;
        return new Dimension(totalWidth, totalHeight);
    }

    private void drawMultilineAlignedString(Graphics2D g2d, String text, int x, int y, int cellWidth, int cellHeight, String alignment) {
        FontMetrics metrics = g2d.getFontMetrics();
        String[] lines = text.split("\n");
        int lineHeight = metrics.getHeight();
        int textY = y + innerPaddingVertical + (cellHeight - innerPaddingVertical * 2 - (lines.length * lineHeight)) / 2 + metrics.getAscent();

        for (String line : lines) {
            int textWidth = metrics.stringWidth(line);
            int textX;

            switch (alignment) {
                case "center":
                    textX = x + innerPaddingHorizontal + (cellWidth - innerPaddingHorizontal * 2 - textWidth) / 2;
                    break;
                case "right":
                    textX = x + cellWidth - innerPaddingHorizontal - textWidth;
                    break;
                default:
                    textX = x + innerPaddingHorizontal;
                    break;
            }

            g2d.drawString(line, textX, textY);
            textY += lineHeight;
        }
    }

    public void writeImage(BufferedImage image, OutputStream out) throws IOException {
        ImageIO.write(image, "png", out);
    }

    public BufferedImage decorateWithTitle(BufferedImage contentImage, CellContent title) {
        BufferedImage dummyImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = dummyImage.createGraphics();

        Dimension titleSize = getMultilineTextSize(title, g2d);
        Dimension contentSize = new Dimension(contentImage.getWidth(), contentImage.getHeight());
        Dimension canvasSize = new Dimension(Math.max(titleSize.width, contentSize.width), titleSize.height + contentSize.height);

        BufferedImage image = new BufferedImage(canvasSize.width, canvasSize.height, BufferedImage.TYPE_INT_ARGB);
        g2d = image.createGraphics();

        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, canvasSize.width, canvasSize.height);
        g2d.setColor(Color.BLACK);

        g2d.setFont(title.getFont());
        drawMultilineAlignedString(g2d, title.getText(), 0, 0, canvasSize.width, titleSize.height, title.getAlignment());
        g2d.drawImage(contentImage, 0, titleSize.height, null);

        g2d.dispose();
        return image;
    }

    public static void main(String[] args) throws IOException {
        SimpleTableRenderer renderer = new SimpleTableRenderer();

        int size = 25;

        List<List<CellContent>> timetable = Arrays.asList(
                Arrays.asList(
                        new CellContent("Time test test test", new Font("Arial", Font.BOLD, size), "center"),
                        new CellContent("Monday", new Font("Arial", Font.ITALIC, size), "center"),
                        new CellContent("Tuesday", new Font("Arial", Font.BOLD, size), "center"),
                        new CellContent("Wednesday", new Font("Arial", Font.BOLD, size), "center")
                ),
                Arrays.asList(
                        new CellContent("01 08:00-10:00", new Font("Arial", Font.PLAIN, size), "left"),
                        new CellContent("Math\nEnglish", new Font("Arial", Font.PLAIN, size), "center"),
                        new CellContent("Physics\nTest", new Font("Arial", Font.PLAIN, size), "left"),
                        new CellContent("Chemistry\nAnd more", new Font("Arial", Font.PLAIN, size), "right")
                ),
                Arrays.asList(
                        new CellContent("02 10:00-12:00", new Font("Arial", Font.PLAIN, size), "right"),
                        new CellContent("Math", new Font("Arial", Font.PLAIN, size), "center"),
                        new CellContent("Physics", new Font("Arial", Font.PLAIN, size), "center"),
                        new CellContent("Chemistry", new Font("Arial", Font.PLAIN, size), "center")
                )
        );

        BufferedImage image = renderer.renderTable(timetable);
        renderer.setInnerPaddingHorizontal(20);
        renderer.setInnerPaddingVertical(4);
        image = renderer.decorateWithTitle(image, new CellContent("Timetable", new Font("Arial", Font.BOLD, size), "left"));
        renderer.writeImage(image, new BufferedOutputStream(Files.newOutputStream(Paths.get("timetable.png"))));
    }
}


