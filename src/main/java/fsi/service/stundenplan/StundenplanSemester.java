package fsi.service.stundenplan;

import lombok.Data;

@Data
public class StundenplanSemester {
    /**
     * e.g. 1IB
     */
    private final String kuerzel;
    /**
     * e.g. Informatik
     */
    private final String name;
    /**
     * e.g. Bachelor
     */
    private final String art;
}
