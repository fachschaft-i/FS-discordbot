package fsi.service.stundenplan;

import lombok.Data;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class StundenplanPlan {
    @Data
    public static class Stundenplan {
        private final List<StundenplanTag> tage;
        private final List<StundenplanZeit> zeiten;

        public String toMessage() {
            StringBuilder message = new StringBuilder();
            for (StundenplanTag tag : tage) {
                message.append(tag.getWochentag()).append(":\n");
                for (StundenplanStunde stunde : tag.getStunden()) {
                    if (!stunde.getEintraege().isEmpty()) {
                        message.append("  ")
                                .append("0").append(stunde.getZeit().getStunde()).append(" (")
                                .append(stunde.getZeit().getStartZeit()).append(" - ")
                                .append(stunde.getZeit().getEndZeit()).append("):\n");
                        final int longestFachKuerzel = stunde.getEintraege().stream().mapToInt(e -> e.getFachKuerzel().length()).max().orElse(0);
                        final int longestRaumKuerzel = stunde.getEintraege().stream().mapToInt(e -> e.getRaumKuerzel().length()).max().orElse(0);
                        for (StundenplanEintrag eintrag : stunde.getEintraege()) {
                            message.append("    - ")
                                    .append(String.format("%-" + longestFachKuerzel + "s %-" + longestRaumKuerzel + "s (%s) ~ %s%n", eintrag.getFachKuerzel(), eintrag.getRaumKuerzel(), eintrag.getProfKuerzel(), eintrag.getFachLang() == null ? "" : eintrag.getFachLang().replaceAll("(.+) \\(.+\\)$", "$1")));
                        }
                    }
                }
            }
            return message.toString();
        }

        public BufferedImage render(String name) {
            final Font boldFont = new Font("Arial", Font.BOLD, 25);
            final Font normalFont = new Font("Arial", Font.PLAIN, 25);

            final List<List<SimpleTableRenderer.CellContent>> contents = new ArrayList<>();

            final List<SimpleTableRenderer.CellContent> header = new ArrayList<>();
            header.add(new SimpleTableRenderer.CellContent("Stunde", boldFont, "center"));
            for (StundenplanTag tag : tage) {
                header.add(new SimpleTableRenderer.CellContent(tag.getWochentag(), boldFont, "center"));
            }
            contents.add(header);

            // crete all cells, find all stunde elements for each cell to build a newline separated string
            final List<List<StringJoiner>> cells = new ArrayList<>();
            for (int i = 1; i <= zeiten.size(); i++) {
                final List<StringJoiner> cell = new ArrayList<>();
                for (StundenplanTag tag : tage) {
                    final StringJoiner joiner = new StringJoiner("\n");
                    for (StundenplanStunde stunde : tag.getStunden()) {
                        if (stunde.getZeit().getStunde().equals(String.valueOf(i))) {
                            final StringJoiner stundeJoiner = new StringJoiner("\n");
                            for (StundenplanEintrag eintrag : stunde.getEintraege()) {
                                stundeJoiner.add(String.format("%s %s (%s)", eintrag.getFachKuerzel(), eintrag.getRaumKuerzel(), eintrag.getProfKuerzel()));
                            }
                            joiner.add(stundeJoiner.toString());
                        }
                    }
                    cell.add(joiner);
                }
                cells.add(cell);
            }

            boolean atLeastOneCellFilled = false;
            for (int i = 0; i < zeiten.size(); i++) {
                final List<SimpleTableRenderer.CellContent> row = new ArrayList<>();
                final StundenplanZeit zeit = zeiten.get(i);
                row.add(new SimpleTableRenderer.CellContent(zeit.getStunde() + "\n" + zeit.getStartZeit() + " - " + zeit.getEndZeit(), normalFont, "center"));
                for (StringJoiner joiner : cells.get(i)) {
                    if (joiner.length() > 0) {
                        atLeastOneCellFilled = true;
                    }
                    row.add(new SimpleTableRenderer.CellContent(joiner.toString(), normalFont, "left"));
                }
                contents.add(row);
            }

            if (!atLeastOneCellFilled) {
                throw new IllegalStateException("No cells filled in Stundenplan rendering");
            }

            final SimpleTableRenderer renderer = new SimpleTableRenderer();
            renderer.setInnerPaddingHorizontal(20);
            renderer.setInnerPaddingVertical(4);
            final BufferedImage image = renderer.renderTable(contents);
            return renderer.decorateWithTitle(image, new SimpleTableRenderer.CellContent("Stundenplan: " + name, boldFont, "left"));
        }
    }

    @Data
    public static class StundenplanTag {
        private final String wochentag;
        private final List<StundenplanStunde> stunden;
    }

    @Data
    public static class StundenplanStunde {
        private final StundenplanZeit zeit;
        private final List<StundenplanEintrag> eintraege;
    }

    @Data
    public static class StundenplanZeit {
        private final String stunde;
        private final String startZeit;
        private final String endZeit;
    }

    @Data
    public static class StundenplanEintrag {
        private final String fachLang;
        private final String fachKuerzel;
        private final String raumLang;
        private final String raumKuerzel;
        private final String profLang;
        private final String profKuerzel;
    }

}
