package fsi.startup;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Logger;

@Log4j2
public class ApplicationStartupLogger {

    public static void logApplicationSplashScreen() {
        final String projectPomVersion = BuildProperties.getProperties().getProperty(BuildProperties.PROJECT_VERSION);
        String projectHashVersion;
        try {
            projectHashVersion = JarHasher.getJarHash().substring(0, 16);
        } catch (Exception e) {
            projectHashVersion = "unknown hash, not started from jar file";
        }
        log.info("   ______ _____     _____   ____        _   ");
        log.info("  |  ____/ ____|   |_   _| |  _ \\      | |  ");
        log.info("  | |__ | (___ ______| |   | |_) | ___ | |_ ");
        log.info("  |  __| \\___ \\______| |   |  _ < / _ \\| __|");
        log.info("  | |    ____) |    _| |_  | |_) | (_) | |_ ");
        log.info("  |_|   |_____/    |_____| |____/ \\___/ \\__|");
        log.info("");
        log.info("   Version: {} ~ {}", projectPomVersion, projectHashVersion);
        log.info("");
    }

    public static void fullStep(Logger log, String message) {
        log.info(" - {}", message);
    }

    public static void subStep(Logger log, String message) {
        log.info("   L {}", message);
    }

    public static void subStepWarn(Logger log, String message) {
        log.warn("   x {}", message);
    }
}
