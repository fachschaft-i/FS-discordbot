package fsi.startup;

import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Log4j2
public class BuildProperties {
    private static final String CONFIG_FILE_PATH = "/build.properties";
    private static Properties properties;

    public static final String PROJECT_VERSION = "project.version";

    public static Properties getProperties() {
        if (properties == null) {
            loadProperties();
        }
        return properties;
    }

    private static synchronized void loadProperties() {
        properties = new Properties();
        try (final InputStream inputStream = BuildProperties.class.getResourceAsStream(CONFIG_FILE_PATH)) {
            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                log.error("Unable to load configuration file: {}", CONFIG_FILE_PATH);
            }
        } catch (IOException e) {
            log.error("Error loading configuration file: {}", e.getMessage());
        }
    }

    public static String loadClassResourceAsString(String path) throws IOException {
        try (final InputStream inputStream = BuildProperties.class.getResourceAsStream(path);
             final InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
             final BufferedReader bufferedReader = new BufferedReader(reader)) {
            final StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            return sb.toString();
        }
    }
}
