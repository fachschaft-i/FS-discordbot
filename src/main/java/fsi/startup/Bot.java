package fsi.startup;

import fsi.config.FsBotConfig;
import fsi.listeners.altklausur.AltklausurSlash;
import fsi.listeners.selfrole.SelfroleListener;
import fsi.listeners.selfrole.SelfroleSlash;
import fsi.listeners.stundenplan.StundenplanSlash;
import fsi.listeners.timetable.TimetableInfoSlash;
import fsi.listeners.utility.CommandLoggerListener;
import fsi.listeners.utility.HelpSlash;
import fsi.listeners.utility.SlashManager;
import fsi.listeners.verification.AdminPrivateChatListener;
import fsi.listeners.verification.VerifyUserPrivateChatListener;
import fsi.listeners.verification.VerifyUserSlash;
import fsi.listeners.voice.VcListener;
import fsi.service.MailSendingService;
import fsi.service.StudentVerificationService;
import fsi.service.altklausur.AltklausurAccess;
import fsi.service.stundenplan.StundenplanService;
import fsi.service.timetable.ProfessorScraper;
import fsi.service.timetable.TimetableScraper;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.ChunkingFilter;
import net.dv8tion.jda.api.utils.MemberCachePolicy;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;

@Log4j2
public class Bot {

    private final JDA jda;

    private final SlashManager commandManager;

    private final StudentVerificationService verificationService;
    private final MailSendingService mailService;
    private final StundenplanService stundenplanService;
    private final AltklausurAccess altklausurAccess;
    private final TimetableScraper timetableScraper;
    private final ProfessorScraper professorScraper;

    public static void main(String[] args) throws InterruptedException {
        ApplicationStartupLogger.logApplicationSplashScreen();
        new Bot();
    }

    private Bot() throws InterruptedException {
        final long performanceSetupTime = System.currentTimeMillis();
        log.info("Setting up Discord bot:");

        FsBotConfig.init();

        ApplicationStartupLogger.fullStep(log, "Cleaning up temp files from previous runs");
        Bot.cleanTempFiles();

        this.verificationService = new StudentVerificationService();
        this.mailService = new MailSendingService();
        try {
            this.mailService.verifyAuthenticated();
        } catch (SocketTimeoutException e) {
            ApplicationStartupLogger.subStepWarn(log, "Timed out during mail verification.");
            log.error("Failed to verify mail sending service. Request for access token timed out: {}", e.getMessage(), e);
            System.exit(3);
        } catch (IOException e) {
            ApplicationStartupLogger.subStepWarn(log, "Failed to verify mail sending service, see logs below for more detail.");
            log.error("Failed to verify mail sending service. Check if your MAIL_CLIENT_ID, MAIL_CLIENT_SECRET, MAIL_REFRESH_TOKEN are correct: {}", e.getMessage(), e);
            System.exit(2);
        }
        this.stundenplanService = new StundenplanService();
        this.altklausurAccess = new AltklausurAccess();
        this.timetableScraper = new TimetableScraper(new File("./data/timetable"));
        this.professorScraper = new ProfessorScraper(new File("./data/professors.json"));

        ApplicationStartupLogger.fullStep(log, "Creating slash command handlers");
        this.commandManager = new SlashManager();

        final HelpSlash helpSlash = new HelpSlash(this.commandManager);
        final SelfroleSlash selfroleSlash = new SelfroleSlash();
        final VerifyUserSlash verifyUserSlash = new VerifyUserSlash(this.verificationService, this.mailService);
        final StundenplanSlash stundenplanSlash = new StundenplanSlash(this.stundenplanService);
        final AltklausurSlash altklausurSlash = new AltklausurSlash(this.altklausurAccess);
        final TimetableInfoSlash profFindInfoSlash = new TimetableInfoSlash(this.timetableScraper, this.professorScraper);

        this.commandManager.addCommands(helpSlash, selfroleSlash, verifyUserSlash, stundenplanSlash, altklausurSlash, profFindInfoSlash);

        ApplicationStartupLogger.fullStep(log, "Creating other listeners");
        final VerifyUserPrivateChatListener verifyUserPrivateChatListener = new VerifyUserPrivateChatListener(this.verificationService);
        final AdminPrivateChatListener adminPrivateChatListener = new AdminPrivateChatListener();
        final CommandLoggerListener commandLoggerListener = new CommandLoggerListener();
        final VcListener vcListener = new VcListener();
        final SelfroleListener selfroleListener = new SelfroleListener();

        ApplicationStartupLogger.fullStep(log, "Setting up JDA instance");
        this.jda = commandManager.registerCommands(
                        JDABuilder.createDefault(FsBotConfig.get(FsBotConfig.Attribute.DISCORD_BOT_TOKEN))
                                .setChunkingFilter(ChunkingFilter.ALL)
                                .enableIntents(GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MESSAGE_REACTIONS)
                                .setMemberCachePolicy(MemberCachePolicy.ALL)
                                // add listeners other than slash commands
                                .addEventListeners(commandLoggerListener)
                                .addEventListeners(verifyUserPrivateChatListener)
                                .addEventListeners(adminPrivateChatListener)
                                .addEventListeners(selfroleListener)
                )
                .build();

        this.jda.awaitReady();

        ApplicationStartupLogger.fullStep(log, "Checking for existence of primary bot guild");
        final Guild primaryGuild = jda.getGuildById(FsBotConfig.get(FsBotConfig.Attribute.PRIMARY_BOT_GUILD_ID));
        if (primaryGuild == null) {
            log.error("Either the primary guild for this bot does not exist, or the bot has not been added to it yet: {}", FsBotConfig.get(FsBotConfig.Attribute.PRIMARY_BOT_GUILD_ID));
            System.exit(1);
        }

        ApplicationStartupLogger.fullStep(log, "Using JDA instance to complete setup");
        verifyUserSlash.setup(this.jda);
        verifyUserPrivateChatListener.setup(this.jda);
        adminPrivateChatListener.setup(this.jda);
        altklausurSlash.setup(this.jda);
        profFindInfoSlash.setup(this.jda);

        ApplicationStartupLogger.fullStep(log, "Adding shutdown hood to application");
        Runtime.getRuntime().addShutdownHook(new Thread(this::setupShutdownHook));

        ApplicationStartupLogger.fullStep(log, "Register guild commands on JDA instance");
        primaryGuild.updateCommands().addCommands(this.commandManager.constructCommands()).queue();

        log.info("");
        log.info(" --> Startup complete, bot is now running (took ~{} seconds)", (System.currentTimeMillis() - performanceSetupTime) / 1000);
        log.info("");

        // new Thread(this.altklausurAccess::ensureCache).start();
        new Thread(profFindInfoSlash::ensureCache).start();
    }

    public void setupShutdownHook() {
        try {
            jda.getUserById(FsBotConfig.get(FsBotConfig.Attribute.ADMIN_USER_ID)).openPrivateChannel().complete().sendMessage("Bot is shutting down...").queue();
        } catch (Exception e) {
            log.error("Failed to notify admin user about bot shutdown: {}", e.getMessage(), e);
        }
        jda.getPresence().setStatus(OnlineStatus.OFFLINE);
        jda.shutdown();
        log.info("FS-I Bot shutdown completed successfully");
    }

    public static File obtainTempFile(String fileName) {
        final File file = new File("./temp", fileName);
        file.getParentFile().mkdirs();
        file.deleteOnExit();

        return file;
    }

    private static void cleanTempFiles() {
        final File tempDir = new File("./temp");
        if (tempDir.exists()) {
            for (File file : tempDir.listFiles()) {
                Bot.cleanRecursive(file);
            }
        }
    }

    private static void cleanRecursive(File file) {
        if (file.isDirectory()) {
            for (File subFile : file.listFiles()) {
                cleanRecursive(subFile);
            }
        }
        file.delete();
    }
}
