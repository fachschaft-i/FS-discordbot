Hey **[[USERNAME]]**, die Prüfungsphase an der Hochschule Mannheim ist vorbei! Wir hoffen natürlich, dass deine Prüfungen gut gelaufen sind.
Jetzt ist der perfekte Zeitpunkt, um deine Altklausuren oder Gedankenprotokolle zu diesen (oder weiteren!) Modulen zu teilen:
**[[MODULES]]**

Verwende dazu den Befehl `/altklausuren upload` im Channel [[CHANNEL]], um einen persönlichen Upload-Link zu erhalten.
So kannst du der nächsten Generation beim Lernen helfen. Vielen Dank!