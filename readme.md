## FS-I Discord Bot

Der FS-I Discord Bot wird auf dem Server der Fachschaft Informatik an der Hochschule Mannheim eingesetzt, um
die Verwaltung und Interaktion der Benutzer zu erleichtern. Dieses Repository enthält den Quellcode und die
Dokumentation für den Bot.

<!-- TOC -->

* [FS-I Discord Bot](#fs-i-discord-bot)
    * [Hosting](#hosting)
    * [Features](#features)
        * [Benutzerverifizierung](#benutzerverifizierung)
        * [Altklausuren-Sharing](#altklausuren-sharing)
        * [Selfrole: Automatische Rollenvergabe](#selfrole-automatische-rollenvergabe)
        * [Lernraum Voice-Channel](#lernraum-voice-channel)
        * [Stundenplan Generator](#stundenplan-generator)
        * [Prof/Raum-Infos](#profraum-infos)
    * [Bauen des Bots](#bauen-des-bots)
    * [Konfiguration und Ausführung](#konfiguration-und-ausführung)
    * [Updaten des Bots](#updaten-des-bots)

<!-- TOC -->

### Hosting

- Der Bot wird momentan von [Yan Wittmann](mailto:yan.wittmann@stud.hs-mannheim.de) maintained, bei Problemen meldet man
  sich also am besten bei ihm.
- Der Bot wird auf einem Ubuntu-Rechner im Fachschaftsraum in Gebaute A gehostet und startet sich mit crontab
  automatisch neu, falls der Rechner neu startet.

### Features

#### Benutzerverifizierung

Stellt sicher, dass nur verifizierte Studenten Zugang zu bestimmten Kanälen haben, indem die Hochschul-E-Mail-Adresse
der Benutzer überprüft wird.
Dazu wird eine E-Mail an `matrikelnummer@stud.hs-mannheim.de` mit einem Code gesendet, den ein Nutzer in einem
Privatkanal mit dem Bot eingeben muss, um verifiziert zu werden.

Im Detail wird die Verifikation wird über einen Kanal `VERIFY_CHANNEL_ID` gestartet, indem der Nutzer
`/verify matrikelnummer:...` eingibt.  
Daraufhin wird über eine Google Cloud Console Applikation freigegebene Mailadresse eine Mail an die angegebene
E-Mail-Adresse gesendet, die den Nutzer auffordert, den Code in einem Privatkanal mit dem Bot einzugeben.
Dieser Code ist nur eine gewisse Zeit lang gültig und der Nutzer wird bei einem Fehlversuch für
`VERIFY_COOLDOWN_DURATION_MS` millisekunden vom Versuch ausgeschlossen.

#### Altklausuren-Sharing

Die Fachschaft verwaltet Altklausuren auf dem
[NextCloud-Server der Hochschule](https://owncloud.informatik.hs-mannheim.de/index.php/apps/files/files/276567?dir=/Fachschaft/Klausuren).
Früher wurde ein Python-Skript verwendet, um den Download manuell auszulösen:
https://gitlab.com/fachschaft-i/altklausur-scraper

Es hat sich aber gezeigt, dass oft diese Studenten-Requests von den Zuständigen erst spät oder gar nicht bearbeitet
wurden, weswegen der Bot nun automatisch auf Anfrage die Altklausuren herunterlädt und als File Share auf dem
NextCloud-Server hochlädt.

Dazu wird der Befehl `/altklausur` verwendet.

- `/altklausur list`: Listet verfügbare Module und deren zugehörige Professoren für Altklausuren auf.
- `/altklausur download ids:<Module-ID> <Prof-ID>`: Lädt Altklausuren für spezifizierte Module und/oder Professoren
  herunter.
- `/altklausur refresh`: Aktualisiert den Cache der verfügbaren Altklausuren.

#### Selfrole: Automatische Rollenvergabe

Erlaubt die Erstellung von einem Drop-Down Element, über das ein Nutzer sich selbst Rollen zuweisen kann.

![img.png](doc/bot-selfrole-demo.png)

Dies wird über den Befehl `/selfrole` gestartet, der diverse Parameter besitzt:

- `titel`: Der Titel des Embeds, das über der Auswahl angezeigt wird.
- `beschreibung`: Der Text-Inhalt des Embeds, das über der Auswahl angezeigt wird.
- `max`: Die maximale Anzahl an Rollen, die ein Nutzer auswählen kann. Standardmäßig können alle Rollen gewählt werden.
- `role-01`, `role-02`, ...: Die Rollen, die ein Nutzer auswählen kann. Hier können bis zu 25 - 3 = 22 Rollen
  zur Verfügung gestellt werden.

Nur Nutzer mit `Permission.BAN_MEMBERS` und `Permission.KICK_MEMBERS` können diesen Befehl ausführen.

#### Lernraum Voice-Channel

Jeder Nutzer kann durch Beitreten in einen Voice Channel `VOICE_CHANNEL_ID_CREATE_CHANNEL` einen temporären Voice
Channel mit seinem Namen anlegen.
Dieser wird automatisch gelöscht, wenn der Channel leer ist.

#### Stundenplan Generator

Mit einem `/stundenplan` Befehl kann ein Embed mit drop-downs gebaut werden, mit dem ein Nutzer seinen eigenen
Stundenplan durch Auswahl von Studiengang, Semester und Modulen erstellen kann.
Dieser wird dann dem Nutzer in einem privaten Chat zugesendet.

#### Prof/Raum-Infos

Mit dem Befehl `/find` kann nach Informationen über Professoren oder freie Räume in der Hochschule gesucht werden.
Dazu werden dynamisch Daten von der Hochschul-Website abgerufen, geparst und interpretiert.

Beispiel: `/find prof name:FIM` oder
`/find freeroom time: 08:00 weekday: Mittwoch building: A- Informatik, Maschinenbau, Rektorat`.

Mit dem ersten Befehl wird zunächst nach Professoren gesucht, die `FIM` als Kürzel haben, danach wird im vollen Namen
gesucht. Das Ergebnis ist ein Embed mit dem Profil des Professors, wie im Bild unten zu sehen.

![Fimmel, Dr. Elena (FIM)](doc/proffind-fim.png)

Der nächste Befehl sucht nach freien Räumen, in einem bestimmten Gebäude, an einem bestimmten Wochentag und zu einer
gewissen Uhrzeit. Ein Beispiel für die Ausgabe ist im Bild unten zu sehen.

![img.png](doc/proffind-room-a.png)

### Bauen des Bots

Um den Bot in eine ausführbare JAR-Datei zu bauen, wird [Maven](https://maven.apache.org/) verwendet.

```shell
mvn clean package
```

Damit wird im `target`-Ordner eine JAR-Datei `FS-discordbot-<version>.jar` erstellt, die mit mindestens Java 8
ausgeführt werden kann.

### Konfiguration und Ausführung

> WICHTIG! VOR DEM KONFIGURIEREN DIESEN ABSCHNITT LESEN:

Da hier credentials vom Discord bot, die Google Mail API und die NextCloud API verwendet werden, ist es wichtig, diese
Daten sicher zu speichern.
Wenn es dir als maintainer nicht wichtig genug ist, deine Daten zu schützen, dann kannst du tatsächlich einfach diese
Werte in der `.env`-Datei speichern.
Es sollte dir allerdings wichtig sein, das zu tun.
Darum kannst du mit der Beschreibung in [doc/env-file-encryption-setup.md](doc/env-file-encryption-setup.md) diesen
Prozess durchführen.

Jetzt aber … details zu den einzelnen Konfigurationsparametern kann in [doc/configuration.md](doc/configuration.md) und
[doc/google_mail_secret.md](doc/google_mail_secret.md) gefunden werden, da hier zu wenig Platz ist.

In jedem Fall wird das Projekt mit .env (dotenv) konfiguriert, sodass die Umgebungsvariablen in einer `.env`-Datei
gespeichert werden können.
Durch die Features von dotenv können diese Parameter natürlich auch über Umgebungsvariablen gesetzt werden.

```properties
DISCORD_BOT_TOKEN=...
PRIMARY_BOT_GUILD_ID=...
# ...
```

Mit der `.env`-Datei im selben Verzeichnis kann der Bot mit folgendem Befehl gestartet werden:

```shell
java -jar FS-discordbot-<version>.jar
```

In der darauffolgenden Ausgabe sind einige interessante Faktoren zu sehen: Projektversion aus der [pom.xml](pom.xml),
Hash der JAR-Datei, der Setup-Prozess mit allen Schritten für einfacheres Debugging beim Setup und wie lange der
Prozess gedauert hat.

```shell
yan@yan:~/workspace/projects/FS-discordbot/target$ java -jar FS-discordbot-1.0.0.jar 
   ______ _____     _____   ____        _   
  |  ____/ ____|   |_   _| |  _ \      | |  
  | |__ | (___ ______| |   | |_) | ___ | |_ 
  |  __| \___ \______| |   |  _ < / _ \| __|
  | |    ____) |    _| |_  | |_) | (_) | |_ 
  |_|   |_____/    |_____| |____/ \___/ \__|

   Version: 1.0.0 ~ c636de6f7bd0ac61

Setting up Discord bot:
 - Loading application configuration from .env (dotenv)
   L Loaded 77 properties
   L All [18 / 18] named attributes were found during loading of .env (dotenv)
 - Creating StudentVerificationService
   L Using configuration: VERIFY_COOLDOWN_DURATION_MS=60000
> ... and so on ...
 - Setting up JDA instance
[JDA] Login Successful!
[JDA] Connected to WebSocket
[JDA] Finished Loading!
 - Checking for existence of primary bot guild
 - Using JDA instance to complete setup
 - Adding shutdown hood to application
 - Register guild commands on JDA instance
   L Selfrole command max roles count: 22

 --> Startup complete, bot is now running (took ~3 seconds)
```

### Updaten des Bots

Der Bot kann sich selbst aktualisieren, indem ein Update heruntergeladen und angewendet wird.
Mehr dazu in [doc/admin_user.md](doc/admin_user.md).
